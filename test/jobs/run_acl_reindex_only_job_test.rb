require "test_helper"

class RunAclReindexOnlyJobTest < ActiveJob::TestCase
  test "running job should work" do
    assert_nothing_raised { RunAclReindexOnlyJob.perform_now }
  end
end
