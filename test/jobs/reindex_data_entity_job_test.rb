require "test_helper"

class ReindexDataEntityJobTest < ActiveJob::TestCase
  test "running job with workflow should work" do
    workflow = WorkflowDefinition.find_by!(name: "Erstgespräch Erfindung").create_workflow!
    workflow.ambitions << Ambition.create(title: "Blubb Ziel")
    assert_nothing_raised { ReindexSearchableJob.perform_now(workflow) }
  end
end
