require "./test/simplecov_env"
SimpleCovEnv.start!

ENV["RAILS_ENV"] ||= "test"

require_relative "../config/environment"
require "rails/test_help"

require "capybara/rails"
require "capybara/minitest"

# Execute Tests in parallel on CI
# Requires a report file with timing metrics to be present. Regenerate (if new tests are added or some take longer) with:
# CI=true KNAPSACK_GENERATE_REPORT=true bundle exec rake test test:system
# (to sort for nicer diff/comparison do 'puts knapsack_json.sort.to_h.to_json' in console and paste back)
if ENV["CI"]
  require "knapsack"
  Knapsack.tracker.config({enable_time_offset_warning: false})
  knapsack_adapter = Knapsack::Adapters::MinitestAdapter.bind
  knapsack_adapter.set_test_helper_path(__FILE__)
end

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  unless ENV["CI"] # Parallel Tests only with local development for now
    parallelize(workers: 3) # More than three does not yield any improvement on Dev machine (quadcore) - see #612

    parallelize_setup do |worker|
      @@worker_id = worker # In case we need the worker_id some time later
      # Each worker get's a elasticsearch instance
      ENV["ELASTICSEARCH_URL"] = "http://localhost:920#{worker}"
      # Wait for all containers to have booted properly
      CustomElasticSearchConfig.wait_for_api_version_response

      # Workaround for correct coverage reports until https://github.com/simplecov-ruby/simplecov/issues/718 is fixed
      SimpleCov.command_name "#{SimpleCov.command_name}-#{worker}"
    end

    parallelize_teardown do
      SimpleCov.result
    end
  end

  # Add more helper methods to be used by all tests here...
end

class ActionDispatch::IntegrationTest
  # Make the Capybara DSL available in all integration tests (visit _url_ etc.)
  include Capybara::DSL
  # Make `assert_*` methods behave like Minitest assertions
  include Capybara::Minitest::Assertions

  # include Devise::Test::IntegrationHelpers # allows sign_in(@user) but we do not use it
  include Warden::Test::Helpers # allows login_as(@user)

  Warden.test_mode!

  teardown do
    Warden.test_reset!
    Capybara.reset_sessions!
    Capybara.use_default_driver
  end
end
