require "test_helper"

class HomeControllerTest < ActionDispatch::IntegrationTest
  setup do
    @current_user = User.find_by!(email: "srs_user@example.org")
  end

  test "should allow home#show to logged in user" do
    login_as(@current_user)
    get home_show_path
    assert_response :success
  end

  test "should allow home#my to logged in user" do
    login_as(@current_user)
    get "/my"
    assert_response :success
  end

  test "home#show should redirect to login if not logged in" do
    get home_show_path
    assert_redirected_to new_user_session_path
  end

  test "home#my should redirect to login if not logged in" do
    get "/my"
    assert_redirected_to new_user_session_path
  end
end
