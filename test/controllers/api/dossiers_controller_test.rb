require "test_helper"

class Api::DossiersControllerTest < ActionDispatch::IntegrationTest
  def setup
    @current_user = User.find_by!(email: "srs_user@example.org")
    login_as @current_user
    @dossier_definition = DossierDefinition.find_by(name: "Person")
    @dossier = @dossier_definition.dossiers.find(1) # Admin User
  end

  def get_field_from_name!(name)
    @dossier_definition.fields.find_by!(name: name)
  end

  test "should get index" do
    CustomElasticSearchConfig.reindex_and_refresh(Dossier)
    get api_dossiers_path(format: :json)
    assert_response :success
  end

  test "should get index with additional query params" do
    CustomElasticSearchConfig.reindex_and_refresh(Dossier)
    params = {
      page: 1,
      query: "james",
      definition_ids: [@dossier_definition.id],
      field: "Name",
      field_query: @dossier.get_field_value(get_field_from_name!("Name").id),
      order: "created_at_desc"
    }
    get api_dossiers_path(format: :json), params: params
    assert_response :success
  end

  test "index should export to excel" do
    CustomElasticSearchConfig.reindex_and_refresh(Dossier)

    get api_dossiers_path(format: :xlsx)
    assert_response :success

    assert_equal "application/octet-stream", response.header["Content-Type"] # File download
    assert_equal Dossier.not_deleted.count, @controller.instance_variable_get(:@result).count # No pagination
  end

  test "should get list with query params" do
    CustomElasticSearchConfig.reindex_and_refresh(Dossier)
    params = {
      definition_id: @dossier_definition.id,
      except: [@dossier.id],
      query: "user"
    }
    get list_api_dossiers_path(format: :json), params: params
    assert_response :success
  end

  test "should get dossier show" do
    CustomElasticSearchConfig.reindex_and_refresh(Dossier, Workflow) # process for reference counts

    get api_dossier_path(@dossier, format: :json)
    assert_response :success
  end

  test "should refuse to get show of a deleted dossier" do
    deleted_dossier = Dossier.find_by(deleted: true)
    get api_dossier_path(deleted_dossier, format: :json)
    assert_response :gone
  end

  test "should get new, a not persisted dossier" do
    get new_api_dossier_path(format: :json), params: {dossier_definition_id: @dossier_definition.id}
    assert_response :success
  end

  test "should create a new dossier" do
    data = {
      "E-Mail" => "test@test.org",
      "Name" => "Test Dossier Name"
    }

    assert_difference -> { Dossier.count } do
      post api_dossiers_path(format: :json), params: {
        dossier_definition_id: @dossier_definition.id,
        field_data: data.map { |k, v| [get_field_from_name!(k).id, v] }.to_h
      }
    end
    assert_response :success
  end

  test "should not create a new dossier without all required fields" do
    assert_no_difference -> { Dossier.count } do
      post api_dossiers_path(format: :json), params: {
        dossier_definition_id: @dossier_definition.id,
        field_data: {email: "test@test.org"}
      }
    end
    assert_response :unprocessable_entity
  end

  test "should update dossier" do
    CustomElasticSearchConfig.reindex_and_refresh(Dossier, Workflow) # process for reference counts

    patch api_dossier_path(@dossier, format: :json), params: {dossier_field_definition_id: get_field_from_name!("Name").id, value: "Updated Name"}
    assert_response :success
  end

  test "should respond with error to invalid update" do
    patch api_dossier_path(@dossier, format: :json), params: {dossier_field_definition_id: get_field_from_name!("Name").id, value: nil}
    assert_response :unprocessable_entity
  end

  test "should delete dossier" do
    delete api_dossier_path(@dossier, format: :json)
    assert_response :success
  end

  test "should get dossier show_references" do
    CustomElasticSearchConfig.reindex_and_refresh(Dossier, Workflow) # process for reference counts

    get show_references_api_dossier_path(@dossier, format: :json)
    assert_response :success
  end

  test "should refuse to get show_references of a deleted dossier" do
    deleted_dossier = Dossier.find_by(deleted: true)
    get show_references_api_dossier_path(deleted_dossier, format: :json)
    assert_response :gone
  end

  ## Access Denied Tests

  # All logged in Users may access #index and #list but get results according to search
  test "should refuse #index to not logged in users" do
    logout
    get api_dossiers_path(format: :json)
    assert_response :unauthorized
  end

  test "should refuse #list to not logged in users" do
    logout
    get list_api_dossiers_path(format: :json)
    assert_response :unauthorized
  end

  test "should refuse #show for non group user" do
    login_non_group_user
    get api_dossier_path(@dossier, format: :json)
    assert_response :forbidden
  end

  test "should refuse #new for non group user" do
    login_non_group_user
    get new_api_dossier_path(format: :json), params: {dossier_definition_id: @dossier_definition.id}
    assert_response :forbidden
  end

  test "should refuse #create for non group user" do
    login_non_group_user
    post api_dossiers_path(format: :json), params: {dossier_definition_id: @dossier_definition.id}
    assert_response :forbidden
  end

  test "should refuse #update for non group user" do
    login_non_group_user
    patch api_dossier_path(@dossier, format: :json), params: {dossier_field_definition_id: get_field_from_name!("Name").id, value: "Updated Name"}
    assert_response :forbidden
  end

  test "should refuse #destroy for non group user" do
    login_non_group_user
    delete api_dossier_path(@dossier, format: :json)
    assert_response :forbidden
  end

  test "should refuse #show_references for non group user" do
    login_non_group_user
    get show_references_api_dossier_path(@dossier, format: :json)
    assert_response :forbidden
  end

  private

  def login_non_group_user
    logout
    login_as(User.find_by_email("no_access_user@example.org"))
  end
end
