require "test_helper"

class Api::WorkflowsControllerTest < ActionDispatch::IntegrationTest
  def setup
    @current_user = User.find_by!(email: "srs_user@example.org")
    @no_access_user = User.find_by!(email: "no_access_user@example.org")

    @srs_workflow = Workflow.find_by!(title: "srs-workflow")

    login_as(@current_user)
  end

  test "index should work" do
    CustomElasticSearchConfig.reindex_and_refresh(Workflow, WorkflowDefinition, Ambition)
    get api_workflows_url(format: :json)
    assert_response :success
  end

  test "index should work when filtering" do
    CustomElasticSearchConfig.reindex_and_refresh(Workflow, WorkflowDefinition, Ambition)

    params = {
      page: 1,
      query: "james",
      assignee_ids: [@current_user.id],
      contributor_ids: User.all.limit(3).pluck(:id),
      workflow_definition_ids: [WorkflowDefinition.first.id],
      ambition_ids: Ambition.all.limit(5).pluck(:id),
      tab_category: "RUNNING",
      order: "created_at_desc"
    }

    get api_workflows_url(params.merge(format: :json))
    assert_response :success
  end

  test "index should export to excel" do
    login_as(User.find_by!(email: "admin@example.org"))
    CustomElasticSearchConfig.reindex_and_refresh(Workflow, WorkflowDefinition, Ambition)

    params = {
      tab_category: "ALL"
    }

    get api_workflows_url(params.merge(format: :xlsx))
    assert_response :success

    assert_equal "application/octet-stream", response.header["Content-Type"] # File download
    assert_equal Workflow.not_deleted.count, @controller.instance_variable_get(:@result).count # No pagination
  end

  test "should get list" do
    CustomElasticSearchConfig.reindex_and_refresh(Workflow)
    get list_api_workflows_path(format: :json)
    assert_response :success
  end

  test "should get list with filtering" do
    CustomElasticSearchConfig.reindex_and_refresh(Workflow)
    params = {
      query: "james",
      except: Workflow.all.limit(3).pluck(:id),
      max_result_count: 2
    }
    get list_api_workflows_path(params.merge(format: :json))
    assert_response :success
  end

  test "workflow#show should work" do
    get api_workflow_url(@srs_workflow, format: :json)
    assert_response :success
  end

  test "should get http 'gone' response for deleted workflow show" do
    @srs_workflow.trash!
    get api_workflow_url(@srs_workflow, format: :json)
    assert_response :gone
  end

  test "should update workflow" do
    patch api_workflow_path(@srs_workflow, format: :json), params: {workflow: {title: "Ein neuer Titel"}}
    assert_response :success
  end

  test "should return errors for updating to empty title workflow" do
    patch api_workflow_path(@srs_workflow, format: :json), params: {workflow: {title: ""}}
    assert_response :unprocessable_entity
  end

  test "should update_assignee" do
    patch update_assignee_api_workflow_path(@srs_workflow, format: :json), params: {assignee: @current_user.id}
    assert_response :success
  end

  test "should allow removing assignee" do
    patch update_assignee_api_workflow_path(@srs_workflow, format: :json), params: {assignee: nil}
    assert_response :success
  end

  test "should update_contributors" do
    patch update_contributors_api_workflow_path(@srs_workflow, format: :json), params: {contributors: User.all.limit(3).pluck(:id)}
    assert_response :success
  end

  test "should allow removing all contributors" do
    patch update_contributors_api_workflow_path(@srs_workflow, format: :json), params: {contributors: [nil]}
    assert_response :success
  end

  test "should update_ambitions" do
    patch update_ambitions_api_workflow_path(@srs_workflow, format: :json), params: {ambitions: Ambition.all.limit(3).pluck(:id)}
    assert_response :success
  end

  test "should allow removing all ambitions" do
    patch update_ambitions_api_workflow_path(@srs_workflow, format: :json), params: {ambitions: [nil]}
    assert_response :success
  end

  test "should get new workflow with default values" do
    get new_api_workflow_path(format: :json), params: {workflow_definition_id: @srs_workflow.workflow_definition.id}
    assert_response :success
  end

  test "should create new workflows" do
    assert_difference -> { Workflow.count } do
      post api_workflows_path(format: :json),
        params: {workflow_definition_id: @srs_workflow.workflow_definition.id, workflow: {title: "Test-Workflow"}}
    end
    assert_response :success
  end

  test "should create new workflows with ambition and predecessor" do
    assert_difference -> { Workflow.count } do
      post api_workflows_path(format: :json),
        params: {
          workflow_definition_id: @srs_workflow.workflow_definition.id,
          ambition_id: Ambition.first.id,
          predecessor_workflow_id: @srs_workflow.id,
          workflow: {title: "Test-Workflow"}
        }
    end
    assert_response :success
  end

  test "should allow completing a workflow" do
    @srs_workflow.all_tasks.each { |t| t.start! && t.complete! }
    assert_difference -> { Workflow.completed.count } do
      patch complete_api_workflow_path(@srs_workflow, format: :json)
    end
    assert_response :success
  end

  # Access Forbidden Tests - should be compacted and/or moved to different test

  test "no_access_user shouldn't be able to show a workflow" do
    login_as(@no_access_user)
    get api_workflow_url(@srs_workflow, format: :json)
    assert_response :forbidden
  end

  test "no_access_user shouldn't be able to update (title and description) of a workflow" do
    login_as(@no_access_user)
    patch api_workflow_url(@srs_workflow, format: :json), params: {title: "updated title", description: "updated description"}
    assert_response :forbidden
  end

  test "no_access_user shouldn't be able to set or unset the assignee of a workflow" do
    login_as(@no_access_user)
    patch update_assignee_api_workflow_url(@srs_workflow, format: :json), params: {assignee: @no_access_user.id}
    assert_response :forbidden

    patch update_assignee_api_workflow_url(@srs_workflow, format: :json), params: {assignee: nil}
    assert_response :forbidden
  end

  test "no_access_user shouldn't be able to update the contributors of a workflow" do
    login_as(@no_access_user)
    patch update_contributors_api_workflow_url(@srs_workflow, format: :json), params: {contributors: [@no_access_user.id]}
    assert_response :forbidden
  end

  test "no_access_user shouldn't be able to complete a workflow" do
    login_as(@no_access_user)

    patch complete_api_workflow_path(@srs_workflow, format: :json)

    assert_response :forbidden
  end

  test "new invalid task should have error messages" do
    assert_difference -> { @srs_workflow.direct_tasks.count }, 0 do
      post add_task_api_workflow_url(@srs_workflow, format: :json), params: {task: {name: nil, description: nil}}
    end
    assert_response :unprocessable_entity
    assert_equal ["muss ausgefüllt werden"], JSON.parse(response.body)["name"]
  end

  test "new task should be created at the end and should be startable" do
    assert_difference -> { @srs_workflow.direct_tasks.count }, 1 do
      post add_task_api_workflow_url(@srs_workflow, format: :json), params: {task: {name: "Ein neuer Task", description: "Beschreibung des neuen Tasks."}}
    end
    assert_response :success

    new_task = @srs_workflow.direct_tasks.last

    assert_equal "Ein neuer Task", new_task.name
    assert_equal "Beschreibung des neuen Tasks.", new_task.description
    assert_equal "created", new_task.aasm_state
    assert_not_nil new_task.state_updated_at

    patch start_api_task_url(new_task, format: :json)
    assert_response :success
  end

  test "shouldn't be possible to add a new task to a non active workflow" do
    @completed_workflow = Workflow.find_by!(title: "PA-1")
    assert @completed_workflow.completed?
    post add_task_api_workflow_url(@completed_workflow, format: :json), params: {task: {name: "Ein neuer Task", description: "Beschreibung des neuen Tasks."}}
    assert_response :method_not_allowed
  end

  test "new block should be created at the end" do
    assert_difference -> { @srs_workflow.blocks.count }, 1 do
      post add_block_api_workflow_url(@srs_workflow, format: :json), params: {block: {title: "Ein neuer Block"}}
      assert_response :success
    end

    new_block = @srs_workflow.items.last

    assert_equal "Ein neuer Block", new_block.title
    assert_equal false, new_block.parallel
    assert_equal "created", new_block.aasm_state
  end

  test "new block should be created with params" do
    workflow = Workflow.find_by!(title: "Erstgespräch Erfindung für eine Test-Erfindung")
    assert workflow.content_items.where(content_type: ContentTypes::Boolean.to_s).any?
    content_item = workflow.content_items.where(content_type: ContentTypes::Boolean.to_s).first

    assert_difference -> { workflow.blocks.count }, 1 do
      post add_block_api_workflow_url(workflow, format: :json), params: {block: {title: "Ein ganz neuer Block", parallel: true, decision: "true", content_item_id: content_item.id}}
      assert_response :success
    end

    new_block = workflow.items.last

    assert_equal "Ein ganz neuer Block", new_block.title
    assert_equal true, new_block.parallel
    assert_equal "true", new_block.decision
    assert_equal content_item.id, new_block.content_item_id
    assert_equal "created", new_block.aasm_state
  end

  test "shouldn't be possible to add a new block to a non active workflow" do
    @completed_workflow = Workflow.find_by!(title: "PA-1")
    assert @completed_workflow.completed?
    post add_block_api_workflow_url(@completed_workflow, format: :json), params: {block: {title: "Ein ganz neuer Block", parallel: true, decision: "true", content_item_id: @completed_workflow.content_items.first.id}}
    assert_response :method_not_allowed
  end
end
