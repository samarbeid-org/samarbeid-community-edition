require "test_helper"

class Api::TasksControllerTest < ActionDispatch::IntegrationTest
  def setup
    @current_user = User.find_by!(email: "srs_user@example.org")
    @no_access_user = User.find_by!(email: "no_access_user@example.org")

    @srs_workflow = Workflow.find_by!(title: "srs-workflow")
    @srs_task = @srs_workflow.direct_tasks.find { |t| t.name == "Beschreibung erstellen" }

    login_as(@current_user)
  end

  test "index of tasks should work without parameter and return correct json" do
    CustomElasticSearchConfig.reindex_and_refresh(Task, WorkflowDefinition, Ambition)
    get api_tasks_url(format: :json)
    assert_response :success

    assert @response.parsed_body.key?("tab_categories")
    assert_equal 4, @response.parsed_body["tab_categories"].count

    assert @response.parsed_body.key?("users")
    assert_equal 7, @response.parsed_body["users"].count

    assert @response.parsed_body.key?("workflow_definitions")
    assert_equal 6, @response.parsed_body["workflow_definitions"].count

    assert @response.parsed_body.key?("ambitions")
    assert_equal 10, @response.parsed_body["ambitions"].count

    assert @response.parsed_body.key?("task_definitions")
    assert_equal 37, @response.parsed_body["task_definitions"].count

    assert @response.parsed_body.key?("total_pages")
    assert_equal 1, @response.parsed_body["total_pages"]

    assert @response.parsed_body.key?("result")
    assert_equal 10, @response.parsed_body["result"].count
  end

  test "index of tasks for my marked tasks should work" do
    CustomElasticSearchConfig.reindex_and_refresh(Task, WorkflowDefinition, Ambition)
    get api_tasks_url(tab_category: "MARKED", format: :json)
    assert_response :success
  end

  test "index of tasks with full filters should work" do
    CustomElasticSearchConfig.reindex_and_refresh(Task, WorkflowDefinition, Ambition)

    params = {
      page: 1,
      assignee_ids: [@current_user.id],
      contributor_ids: User.all.limit(3).pluck(:id),
      workflow_definition_ids: [@srs_workflow.workflow_definition.id],
      ambition_ids: Ambition.all.limit(5).pluck(:id),
      tab_category: "TODO",
      task_definition_ids: [@srs_task.task_definition.id],
      order: "created_at_desc"
    }

    get api_tasks_url(params.merge(format: :json))
    assert_response :success
  end

  test "list of tasks should work" do
    CustomElasticSearchConfig.reindex_and_refresh(Task)
    get list_api_tasks_url(format: :json)
    assert_response :success
  end

  test "list of tasks should work with filters" do
    CustomElasticSearchConfig.reindex_and_refresh(Task)
    get list_api_tasks_url(format: :json), params: {except: [@srs_task.id], query: "srs"}
    assert_response :success
  end

  test "should show task" do
    get api_task_url(@srs_task, format: :json)
    assert_response :success
  end

  test "should update_assignee" do
    patch update_assignee_api_task_path(@srs_task, format: :json), params: {assignee: @current_user.id}
    assert_response :success
  end

  test "should allow removing assignee" do
    patch update_assignee_api_task_path(@srs_task, format: :json), params: {assignee: nil}
    assert_response :success
  end

  test "should update_contributors" do
    patch update_contributors_api_task_path(@srs_task, format: :json), params: {contributors: User.all.limit(3).pluck(:id)}
    assert_response :success
  end

  test "should allow removing all contributors" do
    patch update_contributors_api_task_path(@srs_task, format: :json), params: {contributors: []}
    assert_response :success
  end

  test "update name of a task" do
    patch api_task_url(@srs_task, format: :json), params: {task: {name: "updated name"}}
    assert_response :success
    assert @srs_task.reload.name == "updated name"
  end

  test "update description of a task" do
    patch api_task_url(@srs_task, format: :json), params: {task: {description: "updated description"}}
    assert_response :success
    assert @srs_task.reload.description == "updated description"
  end

  test "update name and description of a task" do
    patch api_task_url(@srs_task, format: :json), params: {task: {name: "updated_name", description: "updated description"}}
    assert_response :success
    assert @srs_task.reload.name == "updated_name"
    assert @srs_task.reload.description == "updated description"
  end

  test "update due date of a task" do
    patch update_due_date_api_task_url(@srs_task, format: :json), params: {due_at: Date.today}
    assert_response :success
  end

  test "remove due date of a task" do
    patch update_due_date_api_task_url(@srs_task, format: :json), params: {due_at: nil}
    assert_response :success
  end

  test "mark a task" do
    patch update_marked_api_task_url(@srs_task, format: :json), params: {marked: true}
    assert_response :success
  end

  test "start a task" do
    workflow = WorkflowDefinition.first.create_workflow!
    task = workflow.items.first

    patch start_api_task_url(task, format: :json)
    assert_response :success
  end

  test "complete a task" do
    workflow = WorkflowDefinition.first.create_workflow!

    task = workflow.items.first
    task.start!

    patch complete_api_task_url(task, format: :json)
    assert_response :success
  end

  test "get and update business data of a task" do
    get data_api_task_url(@srs_task, format: :json)
    assert_response :success
    content_item = @srs_task.task_items.first.content_item
    patch data_api_task_url(@srs_task, format: :json), params: {item_id: content_item.id, value: "a new value"}
    assert_response :success
  end

  test "add a file to a task" do
    srs_description_task = Task.find(68)
    content_item = srs_description_task.task_items.map(&:content_item).find { |ci| ci.content_type.name === ContentTypes::File.name }

    patch data_api_task_url(@srs_task, format: :json), params: {item_id: content_item.id, value: [UploadedFile.first.to_global_id.to_s]}

    assert_response :success
  end

  # Access Forbidden Tests

  test "no_access_user shouldn't be able to assign a user to a task" do
    login_as(@no_access_user)
    patch update_assignee_api_task_url(@srs_task, format: :json), params: {assignee: @no_access_user.id}
    assert_response :forbidden
  end

  test "no_access_user shouldn't be able to update the contributors of a task" do
    login_as(@no_access_user)
    patch update_contributors_api_task_url(@srs_task, format: :json), params: {contributors: [@no_access_user.id]}
    assert_response :forbidden
  end

  test "no_access_user shouldn't be able to update description of a task" do
    login_as(@no_access_user)
    patch api_task_url(@srs_task, format: :json), params: {description: "updated description"}
    assert_response :forbidden
  end

  test "no_access_user shouldn't be able to update due date of a task" do
    login_as(@no_access_user)
    patch update_due_date_api_task_url(@srs_task, format: :json), params: {due_at: Date.today}
    assert_response :forbidden
  end

  test "no_access_user shouldn't be able to get and update business data of a task" do
    login_as(@no_access_user)
    get data_api_task_url(@srs_task, format: :json)
    assert_response :forbidden
    patch data_api_task_url(@srs_task, format: :json), params: {item_id: "reference", value: "test reference"}
    assert_response :forbidden
  end

  test "no_access_user shouldn't be able to complete a task" do
    login_as(@no_access_user)
    patch complete_api_task_url(@srs_task, format: :json)
    assert_response :forbidden
  end

  test "no_access_user shouldn't be able to mark a task" do
    login_as(@no_access_user)
    patch update_marked_api_task_url(@srs_task, format: :json), params: {marked: true}
    assert_response :forbidden
  end

  test "no_access_user shouldn't be able to clone a task" do
    login_as(@no_access_user)
    patch clone_api_task_url(@srs_task, format: :json)
    assert_response :forbidden
  end

  # Moving tests

  test "task from end should be movable to beginning" do
    movee = @srs_workflow.direct_tasks.last
    assert_operator @srs_workflow.items.index(movee), :>, 0
    assert_not_nil movee
    patch move_api_task_url(movee, format: :json), params: {index: 0}
    assert_response :success
    assert @srs_workflow.reload.items.first.id == movee.id
  end

  test "tasks from end should be movable to middle" do
    movee = @srs_workflow.direct_tasks.last
    assert_operator @srs_workflow.items.index(movee), :!=, 1
    assert_not_nil movee
    patch move_api_task_url(movee, format: :json), params: {index: 1}
    assert_response :success
    assert @srs_workflow.reload.items.second.id == movee.id
  end

  test "tasks from beginning should be movable" do
    movee = @srs_workflow.direct_tasks.first
    assert_operator @srs_workflow.items.index(movee), :!=, 2
    assert_not_nil movee
    patch move_api_task_url(movee, format: :json), params: {index: 2}
    assert_response :success
    assert @srs_workflow.reload.items.third.id == movee.id
  end

  test "tasks should be movable to end with higher index" do
    movee = @srs_workflow.direct_tasks.first
    new_index = @srs_workflow.items.length + 99
    assert_operator @srs_workflow.items.index(movee), :<, new_index
    assert_not_nil movee
    patch move_api_task_url(movee, format: :json), params: {index: new_index}
    assert_response :success
    assert @srs_workflow.reload.items.last == movee
  end

  test "task should be movable from workflow into block (beginning)" do
    @current_user = User.find_by!(email: "dsb@example.org")
    login_as(@current_user)

    @workflow = Workflow.find_by!(title: "MM-20")
    @block = @workflow.blocks.first
    assert_operator @block.items.length, :>=, 2

    movee = @workflow.direct_tasks.first
    assert_not_nil movee
    patch move_api_task_url(movee, format: :json), params: {index: 0, target_id: @block.id, target_type: @block.class.name.underscore}
    assert_response :success
    refute @workflow.reload.direct_tasks.include?(movee)
    assert @block.reload.direct_tasks.first == movee
  end

  test "task should be movable from workflow into block (end)" do
    @current_user = User.find_by!(email: "dsb@example.org")
    login_as(@current_user)

    @workflow = Workflow.find_by!(title: "MM-20")
    @block = @workflow.blocks.first
    assert_operator @block.items.length, :>=, 2

    movee = @workflow.direct_tasks.first
    assert_not_nil movee
    patch move_api_task_url(movee, format: :json), params: {index: @block.items.length, target_id: @block.id, target_type: @block.class.name.underscore}
    assert_response :success
    refute @workflow.reload.direct_tasks.include?(movee)
    assert @block.reload.direct_tasks.last == movee
  end

  test "task should be movable from workflow into block (middle)" do
    @current_user = User.find_by!(email: "dsb@example.org")
    login_as(@current_user)

    @workflow = Workflow.find_by!(title: "MM-20")
    @block = @workflow.blocks.first
    assert_operator @block.items.length, :>=, 2

    movee = @workflow.direct_tasks.first
    assert_not_nil movee
    patch move_api_task_url(movee, format: :json), params: {index: 1, target_id: @block.id, target_type: @block.class.name.underscore}
    assert_response :success
    refute @workflow.reload.direct_tasks.include?(movee)
    assert @block.reload.direct_tasks.second == movee
  end

  test "task should be movable from block into workflow (beginning)" do
    @current_user = User.find_by!(email: "dsb@example.org")
    login_as(@current_user)

    @workflow = Workflow.find_by!(title: "MM-20")
    block = @workflow.blocks.first
    movee = block.direct_tasks.first
    assert_not_nil movee

    patch move_api_task_url(movee, format: :json), params: {index: 0, target_id: @workflow.id, target_type: @workflow.class.name.underscore}
    assert_response :success
    assert @workflow.reload.direct_tasks.first == movee
    refute block.reload.direct_tasks.include?(movee)
  end

  test "task should be movable from block to same block" do
    @current_user = User.find_by!(email: "dsb@example.org")
    login_as(@current_user)
    workflow = Workflow.find_by!(title: "MM-20")
    block = workflow.blocks.first

    movee = block.direct_tasks.first
    refute workflow.direct_tasks.include?(movee)
    assert block.direct_tasks.include?(movee)
    assert block.direct_tasks.count > 1
    patch move_api_task_url(movee, format: :json), params: {index: 999, target_id: block.id, target_type: block.class.name.underscore}
    assert_response :success

    refute workflow.reload.direct_tasks.include?(movee)
    assert block.reload.direct_tasks.last == movee
    assert block.reload.items.last == movee
  end

  test "task should be movable from block to same block (without giving target)" do
    @current_user = User.find_by!(email: "dsb@example.org")
    login_as(@current_user)
    workflow = Workflow.find_by!(title: "MM-20")
    block = workflow.blocks.first

    movee = block.direct_tasks.first
    refute workflow.direct_tasks.include?(movee)
    assert block.direct_tasks.include?(movee)
    assert block.direct_tasks.count > 1
    patch move_api_task_url(movee, format: :json), params: {index: 999}
    assert_response :success

    refute workflow.reload.direct_tasks.include?(movee)
    assert block.reload.direct_tasks.last == movee
  end

  test "task should be movable from block to other block" do
    @current_user = User.find_by!(email: "dsb@example.org")
    login_as(@current_user)
    workflow = Workflow.find_by!(title: "MM-20")
    block1 = workflow.blocks.first
    block2 = workflow.blocks.last
    refute_equal block1, block2

    movee = block1.direct_tasks.first
    refute workflow.direct_tasks.include?(movee)
    assert block1.direct_tasks.include?(movee)
    refute block2.direct_tasks.include?(movee)

    patch move_api_task_url(movee, format: :json), params: {index: 1, target_id: block2.id, target_type: block2.class.name.underscore}
    assert_response :success

    assert block2.direct_tasks.include?(movee)
    refute block1.direct_tasks.include?(movee)
    refute workflow.reload.direct_tasks.include?(movee)
    assert block2.reload.direct_tasks.second == movee
  end

  test "add data field to task which already has task_items" do
    task = @srs_workflow.direct_tasks.first
    assert_not_nil task
    assert task.task_items.present?
    post add_data_field_api_task_url(task, format: :json), params: {
      content_item: {label: "Testdatafield", content_type: "text"},
      task_item: {required: false}
    }
    assert_response :success
    task.reload
    assert task.task_items.last.content_item.label == "Testdatafield"
    assert task.task_items.last.content_item.content_type == ContentTypes::Text
  end

  test "add data field to task which doesn't have task_items yet" do
    task = @srs_workflow.direct_tasks.last
    assert_not_nil task
    assert task.task_items.empty?
    post add_data_field_api_task_url(task, format: :json), params: {
      content_item: {label: "Testdatafield", content_type: "text"},
      task_item: {required: false}
    }
    assert_response :success
    task.reload
    assert task.task_items.last.content_item.label == "Testdatafield"
    assert task.task_items.last.content_item.content_type == ContentTypes::Text
  end

  test "shouldn't be possible to add a new data field to a non active workflow" do
    @completed_workflow = Workflow.find_by!(title: "PA-1")
    assert @completed_workflow.completed?

    task = @completed_workflow.direct_tasks.first
    assert_not_nil task
    assert task.task_items.present?
    post add_data_field_api_task_url(task, format: :json), params: {
      content_item: {label: "Testdatafield", content_type: "text"},
      task_item: {required: false}
    }
    assert_response :method_not_allowed
  end

  test "should be possible to clone a task" do
    assert_difference -> { Task.count } do
      patch clone_api_task_url(@srs_task, format: :json)
    end
    assert_response :success
  end

  test "should be possible to destroy a task" do
    assert_difference -> { Task.not_deleted.count }, -1 do
      delete api_task_url(@srs_task, format: :json)
    end
    assert_response :success
  end
end
