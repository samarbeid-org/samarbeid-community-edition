require "test_helper"

class Api::NotificationsControllerTest < ActionDispatch::IntegrationTest
  def setup
    @user = User.find_by!(email: "srs_user@example.org")
    login_as @user
  end

  test "should get index for new notifications" do
    get api_notifications_path(tab_category: "NEW", format: :json)
    assert_response :success
  end

  test "should get index for bookmarked notifications" do
    get api_notifications_path(tab_category: "BOOKMARKED", format: :json)
    assert_response :success
  end

  test "should get index for done notifications" do
    get api_notifications_path(tab_category: "DONE", format: :json)
    assert_response :success
  end

  test "should patch unread notification as done" do
    notification = @user.notifications.unread.first

    patch done_api_notification_path(notification, format: :json)
    assert_response :success
  end
end
