require "test_helper"

class Api::EventsControllerTest < ActionDispatch::IntegrationTest
  def setup
    @current_user = User.find_by!(email: "dsb@example.org")
    login_as @current_user
  end

  test "should get events for ambition" do
    object = Ambition.find_by!(title: "some-ambition")

    get api_events_path(format: :json), params: {
      object_gid: object.to_global_id.to_s
    }
    assert_response :success
  end

  test "should get events for workflow" do
    object = Workflow.find_by!(title: "MM-20")

    get api_events_path(format: :json), params: {
      object_gid: object.to_global_id.to_s
    }
    assert_response :success
  end

  test "should get events for task" do
    object = Workflow.find_by!(title: "MM-20").direct_tasks.first

    get api_events_path(format: :json), params: {
      object_gid: object.to_global_id.to_s
    }
    assert_response :success
  end

  test "shouldn't get events for an object which the user doesn't have access to" do
    object = Workflow.find_by!(title: "Maßnahme die überall vorkommt")

    get api_events_path(format: :json), params: {
      object_gid: object.to_global_id.to_s
    }
    assert_response :forbidden
  end

  test "should add comments to ambition" do
    object = Ambition.find_by!(title: "some-ambition")

    assert_difference -> { object.comments.count } do
      post create_comment_api_events_path(format: :json), params: {
        object_gid: object.to_global_id.to_s,
        message: "Ein neuer Kommentar"
      }
    end
    assert_response :success
  end

  test "should add comments to workflow" do
    object = Workflow.find_by!(title: "MM-20")

    assert_difference -> { object.comments.count } do
      post create_comment_api_events_path(format: :json), params: {
        object_gid: object.to_global_id.to_s,
        message: "Ein neuer Kommentar"
      }
    end
    assert_response :success
  end

  test "should add comments to task" do
    object = Workflow.find_by!(title: "MM-20").direct_tasks.first

    assert_difference -> { object.comments.count } do
      post create_comment_api_events_path(format: :json), params: {
        object_gid: object.to_global_id.to_s,
        message: "Ein neuer Kommentar"
      }
    end
    assert_response :success
  end

  test "should add comments to workflow_definition" do
    logout
    login_as User.find_by!(email: "team-admin@example.org")

    object = WorkflowDefinition.find_by!(name: "Veranstaltungsplanung")

    assert_difference -> { object.comments.count } do
      post create_comment_api_events_path(format: :json), params: {
        object_gid: object.to_global_id.to_s,
        message: "Ein neuer Kommentar"
      }
    end
    assert_response :success
  end

  test "should add comments to dossier" do
    object = Dossier.first

    assert_difference -> { object.comments.count } do
      post create_comment_api_events_path(format: :json), params: {
        object_gid: object.to_global_id.to_s,
        message: "Ein neuer Kommentar"
      }
    end
    assert_response :success
  end

  test "shouldn't be possible to add comments to an object which the user doesn't have access to" do
    object = Workflow.find_by!(title: "Maßnahme die überall vorkommt")

    assert_no_difference -> { object.comments.count } do
      post create_comment_api_events_path(format: :json), params: {
        object_gid: object.to_global_id.to_s,
        message: "Ein neuer Kommentar"
      }
    end
    assert_response :forbidden
  end

  test "should get obsolete-info for workflow" do
    event = Workflow.find_by!(title: "MM-20").events.first
    assert_equal "Events::AssignedEvent", event.type
    get obsolete_info_api_event_path(event)
    assert_response :success
  end

  test "should get updated obsolete-info for workflow" do
    workflow = Workflow.find_by!(title: "MM-20")
    event = workflow.events.first
    assert_equal "Events::AssignedEvent", event.type
    assert event.superseder_id.nil?

    get obsolete_info_api_event_path(event)
    assert_response :success
    assert_equal false, @response.parsed_body["obsolete"]

    patch update_assignee_api_workflow_path(workflow, format: :json), params: {assignee: nil}
    assert_response :success

    get obsolete_info_api_event_path(event)
    assert_response :success
    assert_equal true, @response.parsed_body["obsolete"]
    assert_equal workflow.events.last.id, @response.parsed_body["last_superseder"]["id"]
  end

  test "shouldn't get obsolete-info for workflow when object is not readable" do
    event = Workflow.find_by!(title: "Maßnahme die überall vorkommt").events.first

    get obsolete_info_api_event_path(event)
    assert_response :forbidden
  end
end
