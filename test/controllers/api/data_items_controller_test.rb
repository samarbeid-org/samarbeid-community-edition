require "test_helper"

class Api::DataItemsControllerTest < ActionDispatch::IntegrationTest
  def setup
    @current_user = User.find_by!(email: "dsb@example.org")
    login_as(@current_user)
    @workflow = Workflow.find_by!(title: "MM-20")
    @content_item1 = @workflow.content_items.find_by!(label: "E-Mail-Adresse Absender")
    @task_item1 = @content_item1.task_items.where(info_box: false).first
    @task = @task_item1.task
    @task_item2 = @task.task_items.second
  end

  test "label should be updatable (only)" do
    refute_equal "new label", @task_item1.content_item.label
    patch api_data_item_url(@task_item1, format: :json), params: {content_item: {label: "new label"}}
    assert_response :success
    assert_equal "new label", @task_item1.reload.content_item.label
  end

  test "required and infobox status should be updatable (only)" do
    refute_equal true, @task_item1.info_box
    refute_equal true, @task_item1.required
    patch api_data_item_url(@task_item1, format: :json), params: {task_item: {info_box: true, required: true}}
    assert_response :success
    @task_item1.reload
    assert_equal true, @task_item1.info_box
    assert_equal true, @task_item1.required
  end

  test "both label and required+infobox status should be updatable" do
    refute_equal "new label", @task_item1.content_item.label
    refute_equal true, @task_item1.info_box
    refute_equal true, @task_item1.required
    patch api_data_item_url(@task_item1, format: :json), params: {content_item: {label: "new label"}, task_item: {info_box: true, required: true}}
    assert_response :success
    assert_equal "new label", @task_item1.reload.content_item.label
    assert_equal true, @task_item1.info_box
  end

  test "merging of error messages for task_item and content_item" do
    refute_equal "new label", @task_item1.content_item.label
    refute_equal true, @task_item1.info_box
    patch api_data_item_url(@task_item1, format: :json), params: {content_item: {label: nil}, task_item: {info_box: "hi"}}
    assert_response :unprocessable_entity
    assert_not_nil @task_item1.reload.content_item.label
    assert_not_nil @task_item1.info_box
    response_hash = JSON.parse(response.body)
    assert_equal ["muss ausgefüllt werden"], response_hash["label"]

    # task_item doesn't validate anything. The "hi" in the api call is interpreted as true
    assert_equal true, @task_item1.info_box
    assert_equal 1, response_hash.size
  end

  test "deleting task_item which is not alone in its content_item" do
    get data_api_task_url(@task_item1.task, format: :json)
    response_hash = JSON.parse(response.body)
    data_field_infos = response_hash["dataFields"].find{_1["definition"]["task_item_id"] == @task_item1.id}
    assert_operator data_field_infos["definition"]["task_item_count"], :>, 1

    assert_difference -> { @content_item1.task_items.count }, -1 do
      delete api_data_item_url(@task_item1)
      assert_response :success
      assert_raises ActiveRecord::RecordNotFound do
        @task_item1.reload
      end
    end
    assert_nothing_raised do
      @content_item1.reload
    end
  end

  test "deleting task_item which is alone in its content_item" do
    @content_item1.task_items.where.not(id: @task_item1.id).destroy_all
    assert_equal 1, @content_item1.task_items.count

    get data_api_task_url(@task_item1.task, format: :json)
    response_hash = JSON.parse(response.body)
    data_field_infos = response_hash["dataFields"].find{_1["definition"]["task_item_id"] == @task_item1.id}
    assert_equal 1, data_field_infos["definition"]["task_item_count"]

    delete api_data_item_url(@task_item1)
    assert_response :success

    assert_raises ActiveRecord::RecordNotFound do
      @task_item1.reload
    end
    assert_raises ActiveRecord::RecordNotFound do
      @content_item1.reload
    end
  end

  test "deleting task_item which precondition for block is not allowed" do
    precondition_content_item = @workflow.blocks.first.content_item
    assert_not_nil precondition_content_item
    assert_equal 1, precondition_content_item.task_items.count
    precondition_task_item = precondition_content_item.task_items.first

    delete api_data_item_url(precondition_task_item)
    assert_response :unprocessable_entity
    response_hash = JSON.parse(response.body)
    assert_equal ["Dieses Feld wird in einem Block als Bedingung verwendet."], response_hash["base"]

    assert_nothing_raised do
      precondition_task_item.reload
    end
    assert_nothing_raised do
      precondition_content_item.reload
    end
    assert_nothing_raised do
      precondition_task_item.reload
    end
  end

  test "task item should be movable down (index_new > index_old)" do
    movee = @task_item1
    assert_not_nil movee

    other = @task_item2
    assert_not_nil other

    assert_equal 0, movee.position
    assert_equal 0, @task.task_items.index(movee)
    assert_not_nil other
    assert_equal 1, other.position
    assert_equal 1, @task.task_items.index(other)
    patch move_api_data_item_url(movee, format: :json), params: {index: 1}
    assert_response :success
    @task.reload
    movee.reload
    other.reload
    assert_equal 0, @task.task_items.index(other)
    assert_equal 1, @task.task_items.index(movee)

    # those positions are an implementation detail, could be any position as long as the index is correct
    assert_equal 1, other.position
    assert_equal 2, movee.position
  end

  test "task item should be movable up (index_new < index_old)" do
    movee = @task_item2
    assert_not_nil movee

    other = @task_item1
    assert_not_nil other

    assert_equal 1, movee.position
    assert_equal 1, @task.task_items.index(movee)
    assert_not_nil other
    assert_equal 0, other.position
    assert_equal 0, @task.task_items.index(other)
    patch move_api_data_item_url(movee, format: :json), params: {index: 0}
    assert_response :success

    @task.reload
    movee.reload
    other.reload
    assert_equal 1, @task.task_items.index(other)
    assert_equal 0, @task.task_items.index(movee)

    # those positions are an implementation detail, could be any position as long as the index is correct
    assert_equal 1, other.position
    assert_equal 0, movee.position
  end
end
