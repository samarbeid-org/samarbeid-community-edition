require "test_helper"

class Api::UploadedFilesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @uploaded_file = UploadedFile.find(4)
    @current_user = User.find_by!(email: "srs_user@example.org")
    @no_access_user = User.find_by!(email: "no_access_user@example.org")
    @srs_description_task = Task.find(61) # Beschreibung erstellen
    login_as(@current_user)
  end

  test "should create uploaded_file" do
    skip # TODO: Solve problem with attachment is nil when creating an uploaded_file via controller test
    fixture_file = fixture_file_upload("files/cairo-multiline.pdf", "application/pdf")
    assert_difference -> { UploadedFile.count } do
      post api_uploaded_files_url, as: :json, params: {
        file: fixture_file
      }
    end
    assert_response :success
  end

  test "should update uploaded_file" do
    patch api_uploaded_file_url(@uploaded_file, format: :json), params: {uploaded_file: {title: "A custom title"},
                                                                         acl_object_global_id: @srs_description_task.to_global_id.to_s}
    assert_response :success
  end

  test "no_access_user shouldn't be able to update a file of a task" do
    login_as(@no_access_user)
    patch api_uploaded_file_url(@uploaded_file, format: :json), params: {uploaded_file: {title: "A custom title"},
                                                                         acl_object_global_id: @srs_description_task.to_global_id.to_s}
    assert_response :forbidden
  end

  test "using a false global_id should not work" do
    patch api_uploaded_file_url(@uploaded_file, format: :json), params: {uploaded_file: {title: "A custom title"},
                                                                         acl_object_global_id: @current_user.to_global_id.to_s}
    assert_response :forbidden
  end
end
