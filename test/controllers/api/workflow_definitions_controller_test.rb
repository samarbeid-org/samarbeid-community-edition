require "test_helper"

class Api::WorkflowDefinitionsControllerTest < ActionDispatch::IntegrationTest
  def setup
    CustomElasticSearchConfig.reindex_and_refresh(WorkflowDefinition)
    @user = User.find_by!(email: "srs_user@example.org")
    @team_admin = User.find_by!(email: "team-admin@example.org")
    @workflow_definition = WorkflowDefinition.first
  end

  test "should get index" do
    login_as(@user)
    get api_workflow_definitions_url(format: :json)
    assert_response :success
  end

  test "should get list" do
    login_as(@user)
    get list_api_workflow_definitions_path(format: :json)
    assert_response :success
  end

  test "should get show" do
    login_as(@user)
    get api_workflow_definition_path(@workflow_definition, format: :json)
    assert_response :success
  end

  test "should create new workflow_definitions" do
    login_as(@team_admin)
    assert_difference -> { WorkflowDefinition.count } do
      post api_workflow_definitions_path(format: :json), params: {name: "A new name"}
    end
    assert_response :success
  end

  test "should update" do
    login_as(@team_admin)
    patch api_workflow_definition_path(@workflow_definition, format: :json),
      params: {name: "A new name", description: "Somethign different", group_ids: Group.pluck(:id)}
    assert_response :success
  end

  test "should destroy new workflow definition" do
    login_as(@team_admin)
    workflow_definition = WorkflowDefinition.create!(name: "Empty", groups: @team_admin.groups)
    assert_difference -> { WorkflowDefinition.count }, -1 do
      delete api_workflow_definition_path(workflow_definition, format: :json)
    end
    assert_response :success
  end
end
