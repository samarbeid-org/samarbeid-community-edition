require "test_helper"

class Api::PagesControllerTest < ActionDispatch::IntegrationTest
  test "should get page without authentication" do
    @page = Page.find_by!(slug: "impressum")

    get api_page_path(@page, format: :json)
    assert_response :success

    assert @response.parsed_body.key?("title")
    assert_equal @page.title, @response.parsed_body["title"]

    assert @response.parsed_body.key?("content")
    assert_equal @page.content, @response.parsed_body["content"]
  end
end
