require "test_helper"

class Api::UserSettingsControllerTest < ActionDispatch::IntegrationTest
  def setup
    @user = User.find_by!(email: "srs_user@example.org")
    @admin = User.find_by!(email: "team-admin@example.org")
  end

  test "non admin shouldn't get other user settings" do
    login_as @user
    get api_user_setting_path(@admin, format: :json)
    assert_response :forbidden
  end

  test "non admin should get his own settings" do
    login_as @user
    get api_user_setting_path(@user, format: :json)
    assert_response :success
  end

  test "admin should get other user settings" do
    login_as @admin
    get api_user_setting_path(@user, format: :json)
    assert_response :success
  end

  test "non admin user shouldn't be able to update other user data" do
    login_as @user
    patch api_user_setting_path(@admin), as: :json, params: {
      firstname: "Test"
    }
    assert_response :forbidden
  end

  test "non admin user should be able to update his own data" do
    login_as @user
    patch api_user_setting_path(@user), as: :json, params: {
      firstname: "Test"
    }
    assert_response :success
  end

  test "admin user should be able to update other user data" do
    login_as @admin
    patch api_user_setting_path(@user), as: :json, params: {
      firstname: "Test"
    }

    assert_response :success
  end

  test "non admin user shouldn't be able to update admin attribute" do
    login_as @user
    assert_no_changes -> { @user.reload.is_admin? } do
      patch update_admin_status_api_user_setting_path(@user), as: :json, params: {
        admin: true
      }
    end
    assert_response :forbidden
  end

  test "admin user should be able to update admin attribute of other user" do
    login_as @admin
    assert_changes -> { @user.reload.is_admin? } do
      patch update_admin_status_api_user_setting_path(@user), as: :json, params: {
        admin: true
      }
    end
    assert_response :success
  end

  test "admin user shouldn't be able to update admin attribute of his self" do
    login_as @admin
    assert_no_changes -> { @admin.reload.is_admin? } do
      patch update_admin_status_api_user_setting_path(@admin), as: :json, params: {
        admin: true
      }
    end
    assert_response :forbidden
  end

  test "non admin user shouldn't be able to update password without submitting current_password" do
    login_as @user
    patch update_password_api_user_setting_path(@user), as: :json, params: {
      password: "test123",
      password_confirmation: "test123"
    }
    assert_response :unprocessable_entity
  end

  test "non admin user should be able to update password with submitting current_password" do
    login_as @user
    patch update_password_api_user_setting_path(@user), as: :json, params: {
      password: "test123",
      password_confirmation: "test123",
      current_password: "password"
    }
    assert_response :success

    # user is still signed in after password changed
    get api_user_setting_path(@user, format: :json)
    assert_response :success
  end

  test "admin user should be able to update password of other user without submitting current_password" do
    login_as @admin
    patch update_password_api_user_setting_path(@user), as: :json, params: {
      password: "test123",
      password_confirmation: "test123"
    }
    assert_response :success
  end

  test "admin user shouldn't be able to update his own password without submitting current_password" do
    login_as @admin
    patch update_password_api_user_setting_path(@admin), as: :json, params: {
      password: "test123",
      password_confirmation: "test123"
    }
    assert_response :unprocessable_entity
  end

  test "should get groups of a user" do
    login_as @user
    get groups_api_user_setting_path(@user, format: :json)
    assert_response :success
  end

  test "admin user should be able to update active status of other user" do
    login_as @admin
    assert_changes -> { @user.reload.deactivated_at } do
      patch update_active_status_api_user_setting_path(@user), as: :json, params: {
        active: false
      }
    end
    assert_response :success

    assert_changes -> { @user.reload.deactivated_at } do
      patch update_active_status_api_user_setting_path(@user), as: :json, params: {
        active: true
      }
    end
    assert_response :success
  end

  test "non admin user shouldn't be able to update active status of other user" do
    @non_admin_user = User.find_by!(email: "dsb@example.org")
    refute_equal @user, @non_admin_user

    login_as @non_admin_user
    assert_no_changes -> { @user.reload.deactivated_at } do
      patch update_active_status_api_user_setting_path(@user), as: :json, params: {
        active: false
      }
    end
    assert_response :forbidden
  end

  test "admin user shouldn't be able to update active status of itself" do
    login_as @admin
    assert_no_changes -> { @admin.reload.deactivated_at } do
      patch update_active_status_api_user_setting_path(@admin), as: :json, params: {
        active: false
      }
    end
    assert_response :forbidden
  end
end
