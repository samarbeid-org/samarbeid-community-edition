require "test_helper"

class SearchIndexSyncTest < ActiveSupport::TestCase
  include ActiveJob::TestHelper

  def setup
    @user = User.find_by!(email: "srs_user@example.org")
    @a_new_workflow = WorkflowDefinition.find_by!(name: "Patentanmeldung").create_workflow!
    @reindex_job_for_workflow = {job: Searchkick::ReindexV2Job, args: [@a_new_workflow.class.name, @a_new_workflow.id.to_s, nil, {routing: nil}]}
    @a_new_task = @a_new_workflow.all_tasks.ready.first
    @reindex_job_for_workflows_task = {job: Searchkick::ReindexV2Job, args: [@a_new_workflow.all_tasks.first.class.name, @a_new_task.id.to_s, nil, {routing: nil}]}
    @new_tasks_content_item = @a_new_task.task_items.first.content_item
    @a_new_ambition = Ambition.create!(title: "Some new goal")
    @reindex_job_for_ambition = {job: Searchkick::ReindexV2Job, args: [@a_new_ambition.class.name, @a_new_ambition.id.to_s, nil, {routing: nil}]}
    @a_workflow_definition = WorkflowDefinition.first
    @reindex_job_for_workflow_definition = {job: Searchkick::ReindexV2Job, args: [@a_workflow_definition.class.name, @a_workflow_definition.id.to_s, nil, {routing: nil}]}
  end

  test "ambitions should be reindexed when visibility or search relevant changes occur" do
    assert_enqueued_with(**@reindex_job_for_ambition) { @a_new_ambition.update(assignee: @user) }
    assert_enqueued_with(**@reindex_job_for_ambition) { @a_new_ambition.contributors << @user }
    assert_enqueued_with(**@reindex_job_for_ambition) { @a_new_ambition.update(deleted: true) }
    assert_enqueued_with(**@reindex_job_for_ambition) { @a_new_ambition.update(description: "Eine neue Beschreibung") }
    assert_enqueued_with(**@reindex_job_for_ambition) { Comment.create!(message: "a new comment", author: @user, object: @a_new_ambition) }

    # Workflows should also be reindexed when Ambitions are marked as deleted
    @a_new_ambition.update(workflows: [@a_new_workflow])
    assert_enqueued_with(**@reindex_job_for_workflow) { @a_new_ambition.update(deleted: true) }
  end

  test "ambitions.workflows<< should trigger reindex" do
    assert_enqueued_with(**@reindex_job_for_ambition) do
      assert_enqueued_with(**@reindex_job_for_workflow) do
        @a_new_ambition.workflows << @a_new_workflow
      end
    end
  end

  test "ambitions.workflows= should trigger reindex" do
    assert_enqueued_with(**@reindex_job_for_ambition) do
      assert_enqueued_with(**@reindex_job_for_workflow) do
        @a_new_ambition.workflows = [@a_new_workflow]
      end
    end
  end

  test "workflow should be reindexed when visibility or search  relevant changes occur" do
    assert_enqueued_with(**@reindex_job_for_workflow) { @a_new_workflow.update(title: "Another new title") }
    assert_enqueued_with(**@reindex_job_for_workflow) { @a_new_workflow.update(description: "Eine neue Beschreibung") }
    assert_enqueued_with(**@reindex_job_for_workflow) { Comment.create!(message: "a new comment", author: @user, object: @a_new_workflow) }

    assert_enqueued_with(**@reindex_job_for_workflow) { @new_tasks_content_item.update(value: "change the value") }
    assert_enqueued_with(**@reindex_job_for_workflow) { @new_tasks_content_item.destroy }

    assert_enqueued_with(**@reindex_job_for_workflow) do
      assert_enqueued_with(**@reindex_job_for_workflows_task) do
        @a_new_workflow.update(assignee: @user)
      end
    end

    assert_enqueued_with(**@reindex_job_for_workflow) do
      assert_enqueued_with(**@reindex_job_for_workflows_task) do
        @a_new_workflow.update(assignee: nil)
      end
    end

    assert_enqueued_with(**@reindex_job_for_workflow) do
      assert_enqueued_with(**@reindex_job_for_workflows_task) do
        @a_new_workflow.trash!
      end
    end

    # After adding a ambition the ambition should be reindexed to show it's new Workflow Definition in search
    assert_enqueued_with(**@reindex_job_for_ambition) { @a_new_workflow.ambitions = [@a_new_ambition] }
  end

  test "workflow << methods should trigger reindex" do
    # Contributor may have visibility to workflow and task -> ACL update
    assert_enqueued_with(**@reindex_job_for_workflow) do
      assert_enqueued_with(**@reindex_job_for_workflows_task) do
        @a_new_workflow.contributors << @user
      end
    end

    # Ambition index contains workflow_definitions, maybe a new one was added
    # workflow contains list of ambitions as filter
    assert_enqueued_with(**@reindex_job_for_workflow) do
      assert_enqueued_with(**@reindex_job_for_ambition) do
        @a_new_workflow.ambitions << @a_new_ambition
      end
    end
  end

  test "workflow = methods should trigger reindex" do
    assert_enqueued_with(**@reindex_job_for_workflow) do
      assert_enqueued_with(**@reindex_job_for_workflows_task) do
        @a_new_workflow.contributors = [@user]
      end
    end

    assert_enqueued_with(**@reindex_job_for_workflow) do
      assert_enqueued_with(**@reindex_job_for_ambition) do
        @a_new_workflow.ambitions = [@a_new_ambition]
      end
    end
  end

  test "tasks should be reindexed when visibility or search relevant changes occur" do
    assert_enqueued_with(**@reindex_job_for_workflows_task) { @a_new_task.update(description: "Eine neue Beschreibung") }
    assert_enqueued_with(**@reindex_job_for_workflows_task) { Comment.create!(message: "a new comment", author: @user, object: @a_new_task) }
    assert_enqueued_with(**@reindex_job_for_workflows_task) { @a_new_task.marked_by_users << @user }
    assert_enqueued_with(**@reindex_job_for_workflows_task) { @a_new_task.marked_by_users = [] }
  end

  test "tasks AND workflow should be reindexed when visibility relevant changes occur concerning both" do
    assert_enqueued_with(**@reindex_job_for_workflow) do # If a user is allowed to see a single task this affects workflow ACL
      assert_enqueued_with(**@reindex_job_for_workflows_task) do
        @a_new_task.contributors << @user
      end
    end

    assert_enqueued_with(**@reindex_job_for_workflow) do # If a user is allowed to see a single task this affects workflow ACL
      assert_enqueued_with(**@reindex_job_for_workflows_task) do
        @a_new_task.contributors = []
      end
    end

    assert_enqueued_with(**@reindex_job_for_workflow) do # If a user is allowed to see a single task this affects workflow ACL
      assert_enqueued_with(**@reindex_job_for_workflows_task) do
        @a_new_task.update(assignee: @user)
      end
    end

    assert_enqueued_with(**@reindex_job_for_workflow) do # If a user is allowed to see a single task this affects workflow ACL
      assert_enqueued_with(**@reindex_job_for_workflows_task) do
        @a_new_task.update(assignee: nil)
      end
    end
  end

  test "task AND workflow should be reindex on taks state change" do
    assert_enqueued_with(**@reindex_job_for_workflow) do # If a user is allowed to see a single task this affects workflow ACL
      assert_enqueued_with(**@reindex_job_for_workflows_task) do
        @a_new_task.skip!
      end
    end
  end

  test "workflow definitions should be reindexed and trigger full reindex if their groups change " do
    group = @a_workflow_definition.groups.first

    assert_enqueued_with(**@reindex_job_for_workflow_definition) do
      @a_workflow_definition.update(name: "New Name")
    end

    assert_enqueued_with(job: RunAclReindexOnlyJob) do
      group.workflow_definitions = []
    end

    assert_enqueued_with(job: RunAclReindexOnlyJob) do
      group.workflow_definitions << @a_workflow_definition
    end

    assert_enqueued_with(job: RunAclReindexOnlyJob) do
      @a_workflow_definition.groups = []
    end

    assert_enqueued_with(job: RunAclReindexOnlyJob) do
      @a_workflow_definition.groups << group
    end
  end

  test "workflows should be reindexed if the definition has a name change" do
    assert_enqueued_with(job: RunReindexForWorkflowsJob) do
      @a_workflow_definition.update(name: "A new name")
    end
  end

  test "workflows should be reindexed if their content_items change" do
    content_item = @a_new_workflow.content_items.first
    assert_enqueued_with(**@reindex_job_for_workflow) do
      content_item.update(value: "hey - new value which is indexed on workflow")
    end
  end
end
