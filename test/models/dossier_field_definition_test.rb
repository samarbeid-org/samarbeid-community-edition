require "test_helper"

class DossierFieldDefinitionTest < ActiveSupport::TestCase
  def setup
    @dossier_definition = DossierDefinition.find_by!(name: "DOSSIER FÜR AUTOMATISIERTE TESTS")
    @dossier_field_definition = @dossier_definition.fields.find_by!(name: "String")
  end

  test "it should be possible to create a dossier field definition" do
    assert_difference -> { DossierFieldDefinition.count } do
      DossierFieldDefinition.create(definition: @dossier_definition, name: "Test", position: 99, content_type: ContentTypes::String.to_s)
    end
  end

  test "it should not be possible to save a dossier field definition without name" do
    assert_not @dossier_field_definition.update(name: "")
    assert @dossier_field_definition.errors.details[:name].any?
  end

  test "it should not be possible to save a dossier field definition without position" do
    assert_not @dossier_field_definition.update(position: nil)
    assert @dossier_field_definition.errors.details[:position].any?
  end

  test "dossier field definition names should be unique in the context of the dossier definition" do
    dossier_definition_new = DossierDefinition.create!(name: "test")

    dossier_field_definition_new = DossierFieldDefinition.create(definition: @dossier_definition, name: "String", position: 99, content_type: ContentTypes::String.to_s)
    assert dossier_field_definition_new.errors.details[:name].any?

    dossier_field_definition_new.definition = dossier_definition_new
    assert_difference -> { DossierFieldDefinition.count } do
      dossier_field_definition_new.save
    end
  end

  test "dossier field definition position should be unique in the context of the dossier definition" do
    dossier_definition_new = DossierDefinition.create!(name: "test")

    dossier_field_definition_new = DossierFieldDefinition.create(definition: @dossier_definition, name: "Test", position: @dossier_field_definition.position, content_type: ContentTypes::Integer.to_s)
    assert dossier_field_definition_new.errors.details[:position].any?

    dossier_field_definition_new.definition = dossier_definition_new
    assert_difference -> { DossierFieldDefinition.count } do
      dossier_field_definition_new.save
    end
  end

  test "it should not be possible to change the definition of a dossier field definition" do
    dossier_definition_new = DossierDefinition.create!(name: "test")

    assert_not @dossier_field_definition.update(definition: dossier_definition_new)
    assert @dossier_field_definition.errors.details[:definition].any?
  end

  test "fields of a dossier definition are order by position" do
    last_position = @dossier_definition.fields.max_by(&:position)&.position || 0
    dossier_field_definition_new = DossierFieldDefinition.create!(definition: @dossier_definition, name: "Test", position: last_position + 1, content_type: ContentTypes::Integer.to_s)
    assert_equal dossier_field_definition_new, @dossier_definition.fields.last

    @dossier_field_definition.update!(position: last_position + 2)
    assert_equal @dossier_field_definition, @dossier_definition.fields.reload.last
  end

  test "it should only be possible to change the content_type of a dossier to a String or child class and only if it already was String or child class" do
    assert @dossier_field_definition.content_type <= ContentTypes::String

    assert @dossier_field_definition.update(content_type: ContentTypes::Email.to_s)
    assert_empty @dossier_field_definition.errors

    assert @dossier_field_definition.update(content_type: ContentTypes::String.to_s)
    assert_empty @dossier_field_definition.errors

    refute @dossier_field_definition.update(content_type: ContentTypes::Time.to_s)
    assert_not_empty @dossier_field_definition.errors

    integer_field_definition = @dossier_definition.fields.find_by!(content_type: ContentTypes::Integer.to_s)
    refute integer_field_definition.update(content_type: ContentTypes::String.to_s)
    assert_not_empty integer_field_definition.errors
  end

  test "dossier field definition should have a label for rails admin" do
    assert_not_empty @dossier_field_definition.rails_admin_label
  end
end
