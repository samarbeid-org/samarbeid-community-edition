require "test_helper"

class EventTest < ActiveSupport::TestCase
  setup do
    @user = User.find_by!(email: "admin@example.org")
  end

  test "Event is not destroyed with user" do
    @user.events.destroy_all
    assert @user.events.none?

    event = Events::ChangedDueDateEvent.create!(subject: @user, object: Task.first, data: {new_due_date: Date.today})

    assert_equal 1, @user.reload.events.count

    @user.destroy
    assert_nil event.reload.subject
  end

  test "valid task changed due date event" do
    event = Events::ChangedDueDateEvent.new(subject: @user, object: Task.first, data: {new_due_date: Date.today})
    assert event.valid?
  end

  test "A fixture should exist for every event type" do
    ambition = Ambition.find_by!(title: "event-test-ambition")
    workflow = Workflow.find_by!(title: "event-test-workflow")
    workflow_tasks = workflow.tasks_ordered_in_structure
    workflow_definition = WorkflowDefinition.find_by!(name: "Neu erstellte Vorlage - zum ausprobieren")
    content_item = ContentItem.find(82)

    comment = ambition.comments.first

    # Note: We use only events from above specific workflow/ambition as those are also used in system tests
    events = Event.where(object: ambition)
      .or(Event.where(object: workflow))
      .or(Event.where(object: workflow_tasks))
      .or(Event.where(object: comment))
      .or(Event.where(object: workflow_definition))
      .or(Event.where(object: content_item))

    fixture_events = events.map { |e| e.read_attribute(:type).demodulize.underscore }.uniq

    available_events = Dir.glob(Rails.root.join("app", "models", "events", "*.rb")).map { |path| File.basename(path, ".rb") }

    assert_empty available_events - fixture_events, "Could not find fixtures for events #{available_events - fixture_events}"
    assert_empty fixture_events - available_events, "Found fixtures for nonexistent events #{available_events - fixture_events}"
  end

  test "Events should be valid and have a correct localized_object" do
    Event.all.each do |event|
      assert event.valid?, "Event #{event.id}: #{event.errors.full_messages}"

      lo = event.localized_object(true, @user, true)

      assert lo.key?(:action), "localized_object should have key :action"
      assert_kind_of String, lo[:action], "localized_object[:action] should be a String"
      refute_empty lo[:action], "localized_object[:action] shouldn't be empty"

      assert lo.key?(:avatar), "localized_object should have key :avatar"
      assert_kind_of Hash, lo[:avatar], "localized_object[:avatar] should be a Hash"
      assert lo[:avatar].key?(:icon) || lo[:avatar].key?(:label) || lo[:avatar].key?(:url), "localized_object[:avatar] should have have key :icon, :label or :url"

      assert lo.key?(:chunks), "localized_object should have key :chunks"
      assert_kind_of Array, lo[:chunks], "localized_object[:chunks] should be an Array"

      lo[:chunks].each do |chunk|
        assert chunk.key?(:text), "chunk should have key :text"
        assert_kind_of String, chunk[:text], "chunk[:text] should be a String"
        refute chunk[:text].starts_with?("translation missing:"), "chunk text should be translated: '#{chunk[:text]}'"
      end
    end
  end

  test "Events should have superseder support" do
    workflow = Workflow.find_by!(title: "PA-1")
    # this Workflow is first unassigned and later assigned
    unassigned_event = workflow.events.where(type: "Events::UnassignedEvent").first
    assigned_event = workflow.events.where(type: "Events::AssignedEvent").first
    assert_not_nil assigned_event
    assert_not_nil unassigned_event

    assert assigned_event.supersedee_candidates.first == unassigned_event

    # this chunk can be removed if we redo the fixtures and have superseders applied there
    assert_nil unassigned_event.superseder_id
    assigned_event.apply_supersedees
    unassigned_event.reload

    assert_not_nil unassigned_event.superseder_id
    assert_equal unassigned_event.superseder, assigned_event
    assert assigned_event.superseder_id.nil?
  end
end
