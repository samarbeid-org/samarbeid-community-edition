require "test_helper"

class WorkflowTest < ActiveSupport::TestCase
  test "workflows may be created for all supported types" do
    ambition = Ambition.create!(title: "My very own Ambition")
    WorkflowDefinition.all.each do |definition|
      workflow = definition.create_workflow!
      workflow.ambitions << ambition
      assert workflow.persisted?
    end
  end

  test "workflows may be built and persisted for all supported types" do
    WorkflowDefinition.all.each do |definition|
      new_workflow = definition.build_workflow
      new_workflow.title = "Test-Workflow"
      assert new_workflow.valid?, "WorkflowDefinition #{definition.name} does not build valid workflow"
      assert new_workflow.save!, "WorkflowDefinition #{definition.name} does not build valid workflow"
    end
  end

  test "after creating a workflow all tasks are created" do
    workflow_definition = WorkflowDefinition.find_by!(name: "Patentanmeldung")

    workflow = workflow_definition.create_workflow!

    assert_equal workflow_definition.all_task_definitions.count, workflow.all_tasks.count
  end

  test "a workflow without a title shouldn't be valid and titles should be uniq" do
    workflow = WorkflowDefinition.first.build_workflow
    refute workflow.valid?
    workflow.title = "Test-Workflow"
    assert workflow.valid?
    assert workflow.save!

    second_workflow = WorkflowDefinition.first.create_workflow!
    second_workflow.title = "Test-Workflow"
    assert second_workflow.valid?
    assert second_workflow.save!

    workflow_from_different_definition = WorkflowDefinition.last.create_workflow!
    workflow_from_different_definition.title = "Test-Workflow"
    assert workflow_from_different_definition.valid?
    assert workflow_from_different_definition.save!
  end

  test "destroying a workflow should be possible to allow deploying new workflow versions" do
    workflow = Workflow.find(13) # A workflow with attached files and comments triggering notifications

    assert_nothing_raised do
      workflow.destroy!
    end
    # Check all events/notifications for invalid data
    assert_nothing_raised do
      Event.all.map(&:type)
      Event.all.map(&:data)
    end
  end

  test "triggering state machine should work" do
    workflow = WorkflowDefinition.find(5).create_workflow! # NUR FÜR AUTOMATISIERTE TESTS: DSB Meldung Missbrauch

    assert workflow.active?
    workflow.complete!
    assert workflow.completed_at.present?
    workflow.trash!
    assert workflow.trashed?
    workflow.untrash!
    assert workflow.completed?
    workflow.reopen
    refute workflow.completed_at.present?
    assert workflow.active?
    workflow.trash!
    assert workflow.trashed?
    workflow.untrash!
    assert workflow.active?
    workflow.trash!
    assert workflow.trashed?
    workflow.delete_for_ever!
    assert workflow.deleted?
  end

  test "bang events on state machine should raise exception if workflow is invalid" do
    workflow = WorkflowDefinition.find(5).create_workflow!
    workflow.title = nil
    refute workflow.valid?

    assert_raise { workflow.trash! }

    workflow.title = "something"
    assert_nothing_raised { workflow.trash! }
  end

  test "workflow assignees should only be checked for access rights on change - workflows have to work with invalid access assignees" do
    workflow = Workflow.first
    no_access_user = User.find_by!(email: "no_access_user@example.org")

    refute workflow.update(assignee: no_access_user)

    workflow.update_columns(assignee_id: no_access_user)
    assert workflow.valid?
  end

  test "tasks should be returned in order" do
    workflow = WorkflowDefinition.find(5).create_workflow! # NUR FÜR AUTOMATISIERTE TESTS: DSB Meldung Missbrauch
    workflow.all_tasks.each { |t| t.update(position: rand(10_000)) } && workflow.reload # randomize order

    workflow.direct_tasks.each_cons(2) do |predecessor, successor|
      assert predecessor.position < successor.position
    end

    workflow.items.each_cons(2) do |predecessor, successor|
      assert predecessor.position < successor.position
    end

    workflow.blocks.each do |block|
      block.direct_tasks.each_cons(2) do |predecessor, successor|
        assert predecessor.position < successor.position, "#{block.name} failed for predecessor #{predecessor.name} and successor #{successor.name}"
      end
    end
  end

  test "workflows may have only assignees which have access" do
    user_with_no_access = User.find_by!(email: "no_access_user@example.org")
    workflow = Workflow.active.first

    workflow.assignee = user_with_no_access

    refute workflow.valid?
  end
end
