require "test_helper"

class UserTest < ActiveSupport::TestCase
  test "a users mention label should consist of an @ and his name" do
    any_user = User.first
    internal_reference_for_mentioning = any_user.mention_label
    assert internal_reference_for_mentioning.match(any_user.name)
    assert "@", internal_reference_for_mentioning.first
  end

  test "reorder_by_name_with_user_first scope should work" do
    user = User.order(:id).last

    assert_equal user, User.reorder_by_name_with_user_first(user).first
    refute_equal user, User.all.order(:id).first
    assert_equal user, User.all.order(:id).reorder_by_name_with_user_first(user).first
  end

  test "except_users scope should work" do
    assert_equal User.count, User.except_users([]).count

    user_to_exclude = User.first
    refute User.except_users([user_to_exclude.id]).all.include?(user_to_exclude)

    assert_equal 0, User.except_users(User.pluck(:id)).count
  end

  test "filtered_by_query scope should work" do
    assert_nothing_raised { User.filtered_by_query("").all }

    user = User.find_by!(email: "dsb@example.org")
    different_user = User.except_users([user.id]).first

    [user.id, user.name, user.firstname].each do |query|
      assert_equal user, User.filtered_by_query(query).first
      refute User.filtered_by_query(query).to_a.include?(different_user), "failed for #{query}"
    end
  end

  test "filtered_by_groups scope should work" do
    assert_nothing_raised { User.filtered_by_groups([]).all }

    group = Group.first
    second_group = Group.second
    user = group.users.first
    user.update!(groups: Group.all)

    assert_equal group.users.sort, User.filtered_by_groups([group.id]).sort

    assert_equal Group.all.flat_map { |g| g.users }.map(&:id).uniq.sort, User.filtered_by_groups(Group.all.pluck(:id)).pluck(:id).uniq.sort

    all_users_in_groups = User.filtered_by_groups([group.id, second_group.id])
    assert_equal all_users_in_groups.map(&:id).sort.uniq, all_users_in_groups.to_a.map(&:id).sort.uniq
    assert_equal all_users_in_groups.map(&:id).sort.uniq, User.filter_for_list(User.first, group_ids: [group.id, second_group.id]).map(&:id).sort
  end

  test "combining user scopes should work" do
    group = Group.first
    user = group.users.first
    different_user = User.except_users([user.id]).first
    query = user.name

    assert_equal user, User.filter_for_list(user, query: query, group_ids: [group.id]).first
    assert_empty User.filter_for_list(user, query: query, group_ids: [group.id], excluded_user_ids: [user.id])
    assert_equal user, User.filter_for_list(user, query: query, group_ids: [group.id], excluded_user_ids: [different_user.id]).first
  end

  test "admins should report correctly" do
    team_admins = User.where(role: :team_admin).all
    super_admins = User.where(role: :super_admin).all

    assert team_admins.first.is_admin?
    assert super_admins.first.is_admin?

    assert_equal (team_admins | super_admins).sort, User.admin.all.sort
  end
end
