require "test_helper"

class DossierTest < ActiveSupport::TestCase
  def setup
    @dossier_definition = DossierDefinition.find_by!(name: "DOSSIER FÜR AUTOMATISIERTE TESTS")
    @dossier = Dossier.find_by!("data @> ?", {get_field_from_name!("String").id.to_s.to_sym => "Die ist ein Teststring"}.to_json)
  end

  def get_field_from_name!(name)
    @dossier_definition.fields.find_by!(name: name)
  end

  test "it should be possible to create a dossier" do
    data = {
      "String" => "Test String",
      "Text" => "Test Text",
      "Integer" => 123456,
      "Boolean" => true,
      "Date" => "2021-03-07",
      "Richtext" => "<p>Test Richtext</p>"
    }

    assert_difference -> { Dossier.count } do
      Dossier.create(
        definition: @dossier_definition,
        field_data: data.map { |k, v| [get_field_from_name!(k).id, v] }.to_h
      )
    end
  end

  test "setting dossier value should touch updated_at field" do
    dossier = Dossier.find(59)
    Timecop.freeze do
      dossier.set_field_value(10, "neuer String")
      assert_equal Time.now.to_i, dossier.updated_at.to_i
    end
  end

  test "it should not be possible to create a dossier without definition" do
    dossier_new = Dossier.new(definition: nil)
    assert_not dossier_new.save
    assert dossier_new.errors.details[:definition].any?
  end

  test "it should not be possible to change the definition of a dossier" do
    dossier_definition_new = DossierDefinition.create!(name: "new")
    assert_not @dossier.update(definition: dossier_definition_new)
    assert @dossier.errors.details[:definition].any?
  end

  test "it should not be possible to change the created_by user of a dossier" do
    other_user = User.where.not(id: @dossier.created_by.id).first
    assert_not @dossier.update(created_by: other_user)
    assert @dossier.errors.details[:created_by].any?
  end

  test "it should not be possible to save a dossier without a required field" do
    field = get_field_from_name!("String")
    @dossier.set_field_value(field.id, nil)
    assert_not @dossier.save
    assert @dossier.errors.details["field_#{field.id}".to_sym].any?
  end

  test "it should not be possible to save a dossier with a already used value for a unique field" do
    field = get_field_from_name!("String")
    dossier_new = Dossier.new(definition: @dossier_definition)
    dossier_new.set_field_value(field.id, @dossier.get_field_value(field.id))
    assert_not dossier_new.save
    assert dossier_new.errors.details["field_#{field.id}".to_sym].any?
  end

  test "data_fields method should return an array of hash with field definition and value" do
    assert_equal @dossier.definition.fields.count, @dossier.data_fields(false).count
    required_or_recommend_data_fields = @dossier.data_fields(true)
    assert_equal @dossier.definition.fields.required_or_recommended.count, required_or_recommend_data_fields.count
    assert_instance_of Array, required_or_recommend_data_fields
    assert_instance_of Hash, required_or_recommend_data_fields.first
    required_or_recommend_data_fields.first.assert_valid_keys(:definition, :value)
  end

  test "title should return a join string of all values of definition title_fields or a default value" do
    assert_equal "Die ist ein Teststring", @dossier.title
    @dossier.definition.title_fields = []
    assert_equal "#{@dossier.id} • #{@dossier.definition.name}", @dossier.title
  end

  test "title should return a join string of all values of definition subtitle_fields or empty string" do
    assert_equal "dev.vuetifyjs.com • admin@example.org • Testtext, mit zweiter Zeile • Richtext mit bold, italic, Zitat • 99 • 99,99 • Ja • 01.03.2021 • 14:45 Uhr • 27.04.2021, 13:25 Uhr • Item 1, Item 3 • Admin User, Some Other User • cairo-multiline.pdf, table-screenshot.png, sample.docx", @dossier.subtitle
    @dossier.definition.subtitle_fields = []
    assert_equal "", @dossier.subtitle
  end

  test "data must be a hash" do
    @dossier.data = []
    assert_not @dossier.save
    assert @dossier.errors.details[:data].any?
  end

  test "#referenced_in should return correct workflows" do
    assert_equal [Workflow.find(16)], Dossier.find(24).referenced_in
  end
end
