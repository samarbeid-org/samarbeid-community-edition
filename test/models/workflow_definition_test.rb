require "test_helper"

class WorkflowDefinitionTest < ActiveSupport::TestCase
  include ActiveJob::TestHelper

  test "workflow defintions should be reindexed when visibility relevant changes occur" do
    workflow = WorkflowDefinition.first
    workflow.update(groups: [])
    group = Group.find_by!(name: "tomsy")
    assert_enqueued_with(job: RunAclReindexOnlyJob) { workflow.groups << group }
  end

  test "destroying workflow definitions should work to allow admins to clean up system" do
    assert_nothing_raised do
      definition = WorkflowDefinition.find_by!(name: "Veranstaltungsplanung") # has workflows with documents and notifications attached
      definition.destroy!
    end
  end

  test "building a new workflow should copy all data and attached items" do
    workflow_definition = WorkflowDefinition.find(9) # Bürgeranfrage
    new_workflow = workflow_definition.build_workflow

    assert_equal workflow_definition.version, new_workflow.definition_version
    assert_equal workflow_definition.item_definitions.map(&:name), new_workflow.items.map(&:name)

    # Blocks
    first_block_definition = workflow_definition.block_definitions.min_by(&:position)
    first_block = new_workflow.blocks.min_by(&:position)

    assert_equal first_block_definition.direct_task_definitions.sort_by(&:position).map(&:name),
      first_block.direct_tasks.sort_by(&:position).map(&:name)
    assert_equal first_block_definition.content_item_definition.label, first_block.content_item.label
    assert_equal first_block_definition.decision, first_block.decision

    # Content_items
    first_task_definition = workflow_definition.direct_task_definitions.min_by(&:position)
    first_task = new_workflow.direct_tasks.min_by(&:position)

    assert_equal first_task_definition.task_item_definitions.sort_by(&:position).map { |ti| [ti.position, ti.required, ti.info_box] },
      first_task.task_items.sort_by(&:position).map { |ti| [ti.position, ti.required, ti.info_box] }

    assert_equal first_task_definition.task_item_definitions.sort_by(&:position).map(&:content_item_definition).map(&:label),
      first_task.task_items.sort_by(&:position).map(&:content_item).map(&:label)
  end

  test "creating a new workflow should copy all attached items" do
    workflow_definition = WorkflowDefinition.find(9) # Bürgeranfrage
    new_workflow = workflow_definition.create_workflow!
    new_workflow.reload
    assert_equal workflow_definition.item_definitions.map(&:name), new_workflow.items.map(&:name)

    # Blocks
    first_block_definition = workflow_definition.block_definitions.min_by(&:position)
    first_block = new_workflow.blocks.min_by(&:position)

    assert_equal first_block_definition.direct_task_definitions.sort_by(&:position).map(&:name),
      first_block.direct_tasks.sort_by(&:position).map(&:name)
    assert_equal first_block_definition.content_item_definition.label, first_block.content_item.label
    assert_equal first_block_definition.decision, first_block.decision

    # Content_items
    first_task_definition = workflow_definition.direct_task_definitions.min_by(&:position)
    first_task = new_workflow.direct_tasks.min_by(&:position)

    assert_equal first_task_definition.task_item_definitions.sort_by(&:position).map { |ti| [ti.position, ti.required, ti.info_box] },
      first_task.task_items.sort_by(&:position).map { |ti| [ti.position, ti.required, ti.info_box] }

    assert_equal first_task_definition.task_item_definitions.sort_by(&:position).map(&:content_item_definition).map(&:label),
      first_task.task_items.sort_by(&:position).map(&:content_item).map(&:label)
  end

  test "creating a new workflow should link all definitions" do
    workflow_definition = WorkflowDefinition.find(9) # Bürgeranfrage
    new_workflow = workflow_definition.create_workflow!
    new_workflow.reload
    assert_equal workflow_definition, new_workflow.workflow_definition

    # Blocks
    first_block_definition = workflow_definition.block_definitions.min_by(&:position)
    first_block = new_workflow.blocks.min_by(&:position)

    assert_equal first_block_definition, first_block.block_definition

    # TaskItems
    first_task_item_definition = workflow_definition.direct_task_definitions.min_by(&:position).task_item_definitions.min_by(&:position)
    first_task_item = new_workflow.direct_tasks.min_by(&:position).task_items.min_by(&:position)

    assert_equal first_task_item_definition, first_task_item.task_item_definition

    first_content_item_definition = first_task_item_definition.content_item_definition
    first_content_item = first_task_item.content_item

    assert_equal first_content_item_definition, first_content_item.content_item_definition
  end

  test "creating a new workflow should not instantiate any soft-deleted items" do
    workflow_definition = WorkflowDefinition.find(9) # Bürgeranfrage
    workflow_definition.content_item_definitions.each { |ci| ci.update(deleted: true) }
    workflow_definition.item_definitions.each { |ci| ci.update(deleted: true) }

    workflow = workflow_definition.reload.build_workflow

    assert_empty workflow.items
    assert_empty workflow.content_items
  end

  test "a empty workflow definition should be startable" do
    assert_nothing_raised do
      wd = WorkflowDefinition.create!(name: "Leere Maßnahmevorlage")
      wd.create_workflow!
    end
  end
end
