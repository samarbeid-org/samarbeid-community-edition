require "test_helper"

class TaskTest < ActiveSupport::TestCase
  include ActiveJob::TestHelper

  setup do
    @user = User.find_by!(email: "admin@example.org")
  end

  test "User assigned task is not destroyed with user" do
    @user.assigned_tasks.clear
    assert @user.assigned_tasks.none?

    workflow = WorkflowDefinition.find_by!(name: "Erstgespräch Erfindung").create_workflow!
    workflow.ambitions << Ambition.create(title: "Blubb Ziel")

    task = workflow.direct_tasks.first
    task.update(assignee: @user)

    assert_equal 1, @user.reload.assigned_tasks.count

    @user.destroy
    assert_nil task.reload.assignee
  end

  test "#marked_counts should work" do
    CustomElasticSearchConfig.reindex_and_refresh(Task)

    marked_count_query = Task.marked_count_query(@user, {where: {tab_categories: "TODO"}})
    Searchkick.multi_search([marked_count_query])

    assert_equal @user.marked_tasks.count, marked_count_query.total_count
  end

  test "custom scopes should work" do
    assert_nothing_raised do
      Task.current_and_assigned_to(User.first).first
      Task.with_due_in_past.first
    end
  end

  test "current_and_assigned_to scope should work as expected" do
    user = User.first
    new_workflow = WorkflowDefinition.find(5).create_workflow!
    ready_task = new_workflow.direct_tasks.ready.first
    created_task = new_workflow.direct_tasks.created.first

    assert_difference -> { Task.current_and_assigned_to(user).count } do
      ready_task.update!(assignee: user)
    end

    assert_difference -> { Task.current_and_assigned_to(user).count } do
      created_task.update!(assignee: user)
      created_task.start!
    end

    assert_difference -> { Task.current_and_assigned_to(user).count }, -1 do
      new_workflow.trash!
    end
    new_workflow.untrash!
    assert_difference -> { Task.current_and_assigned_to(user).count }, -1 do
      new_workflow.complete!
    end

    assert_difference -> { Task.current_and_assigned_to(User.find_by!(email: "no_access_user@example.org")).count }, 0 do
      task_for_which_user_has_no_access = WorkflowDefinition.find(5).create_workflow!.direct_tasks.first
      task_for_which_user_has_no_access.update!(assignee: user)
      task_for_which_user_has_no_access.start!
    end
  end

  test "task should have due_at set automatically if specified" do
    test_time = Time.new(2021)
    Timecop.freeze(test_time) do
      workflow = WorkflowDefinition.find(5).create_workflow!

      task_due_in_6 = workflow.items.first
      task_due_in_24 = workflow.items.second

      assert task_due_in_6.ready?
      assert task_due_in_6.due?
      # Currently we only track due_at with daily accuracy. If this changes
      # this assertion should be changed
      Timecop.freeze(test_time - 1.day) { refute task_due_in_6.due? }

      refute task_due_in_24.due_at
      task_due_in_24.assignee = @user
      task_due_in_24.enable!
      task_due_in_24.start!
      assert task_due_in_24.active?
      refute task_due_in_24.due?
      Timecop.freeze(test_time + 2.days) { assert task_due_in_24.due? }
    end
  end

  test "triggering state machine should work as expected" do
    workflow = WorkflowDefinition.find(5).create_workflow!
    task = workflow.items.first

    assert task.is_a?(Task) # just in case
    assert task.ready?

    task.start!
    assert task.active?

    task.snooze!
    assert task.ready?
    task.start!

    # Tasks for completed Workflows should not allow any transitions anymore
    workflow.complete!
    refute task.may_complete?
    refute task.may_skip?
    refute task.may_delete_for_ever?
    workflow.reopen!
    assert task.may_complete?
    assert task.may_skip?
    assert task.may_delete_for_ever?
    # Return to normal workflow in active workflow

    task.complete!
    assert task.completed?
    task.reopen!
    assert task.active?
    task.skip!
    assert task.skipped?
    task.reopen!
    assert task.active?
    task.delete_for_ever!
    assert task.deleted?
  end

  test "tasks may have only assignees which have access" do
    user_with_no_access = User.find_by!(email: "no_access_user@example.org")
    task = Task.active.first

    task.assignee = user_with_no_access

    refute task.valid?
  end

  test "new tasks should have their definitions name and description" do
    workflow = WorkflowDefinition.find(5).create_workflow!
    workflow.all_tasks.each do |task|
      assert_equal task.task_definition.name, task.name
      assert_equal task.task_definition.description, task.description
    end
  end
end
