require "test_helper"

class UploadedFileTest < ActiveSupport::TestCase
  test "pdf files should be converted to text" do
    uploaded_file = create_uploaded_file_with("test/fixtures/files/cairo-multiline.pdf")

    assert uploaded_file.file_as_text.match("From James")
  end

  test "umlaute in pdfs should be convereted correctly" do
    uploaded_file = create_uploaded_file_with("test/fixtures/files/test-umlaute.pdf")

    assert uploaded_file.file_as_text.match("Test Umläute!")
  end

  test "docx files should be converted to text" do
    uploaded_file = create_uploaded_file_with("test/fixtures/files/sample.docx")

    assert uploaded_file.file_as_text.match("The quick brown fox jumped over the lazy cat.")
  end

  test "image files should be converted to text" do
    uploaded_file = create_uploaded_file_with("test/fixtures/files/table-screenshot.png")

    assert uploaded_file.file_as_text.match("Zwischenergebnisse")
  end

  test "to_text should be able to extract text from existing and new uploaded files" do
    uploaded_file = create_uploaded_file_with("test/fixtures/files/test-umlaute.pdf")
    assert uploaded_file.file_as_text
    uploaded_file.update!(file_as_text: nil) # so to_text really has to run once more
    assert uploaded_file.file_as_text
  end

  test "to_text should handle huge files gracefully and limit text length" do
    Tempfile.create do |huge_file|
      huge_file << "X" * 6_000_000
      uploaded_file = create_uploaded_file_with(huge_file.path)
      assert_equal "XXXX [length limited to 5 million chars]", uploaded_file.file_as_text.last(40)
    end
  end

  test "title should be extracted from filename" do
    filename = "cairo-multiline.pdf"
    uploaded_file = create_uploaded_file_with("test/fixtures/files/#{filename}")
    assert_equal filename, uploaded_file.title
  end

  test "title may be set to custom value" do
    uploaded_file = create_uploaded_file_with("test/fixtures/files/cairo-multiline.pdf")
    a_new_title = "this seems a nicer title!"
    uploaded_file.update(title: a_new_title)
    assert_equal a_new_title, uploaded_file.reload.title
  end

  test "file size should be limited" do
    Tempfile.create do |gigantic_file|
      gigantic_file << `head -c 100M /dev/urandom`
      uploaded_file = create_uploaded_file_with(gigantic_file.path)
      refute uploaded_file.valid?
    end
  end

  private

  def create_uploaded_file_with(file_path)
    uploaded_file = UploadedFile.create!
    File.open(file_path, "rb") do |pdf_test_file|
      uploaded_file.file.attach(io: pdf_test_file, filename: file_path.split("/").last)
    end
    uploaded_file
  end
end
