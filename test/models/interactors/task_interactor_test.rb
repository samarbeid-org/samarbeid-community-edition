require "test_helper"

class Interactors::TaskInteractorTest < ActiveSupport::TestCase
  include ActiveJob::TestHelper

  def setup
    @user = User.find_by!(email: "admin@example.org")

    @running_workflow = WorkflowDefinition.first.create_workflow!
    @ready_task = @running_workflow.direct_tasks.ready.first
  end

  test "#update_assignee with a user value should set the assignee of the task, add contributor if necessary and create an event" do
    assert_difference -> { Events::AssignedEvent.for_object(@ready_task).count } do
      Interactors::TaskInteractor.update_assignee(@ready_task, @user, @user)
    end

    assert_equal @user, @ready_task.assignee
    assert @ready_task.contributors.include?(@user)
  end

  test "#update_assignee with a nil value should clear the assignee of the task, add contributor if necessary and create an event" do
    @ready_task.update!(assignee: @user)

    assert_difference -> { Events::UnassignedEvent.for_object(@ready_task).count } do
      Interactors::TaskInteractor.update_assignee(@ready_task, nil, @user)
    end

    assert_nil @ready_task.assignee
    assert @ready_task.contributors.include?(@user)
  end

  test "#update_contributors should add and remove users as contributors of the task if necessary and create an event" do
    removed_contributor = User.find_by!(email: "srs_user@example.org")
    @ready_task.contributors << removed_contributor

    assert_difference -> { Events::AddedContributorEvent.for_object(@ready_task).count } do
      assert_difference -> { Events::RemovedContributorEvent.for_object(@ready_task).count } do
        Interactors::TaskInteractor.update_contributors(@ready_task, User.where(id: @user.id), @user)
      end
    end

    assert @ready_task.contributors.include?(@user)
    assert_not @ready_task.contributors.include?(removed_contributor)
  end

  test "#update_contributors should remove current_user from the contributors of the task and create an event" do
    @ready_task.contributors << @user
    assert @ready_task.contributors.include?(@user)

    assert_difference -> { Events::RemovedContributorEvent.for_object(@ready_task).count } do
      Interactors::TaskInteractor.update_contributors(@ready_task, @ready_task.contributors - [@user], @user)
    end

    assert_not @ready_task.contributors.include?(@user)
  end

  test "#update_due_at should change the due_at attribute of the task, add contributor if necessary and create an event" do
    new_due_date = Date.today

    assert_difference -> { Events::ChangedDueDateEvent.for_object(@ready_task).count } do
      Interactors::TaskInteractor.update_due_at(@ready_task, new_due_date.to_s, @user)
    end

    assert_equal new_due_date, @ready_task.due_at
    assert @ready_task.contributors.include?(@user)
  end

  test "#complete should complete the task and create an event" do
    @ready_task.assignee = @user
    @ready_task.start
    assert_difference -> { Events::CompletedEvent.for_object(@ready_task).count } do
      Interactors::TaskInteractor.complete(@ready_task, @user)
    end

    assert @ready_task.reload.completed?
  end

  test "#start should start the task and create an event (and only one)" do
    assert_difference -> { Events::StartedEvent.for_object(@ready_task).count } do
      Interactors::TaskInteractor.start(@ready_task, @user)
    end
    assert_equal @user, Events::StartedEvent.for_object(@ready_task).first.subject
    assert @ready_task.reload.active?
  end

  test "#snooze should return task to ready state" do
    @ready_task.start!

    Interactors::TaskInteractor.snooze(@ready_task, nil, @user)
    assert @ready_task.reload.ready?
    assert @ready_task.start_at.nil?

    @ready_task.start!

    Interactors::TaskInteractor.snooze(@ready_task, 3.days.from_now, @user)
    assert @ready_task.reload.ready?
    refute @ready_task.start_at.nil?
  end

  test "#mark should mark my task" do
    Interactors::TaskInteractor.update_marked(@ready_task, true, @user)
    assert @ready_task.reload.marked?(@user)
    assert_nothing_raised do
      Interactors::TaskInteractor.update_marked(@ready_task, true, @user)
    end
    Interactors::TaskInteractor.update_marked(@ready_task, false, @user)
    refute @ready_task.reload.marked?(@user)
  end

  test "#update the description of the task with mentioning a user should work" do
    other_user = User.find_by!(email: "srs_user@example.org")

    params = ActionController::Parameters.new({
      task: {
        description: "<p>Changed Description <mention m-id=\"#{other_user.id}\" m-type=\"user\"></mention></p>"
      }
    })

    assert_difference -> { Events::ChangedSummaryEvent.for_object(@ready_task).count } do
      Interactors::TaskInteractor.update(@ready_task, params.require(:task).permit(:description), @user)
    end

    assert_equal params[:task][:description], @ready_task.description
    assert @ready_task.contributors.include?(@user)
    assert Events::ChangedSummaryEvent.for_object(@ready_task).last.mentioned_users.include?(other_user)
  end

  test "#update_content_item should update the content_item, add contributor and start task if necessary" do
    new_text = "let's fill in something new"
    created_task = @running_workflow.direct_tasks.created.last
    content_item = created_task.task_items.first.content_item

    assert_difference -> { Events::ChangedDataEvent.for_object(content_item).count } do
      assert_difference -> { Events::StartedEvent.for_object(created_task).count } do
        Interactors::TaskInteractor.update_content_item(created_task, content_item, new_text, @user)
      end
    end

    assert_equal new_text, content_item.reload.value
    assert created_task.contributors.include?(@user)
    assert created_task.active?
    assert_equal @user, created_task.assignee
  end

  test "#update name and description" do
    params = ActionController::Parameters.new({
      task: {
        name: "New task name",
        description: "<p>A new description</p>"
      }
    })

    assert_difference -> { Events::ChangedSummaryEvent.for_object(@ready_task).count } do
      # FIXME: as soon as we have a separate renamed event or a combined event, its count should be checked here.
      Interactors::TaskInteractor.update(@ready_task, params.require(:task).permit(:name, :description), @user)
    end

    assert_equal params[:task][:name], @ready_task.name
    assert_equal params[:task][:description], @ready_task.description
    # FIXME: assert_equal params[:task][:name], Events::RenamedEvent.for_object(@ready_task).last.new_name
    assert_equal params[:task][:description], Events::ChangedSummaryEvent.for_object(@ready_task).last.new_summary
  end

  test "#delete should delete task and do everything necessary" do
    assert_enqueued_with(job: Searchkick::ReindexV2Job) do
      assert_difference -> { Events::DeletedEvent.for_object(@ready_task).count } do
        Interactors::TaskInteractor.delete(@ready_task, @user)
      end
    end
    assert @ready_task.deleted?
    assert @ready_task.contributors.include?(@user)
    assert_empty @ready_task.errors
    assert @ready_task.task_items.all?(&:destroyed?)
    assert @ready_task.task_items.map(&:content_item).all?(&:destroyed?)
  end

  test "#delete should not allow deleting tasks with fields used in blocks" do
    workflow = WorkflowDefinition.find_by!(name: "NUR FÜR AUTOMATISIERTE TESTS: DSB Meldung Missbrauch").create_workflow!
    task = workflow.items.find { |i| i.title == "Entscheidung Löschung" }
    Interactors::TaskInteractor.delete(task, @user)

    refute task.deleted?
    assert_not_empty task.errors
  end
end
