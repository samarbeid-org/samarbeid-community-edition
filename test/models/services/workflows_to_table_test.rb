require "test_helper"

class Services::WorkflowsToTableTest < ActiveSupport::TestCase
  def setup
    @workflow_definition = WorkflowDefinition.find_by(name: "Maßnahmevorlage für automatisierte Tests")
    @workflows = @workflow_definition.workflows
    @workflow = Workflow.find_by(title: "Maßnahme mit Beispieldaten")
  end

  test "should always values for all definition columns even if fields are not present" do
    @workflow.content_items.destroy_all
    table_service = Services::WorkflowsToTable.new

    assert_equal table_service.definition_columns(@workflow.workflow_definition).count, table_service.values_for_definition_columns(@workflow).count
    assert table_service.values_for_definition_columns(@workflow).count >= @workflow.workflow_definition.content_item_definitions.count
  end

  test "should define column headers" do
    table_service = Services::WorkflowsToTable.new(@workflows)

    assert_equal table_service.base_columns.count, table_service.values_for_base_columns(@workflows.first).count
    assert_equal ["Maßnahme-ID", "Maßnahmetitel", "Status", "Verantwortlicher", "Teilnehmer", "Erstellungsdatum", "Ziel", "Boolean", "Date", "Datum mit Zeit", "Decimal"],
      (table_service.base_columns + table_service.definition_columns(@workflows.first.workflow_definition)).take(11)
  end

  test "to_csv should return expected format" do
    table_service = Services::WorkflowsToTable.new(@workflows)

    expected_csv = <<~CSV
      Maßnahme-ID,Maßnahmetitel,Status,Verantwortlicher,Teilnehmer,Erstellungsdatum,Ziel,Boolean,Date,Datum mit Zeit,Decimal,Dossiers,Email,File,Integer,Richtext,Selection,String,Text,Time,Url
      https://samarbeid-tests.example.com/workflows/24,\"\"\"Maßnahme mit Beispieldaten\"\"\",\"\"\"active\"\"\",\"\"\"team-admin@example.org\"\"\",\"[\"\"team-admin@example.org\"\"]\",\"\"\"2021-12-13 18:34\"\"\",[],Nein,13.12.2021,\"13.12.2021, 12:00 Uhr\",\"99,98999999999999\",\"Admin User, Some Other User\",admin@example.org,\"cairo-multiline.pdf, test-umlaute.pdf, tüv.pdf\",99,\"Richtext mit bold, italic, strike
      
      Bullet 1
      
      Bullet 2
      
      Zitat
      
      
      Todo 1
      
      
      
      Todo 2
      
      
      Verweise @Admin User !1 %24 #153 *1 (Person) • Admin User
      \",\"Item 1, Item 3\",Dies ist ein Teststring,\"Testtext, mit zweiter Zeile\",12:00 Uhr,dev.example.org
    CSV
    assert_equal expected_csv, table_service.to_csv_s.first
  end

  test "to_excel should return the expected excel file either written to a file or as a stream" do
    table_service = Services::WorkflowsToTable.new([@workflow])
    tmp_file = Tempfile.new(["test-data", "xlsx"])

    assert table_service.to_excel.is_a?(String)

    assert_equal 0, tmp_file.length
    table_service.to_excel(tmp_file.path)
    assert tmp_file.length > 0

    # If this fails after changing fixtures simply uncomment following command, run test once and thus regenerate excel file
    # of course PLEASE do check if Excel file still looks like expected (i.e. open it :)
    # Services::WorkflowsToTable.new([@workflow]).to_excel("test/fixtures/files/workflow-excel-export.xlsx")
    assert_equal Roo::Spreadsheet.open("test/fixtures/files/workflow-excel-export.xlsx").to_matrix,
      Roo::Spreadsheet.open(tmp_file.path, extension: :xlsx).to_matrix
  end

  test "to_excel should create an excel file with many kinds of workflows" do
    table_service = Services::WorkflowsToTable.new(Workflow.all)

    assert_nothing_raised do
      table_service.to_excel
    end
  end

  test "frontend_link_for should produce links as needed" do
    table_service = Services::WorkflowsToTable.new(@workflows)
    assert_equal "https://samarbeid-tests.example.com/workflows/100",
      table_service.send(:frontend_link_for, "workflow", 100)
    assert_equal "https://samarbeid-tests.example.com/tasks/100",
      table_service.send(:frontend_link_for, "task", 100)
  end
end
