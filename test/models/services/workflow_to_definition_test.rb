require "test_helper"

class Services::WorkflowToDefinitionTest < ActiveSupport::TestCase
  def setup
    @workflow_definition = WorkflowDefinition.find_by!(name: "Bürgeranfrage")
    @workflow = @workflow_definition.create_workflow!
  end

  test "it should persist changes to simple attributes" do
    @workflow.update!(description: "a new description for the definition")
    Services::WorkflowToDefinition.new(@workflow.reload).update!

    assert_equal "a new description for the definition", build_new_workflow.description
  end

  test "it should update content item definitions" do
    content_item = @workflow.content_items.first
    content_item.update!(label: "NEW")
    Services::WorkflowToDefinition.new(@workflow.reload).update!

    assert_includes build_new_workflow.content_items.map(&:label), "NEW"
  end

  test "it should create new content item definitions" do
    @workflow.content_items << build_new_content_item

    Services::WorkflowToDefinition.new(@workflow.reload).update!

    assert_includes build_new_workflow.content_items.map(&:label), "NEW"
  end

  test "it should remove content item definitions if content items were removed" do
    removed_content_item = @workflow.content_items.last
    removed_content_item.task_items.each(&:destroy!) && removed_content_item.destroy!

    Services::WorkflowToDefinition.new(@workflow.reload).update!

    refute_includes build_new_workflow.content_items.map(&:label), removed_content_item.label
  end

  test "it should update  block definitions" do
    block = @workflow.blocks.first
    block.update!(title: "NEW")
    Services::WorkflowToDefinition.new(@workflow.reload).update!

    assert_equal "NEW", build_new_workflow.blocks.first.title
  end

  test "it should create block definitions if needed" do
    @workflow.blocks.create!(title: "NEW", position: @workflow.blocks.map(&:position).max + 10)

    Services::WorkflowToDefinition.new(@workflow.reload).update!

    assert_equal "NEW", build_new_workflow.blocks.last.title
  end

  test "it should remove block definitions if blocks were deleted" do
    removed_block = @workflow.blocks.last
    removed_block.delete_for_ever!

    Services::WorkflowToDefinition.new(@workflow.reload).update!

    refute_includes build_new_workflow.blocks.map(&:title), removed_block.title
  end

  test "it should update task definitions and task_item_definitions" do
    task = @workflow.direct_tasks.first
    task.update!(title: "NEW")
    task_item = task.task_items.first
    task_item.content_item.update!(label: "NEW")
    task_item.update!(position: -1000)
    Services::WorkflowToDefinition.new(@workflow.reload).update!

    new_task = build_new_workflow.direct_tasks.first

    assert_equal "NEW", new_task.title
    assert_equal "NEW", new_task.task_items.first.content_item.label
    assert_equal(-1000, new_task.task_items.first.position)
  end

  test "it should create task definitions and task_item_definitions if needed" do
    new_task = @workflow.direct_tasks.create!(name: "NEW", position: -1000, workflow: @workflow)
    new_task.task_items.create!(position: 1, content_item: @workflow.content_items.first)
    new_task.task_items.create!(position: 2, content_item: build_new_content_item)

    Services::WorkflowToDefinition.new(@workflow.reload).update!

    new_built_task = build_new_workflow.direct_tasks.first

    assert_equal "NEW", new_built_task.title
    assert_equal "NEW", new_built_task.task_items.last.content_item.label
    assert_equal new_task.task_items.map(&:content_item).map(&:label), new_built_task.task_items.map(&:content_item).map(&:label)
  end

  test "it should remove task definitions if task were deleted" do
    removed_task = @workflow.all_tasks.last
    Interactors::TaskInteractor.delete(removed_task, User.first)

    Services::WorkflowToDefinition.new(@workflow.reload).update!

    assert removed_task.deleted? # just in case validations stop deletion of task
    new_workflow = @workflow_definition.reload.create_workflow!
    refute_includes new_workflow.all_tasks.map(&:title), removed_task.title
  end

  test "it should update definition from clones tasks" do
    cloned_task = Services::ClonedTask.new(@workflow.direct_tasks.first).create

    Services::WorkflowToDefinition.new(@workflow.reload).update!

    new_workflow = @workflow_definition.reload.create_workflow!
    assert_includes new_workflow.direct_tasks.map(&:title), cloned_task.title
    assert_equal @workflow.all_tasks.count, new_workflow.all_tasks.count
    assert_equal @workflow.content_items.count, new_workflow.content_items.count
    assert_equal @workflow.all_tasks.flat_map(&:task_items).count, new_workflow.all_tasks.flat_map(&:task_items).count
  end

  test "it should update tasks moved into blocks" do
    first_task = @workflow.direct_tasks.first
    block = @workflow.blocks.first
    first_block_task = block.direct_tasks.first
    first_task.update!(position: -100, workflow_or_block: block)
    first_block_task.update!(position: -100, workflow_or_block: @workflow)

    Services::WorkflowToDefinition.new(@workflow.reload).update!
    new_workflow = @workflow_definition.reload.create_workflow!

    refute_equal first_task.title, new_workflow.direct_tasks.first.title
    assert_equal first_task.title, new_workflow.blocks.first.direct_tasks.first.title
    assert_equal first_block_task.title, new_workflow.direct_tasks.first.title
  end

  test "updating (even with no changes) should increment version" do
    assert_difference -> { @workflow_definition.reload.version } do
      Services::WorkflowToDefinition.new(@workflow).update!
    end
  end

  test "it should cope with deleted tasks from deleted blocks" do
    admin = User.where(role: :super_admin).first!
    block = @workflow.blocks.first
    assert block.direct_tasks.any?
    block.direct_tasks.each { |task| Interactors::TaskInteractor.delete(task, admin) }
    block.delete_for_ever!

    assert_difference -> { @workflow_definition.reload.version } do
      Services::WorkflowToDefinition.new(@workflow).update!
    end
  end

  private

  def build_new_workflow
    @workflow_definition.reload.build_workflow
  end

  def build_new_content_item
    new_content_item = ContentItemDefinition.first.build_content_item(@workflow)
    new_content_item.content_item_definition = nil
    new_content_item.label = "NEW"
    new_content_item
  end
end
