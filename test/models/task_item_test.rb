require "test_helper"

class TaskItemTest < ActiveSupport::TestCase
  test "task_item may be rendered as data_field" do
    task_item = TaskItem.first
    task_item_as_datafield = task_item.as_datafield

    results = ApplicationController.render(
      template: "api/data_fields/_data_field",
      formats: [:json],
      handlers: [:jbuilder],
      locals: {data_field: task_item_as_datafield}
    )

    assert JSON.parse(results).is_a?(Hash)
    assert_equal task_item.content_item.label, JSON.parse(results)["definition"]["name"]
  end

  test "task_items should show who updated them last" do
    content_item = ContentItem.first
    task_item = content_item.task_items.first
    task = task_item.task
    Interactors::TaskInteractor.update_content_item(task, content_item, nil, User.first)

    assert task_item.last_data_change
    assert_equal User.first, task_item.last_data_change.subject
  end
end
