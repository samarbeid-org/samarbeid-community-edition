require "test_helper"

class DossierDefinitionTest < ActiveSupport::TestCase
  def setup
    @dossier_definition = DossierDefinition.find_by!(name: "Person")
  end

  test "it should be possible to create a dossier definition" do
    assert_difference -> { DossierDefinition.count } do
      DossierDefinition.create(name: "test")
    end
  end

  test "it should not be possible to save a dossier definition without name" do
    assert_not @dossier_definition.update(name: "")
    assert @dossier_definition.errors.details[:name].any?
  end

  test "it should not be possible to create a dossier definition with an empty name" do
    dossier_definition_new = DossierDefinition.create(name: "")
    assert dossier_definition_new.errors.details[:name].any?
    refute dossier_definition_new.valid?
  end

  test "dossier definition names should be unique" do
    dossier_definition_new = DossierDefinition.create(name: "Person")
    assert dossier_definition_new.errors.details[:name].any?
    refute dossier_definition_new.valid?
  end

  test "dossier definition should have a name for rails admin" do
    assert_not_empty @dossier_definition.name
  end

  test "it should not be possible to add a non existing field to title_fields or to set it to a value other than an array" do
    @dossier_definition.title_fields << "non_existing_field_id"
    assert_not @dossier_definition.save
    assert @dossier_definition.errors.details[:title_fields].any?
    @dossier_definition.errors.clear

    @dossier_definition.title_fields = "email"
    assert_not @dossier_definition.save
    assert @dossier_definition.errors.details[:title_fields].any?
  end

  test "it should not be possible to add a non existing field to subtitle_fields or to set it to a value other than an array" do
    @dossier_definition.subtitle_fields << "non_existing_field_id"
    assert_not @dossier_definition.save
    assert @dossier_definition.errors.details[:subtitle_fields].any?
    @dossier_definition.errors.clear

    @dossier_definition.subtitle_fields = "email"
    assert_not @dossier_definition.save
    assert @dossier_definition.errors.details[:subtitle_fields].any?
  end
end
