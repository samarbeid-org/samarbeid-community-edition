require "application_system_test_case"
require "system_test_helper"

class WorkflowEngineSystemTest < ApplicationSystemTestCase
  include ActiveJob::TestHelper

  test "starting tasks prematurely should work" do
    login_as(User.find_by!(email: "admin@example.org"))
    visit "/workflows"

    click_on "Maßnahme starten"
    create_workflow_dialog = page.find(".create-workflow-dialog", match: :first)
    within(create_workflow_dialog) do
      click_list_item_link("NUR FÜR AUTOMATISIERTE TESTS: DSB Meldung Missbrauch")
      fill_in("Titel der Maßnahme", with: "Test-Maßnahme")
      click_card_action_button("MASSNAHME STARTEN")
    end

    # First Task is show and ready
    assert_css ".task-edit-page"
    within middle_area do
      assert has_text?("Missbrauchshinweis aufnehmen") # we are redirected to next task
      assert_css ".page-detail-header .v-chip__content", text: "Wartet"
    end

    # Want to start second task early
    click_on "Entscheidung Löschung"
    assert_css ".page-detail-header .v-chip__content", text: "Erstellt"
    click_on "Aufgabe starten"
    assert_css ".page-detail-header .v-chip__content", text: "Aktiv"

    # Want to start task in inaktive block
    find(".v-list-item__title", text: "Löschen: Ja").click
    click_on "Versendung Standard-Dankes-E-Mail"
    assert_css ".page-detail-header .v-chip__content", text: "Erstellt"
    within(middle_area) { assert_text "Versendung Standard-Dankes-E-Mail" }
    click_on "Aufgabe starten"
    assert_css ".page-detail-header .v-chip__content", text: "Aktiv"
  end

  test "completing a unfinished workflow should work" do
    login_as(User.find_by!(email: "admin@example.org"))

    visit "/workflows"

    click_on "Maßnahme starten"
    create_workflow_dialog = page.find(".create-workflow-dialog", match: :first)
    within(create_workflow_dialog) do
      click_list_item_link("NUR FÜR AUTOMATISIERTE TESTS: DSB Meldung Missbrauch")
      fill_in("Titel der Maßnahme", with: "Test-Maßnahme")
      click_card_action_button("MASSNAHME STARTEN")
    end
    assert_css ".task-edit-page"

    within left_sidebar do
      click_on "Test-Maßnahme"
    end

    # complete a unfinished workflow should open a dialog
    click_on "Maßnahme abschließen"
    assert_css ".page-detail-header .v-chip__content", text: "Aktiv"

    complete_workflow_dialog = page.find(".complete-workflow-dialog", match: :first)
    within(complete_workflow_dialog) do
      click_card_action_button("MASSNAHME ABSCHLIESSEN")
    end

    assert_css ".page-detail-header .v-chip__content", text: "Abgeschlossen"
  end

  test "set a decision should open the block" do
    login_as(User.find_by!(email: "admin@example.org"))

    visit "/workflows"

    click_on "Maßnahme starten"
    create_workflow_dialog = page.find(".create-workflow-dialog", match: :first)
    within(create_workflow_dialog) do
      click_list_item_link("NUR FÜR AUTOMATISIERTE TESTS: DSB Meldung Missbrauch")
      fill_in("Titel der Maßnahme", with: "Test-Maßnahme")
      click_card_action_button("MASSNAHME STARTEN")
    end

    within left_sidebar do
      find(".v-list-item", text: "Entscheidung Löschung").click
      refute_css ".v-list-group.v-list-group--active", text: "Löschen: Ja"
    end

    within(middle_area) do
      assert_no_button "Aufgabe abschließen"
      within(find(".v-input", text: "Soll die Anzeige gelöscht werden?")) do
        click_on "Ja"
        has_text? "Ja" # wait until spinner disappears
      end
    end

    within left_sidebar do
      assert_css ".v-list-group.v-list-group--active", text: "Löschen: Ja"
    end
  end
end
