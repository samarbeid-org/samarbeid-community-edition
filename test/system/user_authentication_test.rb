require "application_system_test_case"
require "system_test_helper"

class UserAuthenticationTest < ApplicationSystemTestCase
  setup do
    logout
  end

  # Simulating timout of session is a bit harder than expected. We tried several other options before settling on
  # warden highjacking (is fastest and reliable)
  # Tried: Devise/Warden sign_out helpers, deleting cookies, mokey-patching timedout? method on user object,
  # changing devise config timout for this test and sleeping @user.timout_in
  test "a user who's session expires should be shown sign in form" do
    visit root_path
    assert_equal new_user_session_path, current_path

    login_using_form_with("srs_user@example.org")

    assert_button("user-menu") # We are logged in
    assert has_text?("Benachrichtigungen")

    retry_flaky_actions do
      simulate_session_timeout
      click_on "samarbeid" # clicking anywhere should trigger app to realize our session has timed out
      assert has_text?("Anmelden") # we are shown login form
    end
    assert_equal new_user_session_path, current_path

    ignore_console_log_error(message: "Failed to load resource: the server responded with a status of 401 (Unauthorized)")
  end

  test "only user within admin group see admin-menu and can visit admin interfaces" do
    visit root_url
    ["srs_user@example.org", "team-admin@example.org"].each do |not_super_admin_email|
      login_using_form_with(not_super_admin_email)

      visit rails_admin.dashboard_path
      assert has_text? "Sie dürfen nicht auf diese Seite zugreifen."
      expect_console_log_error(message: "Failed to load resource: the server responded with a status of 403 (Forbidden)")

      visit "/good_job"
      assert has_title? "404 Fehler"

      visit root_path
      logout_using_menu
    end

    login_using_form_with("admin@example.org")
    assert page.has_button?("admin-menu")

    page.find_button("admin-menu").click
    rails_admin_tab = window_opened_by { click_link "Rails Admin" }
    within_window rails_admin_tab do
      assert has_text?("Administration")
      assert_equal rails_admin.dashboard_path, current_path

      # Visit some pages to see if customized rails_admin is working
      visit "/admin/workflow_definition"
      assert has_text?("Liste von")
      visit "/admin/workflow_definition/5"
      assert has_text?("Details für")
      visit "/admin/workflow_definition/5/edit"
      assert has_text?("bearbeiten")
    end

    visit root_path
    page.find_button("admin-menu").click
    rails_admin_tab = window_opened_by { click_link "Good Job" }
    within_window rails_admin_tab do
      assert has_text?("GoodJob")
      assert_equal "/good_job/", current_path
    end
  end

  private

  def simulate_session_timeout
    Warden.on_next_request do |proxy|
      session = proxy.env["rack.session"]["warden.user.user.session"]
      session["last_request_at"] = (Time.at(session["last_request_at"]).utc - User.timeout_in).to_i
    end
  end
end
