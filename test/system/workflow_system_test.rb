require "application_system_test_case"
require "system_test_helper"

class WorkflowSystemTest < ApplicationSystemTestCase
  include ActiveJob::TestHelper

  test "creating a workflow and closing a task should work" do
    login_as(User.find_by!(email: "srs_user@example.org"))
    visit "/workflows"

    click_on "Maßnahme starten"
    create_workflow_dialog = page.find(".create-workflow-dialog", match: :first)
    within(create_workflow_dialog) do
      click_list_item_link("Veranstaltungsplanung")
      fill_in("Titel der Maßnahme", with: "Test-Maßnahme")
      click_card_action_button("MASSNAHME STARTEN")
    end
    assert_css ".task-edit-page"

    within left_sidebar do
      click_on "Test-Maßnahme"
    end
    assert_css ".workflow-edit-page"
    assert_css ".v-breadcrumbs", text: "Veranstaltungsplanung"
    assert_css ".page-detail-header .v-chip__content", text: "Aktiv"

    # make sure we can only select assignees with access rights for workflows
    within right_sidebar do
      within(control_for("Verantwortlicher")) do
        click_on "Ändern"
        fill_in "Suche", with: "no_access_user@example.org"
        assert_css ".v-list-item", text: "Keine Ergebnisse"
      end
    end

    # complete task
    within left_sidebar do
      click_on "Art der Veranstaltung angeben"
    end

    # make oneself assignee
    within right_sidebar do
      within(control_for("Verantwortlicher")) do
        click_on "Ändern"

        # make sure we can only select assignees with access rights for tasks
        fill_in "Suche", with: "no_access_user@example.org"
        assert_css ".v-list-item", text: "Keine Ergebnisse"

        fill_in "Suche", with: "srs"
        find(".v-list-item", text: "srs_user@example.org").click
      end
      assert control_for("Verantwortlicher").has_text?("srs_user@example.org")
    end

    # start task
    click_on "Aufgabe starten"
    assert_css ".page-detail-header .v-chip__content", text: "Aktiv"

    # complete task
    fill_in "Working title", with: "Das wird eine tolle Party!"
    click_on "Aufgabe abschließen"
    assert middle_area.has_text?("Veranstalter angeben") # we are redirected to next task
    click_on "Art der Veranstaltung angeben" # go back to original task
    assert_css ".page-detail-header .v-chip__content", text: "Abgeschlossen"

    # Controls von abgeschlossene Aufgaben sollen readonly sein
    assert has_control?(with_label: "Working title", with_value: "Das wird eine tolle Party!", is_readonly: true)
  end

  test "let's show a workflow with all possible fields" do
    login_as(User.find_by!(email: "team-admin@example.org"))
    visit "/workflows"

    click_on "Maßnahme mit Beispieldaten"

    assert_text "Enthält alle möglichen Felder um diese einmal abzudecken" # Description

    left_sidebar.click_on "Aufgabe mit allen Feldern"
    assert_text "Alle Feldtypen in einer Aufgabe"

    left_sidebar.click_on "Aufgabe mit allen Infoboxen"
    assert_text "Alle Feldtypen als Infoboxen anzeigen"

    visit "/workflows"
    click_on "Maßnahme starten"
    create_workflow_dialog = page.find(".create-workflow-dialog", match: :first)
    within(create_workflow_dialog) do
      click_list_item_link("Maßnahmevorlage für automatisierte Tests")
      fill_in("Titel der Maßnahme", with: "Test-Maßnahme")
      click_card_action_button("MASSNAHME STARTEN")
    end
    assert_text "Aufgabe mit allen Feldern"
  end
end
