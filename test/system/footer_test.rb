require "application_system_test_case"
require "system_test_helper"

class FooterTest < ApplicationSystemTestCase
  include ActiveJob::TestHelper

  test "Footer should only be visible on sign-in page and on static pages" do
    visit "/"
    assert_current_path("/users/sign_in")
    assert_css ".v-bottom-navigation a", count: 2

    click_on "Impressum"
    assert_current_path("/p/impressum")
    assert_css ".v-bottom-navigation a", count: 2

    login_as(User.find_by!(email: "srs_user@example.org"))
    visit "/"
    assert_no_css ".v-bottom-navigation"
  end
end
