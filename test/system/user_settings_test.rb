require "application_system_test_case"
require "system_test_helper"

class UserSettingsTest < ApplicationSystemTestCase
  include ActiveJob::TestHelper
  setup do
    @user = User.find_by!(email: "srs_user@example.org")
    login_as(@user)
  end

  test "Updating notification settings" do
    visit root_url

    find_button("user-menu").click
    assert has_text?("Einstellungen")
    find("a", text: "Einstellungen").click
    assert_current_path("/users/#{@user.id}/settings")

    menu_item = find(".v-navigation-drawer__content a .v-list-item__title", text: "Benachrichtigungen")
    menu_item.click
    assert_current_path("/users/#{@user.id}/settings/notifications")

    assert_selector "div.v-select__selection", text: "sofort"
    retry_flaky_actions do
      find("div.v-select__selection", text: "sofort").click
      find(".v-list-item__title", text: "nie").click
      assert has_text?("Es werden keine Benachrichtigungen zugestellt.")
    end

    test_time = Time.new(2021)
    Timecop.freeze(test_time) do
      assert_selector "div.v-select__selection", text: "nie"
      find("div.v-select__selection", text: "nie").click
      find(".v-list-item__title", text: "sofort").click
      assert has_text?("Nächste E-Mail wird zugestellt am 01.01.2021, 00:01 Uhr Uhr oder später, sobald eine neue Benachrichtigung vorliegt.")

      assert_selector "div.v-select__selection", text: "sofort"
      find("div.v-select__selection", text: "sofort").click
      find(".v-list-item__title", text: "wöchentlich").click
      assert has_text?("Nächste E-Mail wird zugestellt am 04.01.2021, 06:00 Uhr Uhr oder später, sobald eine neue Benachrichtigung vorliegt.")
    end
  end
end
