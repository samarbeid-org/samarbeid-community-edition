require "application_system_test_case"
require "system_test_helper"

class TestHelperTest < ApplicationSystemTestCase
  test "retry_flaky_actions should work for exceptions" do
    expected_retries = 2
    actual_retries = retry_flaky_actions(retries: expected_retries, sleep: 0.0, show_puts: false, raise: false) do
      raise "Something went wrong"
    end
    assert_equal expected_retries, actual_retries
  end

  test "retry_flaky_actions should work for assertions" do
    expected_retries = 2
    actual_retries = retry_flaky_actions(retries: expected_retries, sleep: 0.0, show_puts: false, raise: false) do
      assert false
    end
    assert_equal expected_retries, actual_retries
  end

  test "retry_flaky_actions should work if no failure occurs" do
    retry_flaky_actions do
      assert true
    end
  end
end
