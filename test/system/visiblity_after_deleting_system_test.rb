require "application_system_test_case"
require "system_test_helper"

class VisibilityAfterDeletingSystemTest < ApplicationSystemTestCase
  include ActiveJob::TestHelper

  test "deleting a workflow should remove it and it's tasks from all lists etc." do
    login_as(User.find_by!(email: "admin@example.org"))
    workflow = Workflow.find_by!(title: "Maßnahme die überall vorkommt")

    # Maßnahme die gelöscht werden soll (mit Beschreibung vieler Referenzen die wir im einzelnen unten nochmal prüfen)
    visit "workflows/#{workflow.id}"
    assert_text workflow.title

    # Vorausgegangene Maßnahme
    visit "workflows/7"
    assert_text workflow.title

    # Nachfolger Maßnahme + Erwähnung in Beschreibung
    visit "workflows/22"
    page.find(".custom-control", text: "Vorangegangene Maßnahme").has_css?(".v-list-item", text: workflow.title)

    # Referenz Dossier
    visit "dossiers/1/references"
    assert_text workflow.title

    # Teil eines Ziels
    visit "ambitions/2"
    assert_text workflow.title
    click_tab "HISTORIE"
    within(".v-timeline") { assert_text workflow.title }

    # Notification
    visit "notifications"
    assert_text "@Admin User was soll Ich hier tun?"
    assert_text workflow.title

    # Liste der Maßnahmen
    visit "workflows"
    assert_text workflow.title

    # Liste der Aufgaben
    visit "tasks"
    assert_text workflow.title

    #########################
    # Maßnahme jetzt löschen
    #########################
    visit "workflows/#{workflow.id}"
    open_vertical_dots_context_menu
    perform_enqueued_jobs do
      click_menu_list_item "Maßnahme löschen"
      click_dialog_button "MASSNAHME LÖSCHEN"
    end
    CustomElasticSearchConfig.sync_all_indexes # ensure all reindex is finished

    # Maßnahme die gelöscht wurde
    assert_browser_logs_empty
    visit "workflows/#{workflow.id}"
    refute_text workflow.title
    assert_text "Daten gelöscht"
    expect_console_log_error(message: "Failed to load resource: the server responded with a status of 410 (Gone)")

    # Vorausgegangene Maßnahme
    visit "workflows/7"
    assert_text "%7" # wait for page load
    refute_text workflow.title

    # Nachfolger Maßnahme + Erwähnung in Beschreibung
    visit "workflows/22"
    assert_text "%22" # wait for page load
    assert_text workflow.identifier # in Beschreibung
    page.has_no_css?(".custom-control", text: "Vorangegangene Maßnahme")

    # Referenz Dossier
    visit "dossiers/1/references"
    assert_text "Admin User"
    refute_text workflow.title

    # Teil eines Ziels
    visit "ambitions/2"
    assert_text "!2" # wait for page load
    refute_text workflow.title
    click_tab "HISTORIE"
    within(".v-timeline") do
      assert_text workflow.identifier
      refute_text workflow.title
    end

    # Notification
    visit "notifications"
    assert_text "@Admin User was soll Ich hier tun?"
    assert_text workflow.identifier # Gelöschte Referenz
    refute_text workflow.title

    # Liste der Maßnahmen
    visit "workflows"
    assert_text "Mein Geburtstag" # wait for page load
    refute_text workflow.title # This now works

    # Liste der Aufgaben
    visit "tasks"
    assert_text "Mein Geburtstag" # wait for page load
    skip "This never worked as it should have (Task is on next page on ALLE tab)"
    # Might be due to inline jobs and the task not "knowing" that the workflow has been deleted. Will propably be fixed with #862
    # refute_text workflow.title # It seems as if in test environment we can not "clear" the old list view
    # click_tab "ALLE"
    # refute_text workflow.title
  end
end
