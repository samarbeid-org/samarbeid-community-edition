require "application_system_test_case"
require "system_test_helper"

class SearchSystemTest < ApplicationSystemTestCase
  include ActiveJob::TestHelper
  setup do
    @user = User.find_by!(email: "srs_user@example.org")
    login_as(@user)
  end

  test "searching basically should work" do
    visit root_url

    fill_in "fulltext-search", with: "Algenmarmelade\n"

    search_results = page.all("#search-results .v-list .v-list-item", text: "Algenmarmelade")

    search_result_ambition = search_results.first
    assert search_result_ambition.find(".v-list-item__title", text: "Algenmarmelade")

    search_result_workflow = search_results.last
    assert search_result_workflow.find(".search-result-highlights", text: /Algenmarmelade/)

    fill_in "fulltext-search", with: "Gespräch\n"
    assert_not_empty page.all("#search-results .v-list .v-list-item")

    fill_in "fulltext-search", with: "SRS\n"
    assert_not_empty page.all("#search-results .v-list .v-list-item")
  end

  test "after uploading a file it may be found in the search" do
    visit "/tasks/54" # Task: Beschreibung erstellen

    file_control = find(".v-input", text: "Beschreibung (Dateianhang)")

    perform_enqueued_jobs do # to immediately execute reindex jobs from document
      page.attach_file("test/fixtures/files/cairo-multiline.pdf") do
        file_control.click_on "Hinzufügen"
      end

      assert file_control.first(:xpath, ".//..").has_no_css?(".v-input--is-disabled")
      assert file_control.has_css?(".v-input__slot .v-list .v-list-item .v-list-item__content .v-list-item__title", text: "cairo-multiline.pdf")
    end

    # Edit title of uploaded file
    file_list_item = file_control.find(".v-input__slot .v-list .v-list-item", text: "cairo-multiline.pdf")
    file_list_item.find("button .v-icon.mdi-dots-vertical").click
    click_menu_list_item "Bearbeiten"
    fill_in "Titel", with: "Eine Test Datei"
    click_button("Speichern")
    assert file_control.has_css?(".v-input__slot .v-list .v-list-item .v-list-item__content .v-list-item__title", text: "Eine Test Datei")

    Workflow.searchkick_index.refresh
    fill_in "fulltext-search", with: "From James\n"
    assert page.has_css?("#search-results .v-list .v-list-item", text: "Hello World From James")
  end

  test "adding a person dossier in a task shows the workflow as result in search" do
    visit "/tasks/102" # Erstgespräch Erfindung für eine Test-Erfindung

    within(control_for("Erfinder")) { click_button("Ändern") }
    page.find(".v-menu__content .v-list .v-list-item--link", text: "Anastasia Raubuch").click
    perform_enqueued_jobs do # to immediately execute reindex jobs from document
      within(control_for("Erfinder")) { click_button("Ändern") } # trigger saving
    end
    assert control_for("Erfinder").has_text?("Erfolgreich gespeichert")
    CustomElasticSearchConfig.sync_all_indexes

    fill_in "fulltext-search", with: "Anastasia Raubuch\n"
    search_results = page.all("#search-results .v-list .v-list-item")

    assert_not_empty search_results
    assert search_results.find(text: "Erstgespräch Erfindung für eine Test-Erfindung")
  end
end
