require "test_helper"

class NotificationsMailerTest < ActionMailer::TestCase
  test "recent mails for fixture user dsb@example.org" do
    dsb_user = User.find_by(email: "dsb@example.org")
    assert_equal 0, dsb_user.unseen_and_unsent_notifications.count
    assert_equal 31, dsb_user.notifications.count # current testing fixtures, update to whatever the testing data contains

    dsb_user.noti_last_sent_at = dsb_user.notifications.first.created_at - 1.minute

    assert_equal 31, dsb_user.unseen_and_unsent_notifications.count

    assert_equal 13, dsb_user.unseen_and_unsent_notifications.map(&:event).map(&:type).uniq.count
    assert_equal 22, Event.descendants.count # if you're adding or removing event types, you will need to upgrade that
    assert_equal 9, Event.descendants.count - dsb_user.unseen_and_unsent_notifications.map(&:event).map(&:type).uniq.count # TODO:  See numbers above, looks like we have 6 event types more which we should create tests for

    mail = NotificationsMailer.with(user: dsb_user, notifications: dsb_user.unseen_and_unsent_notifications).recent

    assert_equal "samarbeid | 31 neue Benachrichtigungen", mail.subject
    assert_equal ["dsb@example.org"], mail.to
    assert_equal ["no-reply@example.org"], mail.from
    assert_match "Hallo DSB Nutzer,", mail.body.encoded
    assert_match "Du hast 31 neue Benachrichtigungen in samarbeid seit 14.04.2021, 10:59", mail.body.encoded
  end

  test "each event type has a fixture" do
    event_types_to_test = Event.descendants.filter { _1.instance_methods.include?(:notification_receivers)}.map(&:name)
    all_fixture_types = Notification.all.map(&:event).map(&:class).map(&:name).uniq
    assert_equal [], event_types_to_test - all_fixture_types, "Some Events do not have fixtures and thus are not rendered in the recents mailer tests"
  end

  test "each event type in recent-notifications mails" do
    receiver = User.create(email: "i-receive-mail@example.org", firstname: "First", lastname: "Last", noti_last_sent_at: Time.parse("2018-01-01T00:00:00Z"))
    notifications = Notification.all

    mail = nil
    assert_nothing_raised { mail = NotificationsMailer.with(user: receiver, notifications: notifications).recent }

    assert_equal 0, mail.errors.length
    assert_match(/\Asamarbeid \| #{notifications.length} neue Benachrichtigungen/, mail.subject)

    assert mail.multipart?

    text = mail.text_part.decoded
    assert_match(/Hallo First Last/, text)
    assert_match(/Du hast #{notifications.length} neue Benachrichtigungen in samarbeid seit 01.01.2018, 01:00 Uhr./, text)
    assert_match(%r{https://samarbeid-tests.example.com/notifications}, text)
    assert_match(%r{https://samarbeid-tests.example.com/my/settings/notifications}, text)

    html = Capybara.string mail.html_part.decoded
    html.assert_selector 'a[href="https://samarbeid-tests.example.com/users/1"]', text: "Admin User"
    html.assert_selector 'a[href="https://samarbeid-tests.example.com/my/settings/notifications"]', text: "Einstellungen"
  end

  test "each event type in recent-notifications mails as user with few rights" do
    receiver = User.create(email: "i-receive-mail@example.org", firstname: "First", lastname: "Last", noti_last_sent_at: Time.parse("2018-01-01T00:00:00"))
    notifications = Notification.all

    mail = nil
    assert_nothing_raised { mail = NotificationsMailer.with(user: receiver, notifications: notifications).recent }

    assert_match(/\Asamarbeid \| #{notifications.length} neue Benachrichtigungen/, mail.subject)

    text = mail.text_part.decoded
    # Just a random selection of notifications, update as you please...
    assert_match(/Admin User schrieb einen Kommentar zu Aufgabe/, text)
    assert_match(/Hallo dies ist ein Testkommentar, welcher eine Benachrichtigung für den Admin User erzeugen sollte./, text)
    assert_match(/Aufgabe #50 in Maßnahme %10 wurde gestartet/, text)
    assert_match(/Admin User hat Maßnahme %10 abgeschlossen/, text)
    assert_match(/SRS User schrieb einen Kommentar zu Ziel !2 • Blockchain basierte Nutella • 14.10.2020, 10:15 Uhr/, text)
    assert_match(/SRS User hat Maßnahme %16 zu Ziel !10 • some-ambition hinzugefügt • 14.10.2020, 12:32 Uhr/, text)
    assert_match(/Admin User hat das Fälligkeitsdatum von Aufgabe #123 in Maßnahme %18  auf 15.04.2021 geändert • 14.04.2021, 11:07 Uhr/, text)
    assert_match(/@Admin User was soll Ich hier tun?/, text)

    html = Capybara.string mail.html_part.decoded
    # Just a random selection of notifications, update as you please...
    html.assert_selector 'a[href="https://samarbeid-tests.example.com/ambitions/12"]', text: "!12 • event-test-ambition"

    html.assert_selector "span", text: "#50"
    html.assert_no_selector 'a[href="https://samarbeid-tests.example.com/workflows/50"]'

    html.assert_selector "span", text: "%10"
    html.assert_selector 'a[href="https://samarbeid-tests.example.com/ambitions/10"]', text: "!10 • some-ambition"
  end

  test "each event type in recent-notifications mails as all-seeing-admin" do
    receiver = User.find(1)
    notifications = Notification.all

    mail = nil
    assert_nothing_raised { mail = NotificationsMailer.with(user: receiver, notifications: notifications).recent }

    assert_equal 0, mail.errors.length
    assert_match(/\Asamarbeid \| #{notifications.length} neue Benachrichtigungen/, mail.subject)

    assert mail.multipart?

    text = mail.text_part.decoded
    assert_match(/Hallo Admin User/, text)
    assert_match(/Du hast #{notifications.length} neue Benachrichtigungen in samarbeid/, text)

    # Just a random selection of notifications, update as you please...
    assert_match(/Admin User schrieb einen Kommentar zu Aufgabe/, text)
    assert_match(/Hallo dies ist ein Testkommentar, welcher eine Benachrichtigung für den Admin User erzeugen sollte./, text)
    assert_match(/Aufgabe #50 • Patentansprüche formulieren in Maßnahme %10 • PA-1 wurde gestartet • 28.02.2020, 17:14 Uhr/, text)
    assert_match(/Admin User hat Maßnahme %10 • PA-1 abgeschlossen • 28.02.2020, 17:15 Uhr/, text)
    assert_match(/SRS User schrieb einen Kommentar zu Ziel !2 • Blockchain basierte Nutella • 14.10.2020, 10:15 Uhr/, text)
    assert_match(/SRS User hat Maßnahme %16 • Erstgespräch Erfindung für eine Test-Erfindung zu Ziel !10 • some-ambition hinzugefügt • 14.10.2020, 12:32 Uhr/, text)
    assert_match(/Admin User hat das Fälligkeitsdatum von Aufgabe #123 • Missbrauchshinweis aufnehmen in Maßnahme %18 • event-test-workflow  auf 15.04.2021 geändert • 14.04.2021, 11:07 Uhr/, text)
    assert_match(/@Admin User was soll Ich hier tun?/, text)

    html = Capybara.string mail.html_part.decoded
    html.assert_selector 'a[href="https://samarbeid-tests.example.com/users/1"]', text: "Admin User"
    html.assert_selector 'a[href="https://samarbeid-tests.example.com/my/settings/notifications"]', text: "Einstellungen"

    # Just a random selection of notifications, update as you please...
    html.assert_selector 'a[href="https://samarbeid-tests.example.com/ambitions/12"]', text: "!12 • event-test-ambition"

    html.assert_selector 'a[href="https://samarbeid-tests.example.com/tasks/50"]', text: "#50 • Patentansprüche formulieren"
    html.assert_no_selector "span", text: "#50"

    html.assert_selector 'a[href="https://samarbeid-tests.example.com/workflows/11"]', text: "%11 • PA-3"
    html.assert_no_selector "span", text: "%11"
  end
end
