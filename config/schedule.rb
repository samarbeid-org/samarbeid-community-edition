# Whenever provides a nice dsl to write cronjobs.
set :output, "log/whenever_cron_errors.log"

env :PATH, ENV["PATH"] # Ensure rbenv Path is present for cron execution

every 5.minute do
  runner "DeliverRecentNotificationsMailsJob.perform_now"
  runner "Services::SnoozedTasks.find_and_start"
end

every 3.hours do
  runner "Services::DueTasks.find_and_process"
end

every :day do
  runner "CleanupInvalidDataJob.perform_later"
end

every :day do
  runner "GoodJob::Execution.finished(2.days.ago).delete_all"
end

every :day do
  runner "CleanupNotificationsJob.perform_later"
end
