SENTRY_CSP_URI = ENV.fetch("SENTRY_CSP_URI") { "" }
SENTRY_DSN = ENV.fetch("SENTRY_DSN") { "" }

# Migrated from Raven, see https://github.com/getsentry/sentry-ruby/blob/master/MIGRATION.md
Sentry.init do |config|
  config.dsn = SENTRY_DSN
  config.enabled_environments = ["staging", "production"]
  if ENV["CI"] && ENV["CI_NODE_TOTAL"] # we are running tests on Gitlab CI
    config.dsn = SENTRY_DSN
    config.enabled_environments = ["staging", "production", "test", "development"]
  end
  config.breadcrumbs_logger = [:active_support_logger, :http_logger]
  release_revision_file = Rails.root.join("REVISION")
  config.release = File.read(release_revision_file).squish if File.exist?(release_revision_file)

  # Set tracesSampleRate to 1.0 to capture 100%
  # of transactions for performance monitoring.
  # We recommend adjusting this value in production
  config.traces_sample_rate = nil # Turn-Off tracing
end
Sentry.set_tags(host: ENV.fetch("HOST", "default"))
