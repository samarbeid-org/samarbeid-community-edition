GoodJob.preserve_job_records = true

# From https://github.com/bensheldon/good_job#retries
# By default, GoodJob will automatically and immediately retry a job when an exception is raised to GoodJob.
# When using retry_on with a limited number of retries, the final exception will not be rescued and will raise to
# GoodJob. GoodJob can be configured to discard un-handled exceptions instead of retrying them:
# IMPORTANT: Without this any permanently failing Job NOT Inheriting from ApplicationJob (such as searchkick reindex)
# will be retried FOREVER!
GoodJob.retry_on_unhandled_error = false

# Report to Sentry when jobs failed all their retry attempts
GoodJob.on_thread_error = ->(exception) { Sentry.capture_exception(exception) }

#########
# Mailer Jobs must be configured seperately
# These settings should match those for our normal jobs (see jobs/application_job.rb)
#########
#
# Limit number of retries for sending mails
ActionMailer::MailDeliveryJob.retry_on StandardError, wait: :exponentially_longer, attempts: 10 do |_job, exception|
  # after last retry
  Sentry.capture_exception(exception)
end

# Report any failing job (exceptions)
ActionMailer::MailDeliveryJob.around_perform do |_job, block|
  block.call
rescue => e
  Sentry.capture_exception(e)
  raise
end
