# Be sure to restart your server when you modify this file.

# Define an application-wide content security policy
# For further information see the following documentation
# https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy

# Rails.application.config.content_security_policy do |policy|
#   policy.default_src :self, :https
#   policy.font_src    :self, :https, :data
#   policy.img_src     :self, :https, :data
#   policy.object_src  :none
#   policy.script_src  :self, :https
#   policy.style_src   :self, :https
#   # If you are using webpack-dev-server then specify webpack-dev-server host
#   policy.connect_src :self, :https, "http://localhost:3035", "ws://localhost:3035" if Rails.env.development?

#   # Specify URI for violation reports
#   # policy.report_uri "/csp-violation-report-endpoint"
# end

# If you are using UJS then enable automatic nonce generation
# Rails.application.config.content_security_policy_nonce_generator = -> request { SecureRandom.base64(16) }

# Set the nonce only to specific directives
# Rails.application.config.content_security_policy_nonce_directives = %w(script-src)

# Report CSP violations to a specified URI
# For further information see the following documentation:
# https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy-Report-Only
# Rails.application.config.content_security_policy_report_only = true

# We face several challenges when applying CSP:
# 1. external js-librarires (i.e. sentry exception tracker, Newrelic APM)
# 2. unsafe-eval by those or our librarires (vuej.js)

# to allow faster development for now we disable CSP in those cases.

Rails.application.config.content_security_policy do |policy|
  policy.script_src :self, :https, :unsafe_eval, :unsafe_inline

  # Report violations to our Exception Tracker
  policy.report_uri SENTRY_CSP_URI # we might want to add ?sentry_environment=#{Rails.env} and may deployed version later
end

Rails.application.config.content_security_policy_report_only = true
