Rails.application.routes.draw do
  devise_for :users
  authenticated :user do
    root to: "home#show", as: :authenticated_root
  end
  root to: redirect("/users/sign_in")

  mount RailsAdmin::Engine => "/admin", :as => "rails_admin"
  authenticate :user, ->(user) { user.super_admin? } do
    mount GoodJob::Engine => "/good_job"
  end

  get "home/show"

  namespace :api, defaults: {format: :json} do
    resources :ambitions, only: [:index, :show, :update, :create, :destroy] do
      collection do
        get :list
      end
      member do
        patch :update_assignee
        patch :update_contributors
        patch :update_workflows
        patch :close
        patch :reopen
      end
    end

    resources :workflow_definitions do
      collection do
        get :list
      end
    end

    resources :workflows, only: [:index, :show, :update, :new, :create, :destroy] do
      collection do
        get :list
      end
      member do
        patch :update_assignee
        patch :update_contributors
        patch :update_ambitions
        patch :complete
        patch :reopen
        post :add_task
        post :add_block
        get :content_items
        patch :update_definition
      end
    end

    resources :tasks, only: [:index, :show, :update, :destroy] do
      collection do
        get :list
      end
      member do
        patch :update_assignee
        patch :update_contributors
        patch :update_marked
        patch :update_due_date
        patch :start
        patch :complete
        patch :skip
        patch :reopen
        get :data, action: :show_data
        patch :data, action: :update_data
        patch :move
        post :add_data_field
        patch :clone
      end
    end

    resources :blocks, only: [:update, :destroy] do
      member do
        patch :move
      end
    end

    resources :data_items, only: [:update, :destroy] do
      member do
        patch :move
      end
    end

    resources :content_types, only: [:index]

    resources :events, only: [:index] do
      collection do
        post :create_comment
      end
      member do
        get :obsolete_info
      end
    end

    resources :users, only: [:index, :show, :create] do
      collection do
        get :list
        get :info
      end
    end

    resources :user_settings, only: [:show, :update] do
      member do
        patch :update_admin_status
        patch :update_active_status
        patch :update_password
        patch :update_notification_settings
        get :groups
      end
    end

    resources :groups, only: [:index, :show, :create, :update, :destroy] do
      collection do
        get :list
      end
    end

    resources :notifications, only: [:index] do
      member do
        patch :delivered
        patch :read
        patch :bookmark
        patch :unbookmark
        patch :done
      end
    end

    get "search/fulltext"

    resources :dossiers, only: [:index, :show, :new, :create, :update, :destroy] do
      collection do
        get :list
      end
      member do
        get :show_references
      end
    end

    resources :dossier_definitions, only: [:index, :show, :create, :update, :destroy] do
      collection do
        get :list
      end
    end

    resources :dossier_field_definitions, only: [:create, :update, :destroy] do
      member do
        patch :move
      end
    end

    resources :uploaded_files, only: [:create, :update]

    resources :pages, param: :slug, only: [:show]
  end

  get "p/*path", to: "home#unauthenticated", constraints: ->(request) do
    !request.xhr? && request.format.html?
  end

  get "my/*path", to: "home#my", constraints: ->(request) do
    !request.xhr? && request.format.html?
  end

  get "*path", to: "home#show", constraints: ->(request) do
    !request.xhr? && request.format.html?
  end
end
