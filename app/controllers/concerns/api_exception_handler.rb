module ApiExceptionHandler
  extend ActiveSupport::Concern

  included do
    rescue_from ActiveRecord::RecordNotFound do |e|
      api_response_with(object: e.message, status: :not_found)
    end

    rescue_from RecordDeleted do |e|
      api_response_with(status: :gone)
    end
  end
end

class RecordDeleted < StandardError; end
