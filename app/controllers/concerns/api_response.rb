module ApiResponse
  def api_response_with(view: nil, object: nil, status: :ok, errors: nil)
    if errors&.any?
      render json: errors.messages, status: :unprocessable_entity
    elsif view
      render view
    elsif object
      render json: object, status: status
    else
      head status
    end
  end
end
