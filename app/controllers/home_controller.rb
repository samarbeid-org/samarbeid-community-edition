class HomeController < ApplicationController
  before_action :authenticate_user!, except: :unauthenticated

  def show
  end

  def my
    redirect_to("/users/#{current_user.id}/#{params[:path]}")
  end

  def unauthenticated
    render :show
  end
end
