class Api::EventsController < ApiController
  before_action :load_and_authorize_object, except: [:obsolete_info]

  def index
    @events = Event.for_object(@object)
  end

  def create_comment
    comment = Interactors::CommentInteractor.create(params.require(:message), @object, current_user)

    @events = Event.for_object(@object) if comment.valid?
    api_response_with(view: "api/events/create_comment", errors: comment.errors)
  end

  def obsolete_info
    @event = Event.find(params[:id])
    authorize!(:read, @event.object)
    @event.notifications.find_by(user: current_user)&.read!
  end

  private

  def load_and_authorize_object
    @object = GlobalID::Locator.locate params.require(:object_gid)
    authorize!(:read, @object)
  end
end
