class Api::NotificationsController < ApiController
  load_and_authorize_resource

  NOTIFICATIONS_PER_PAGE = 25

  def index
    load_notifications_and_counts
  end

  def delivered
    @notification.delivered!
    api_response_with(errors: @notification.errors)
  end

  def read
    @notification.read!
    api_response_with(errors: @notification.errors)
  end

  def done
    @notification.done!
    load_notifications_and_counts unless @notification.errors.any?
    api_response_with(view: :index, errors: @notification.errors)
  end

  def bookmark
    @notification.bookmark!
    load_notifications_and_counts unless @notification.errors.any?
    api_response_with(view: :index, errors: @notification.errors)
  end

  def unbookmark
    @notification.unbookmark!
    load_notifications_and_counts(params[:tab_category] || "BOOKMARKED") unless @notification.errors.any?
    api_response_with(view: :index, errors: @notification.errors)
  end

  private

  def load_notifications_and_counts(tab_category = params[:tab_category] || "NEW", page = params[:page]&.to_i || 1, per_page = NOTIFICATIONS_PER_PAGE)
    notifications = current_user.notifications.tab_categories[tab_category.to_sym] || Notification.none
    total_count = notifications.count
    notifications = notifications.order(created_at: :desc).offset((page - 1) * per_page).limit(per_page)

    @result = OpenStruct.new({
      notifications: notifications,
      total_count: total_count,
      per_page: per_page
    })
  end
end
