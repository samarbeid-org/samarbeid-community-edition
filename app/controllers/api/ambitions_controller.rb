class Api::AmbitionsController < ApiController
  load_and_authorize_resource
  before_action :check_for_deleted

  def index
    page = params[:page]&.to_i || 1
    query = params[:query].blank? ? "*" : params[:query]
    assignee_ids = params[:assignee_ids] ||= []
    contributor_ids = params[:contributor_ids] ||= []
    workflow_definition_ids = params[:workflow_definition_ids] ||= []
    tab_category = params[:tab_category] ||= "OPEN"

    case (params[:order] ||= "created_at_desc")
    when "created_at_desc"
      order_attribute = :created_at
      order_sequence = :desc
    when "created_at_asc"
      order_attribute = :created_at
      order_sequence = :asc
    when "title_desc"
      order_attribute = :title
      order_sequence = :desc
    when "title_asc"
      order_attribute = :title
      order_sequence = :asc
    end

    additional_query_params = {
      where: {
        _and: [
          {_or: convert_null_to_nil(assignee_ids).map { |id| {assignee_id: id} }},
          {_or: convert_null_to_nil(contributor_ids).map { |id| {contributor_ids: id} }},
          {_or: workflow_definition_ids.map { |id| {workflow_definition_ids: id} }}
        ],
        tab_categories: tab_category
      },
      order: {order_attribute => order_sequence},
      page: page, per_page: 10
    }

    @users = User.reorder_by_name_with_user_first(current_user).all

    @workflow_definitions = WorkflowDefinition.search_for_index(current_user, "*", false, {execute: false})

    @result = Ambition.search_for_index(current_user, query, true, additional_query_params.merge({execute: false}))

    Searchkick.multi_search([@result, @workflow_definitions])
  end

  def list
    excluded_ambition_ids = params[:except].to_a
    query = params[:query].blank? ? "*" : params[:query]

    additional_query_params = {
      where: {
        id: {not: excluded_ambition_ids}
      },
      order: {title: :asc}
    }

    @result = Ambition.search_for_list(current_user, query, additional_query_params)
  end

  def show
  end

  def create
    ambition = Interactors::AmbitionInteractor.create(current_user, ambition_params)
    api_response_with(object: {id: ambition.id}, errors: ambition.errors)
  end

  def update
    Interactors::AmbitionInteractor.update(@ambition, ambition_params, current_user)
    api_response_with(view: :show, errors: @ambition.errors)
  end

  def destroy
    Interactors::AmbitionInteractor.delete(@ambition, current_user)
    api_response_with(status: :ok, errors: @ambition.errors)
  end

  def update_assignee
    Interactors::AmbitionInteractor.update_assignee(@ambition, User.find_by(id: params.fetch(:assignee)), current_user)
    api_response_with(view: :show, errors: @ambition.errors)
  end

  def update_contributors
    Interactors::AmbitionInteractor.update_contributors(@ambition, User.where(id: params.fetch(:contributors)), current_user)
    api_response_with(view: :show, errors: @ambition.errors)
  end

  def update_workflows
    Interactors::AmbitionInteractor.update_workflows(@ambition, Workflow.where(id: params.fetch(:workflows)), current_user)
    api_response_with(status: :ok, errors: @ambition.errors)
  end

  def close
    Interactors::AmbitionInteractor.close(@ambition, current_user)
    api_response_with(view: :show, errors: @ambition.errors)
  end

  def reopen
    Interactors::AmbitionInteractor.reopen(@ambition, current_user)
    api_response_with(view: :show, errors: @ambition.errors)
  end

  private

  def check_for_deleted
    return unless @ambition
    raise RecordDeleted if @ambition.deleted
  end

  def ambition_params
    params.require(:ambition).permit(:title, :description)
  end
end
