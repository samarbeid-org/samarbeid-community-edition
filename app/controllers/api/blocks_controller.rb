class Api::BlocksController < ApiController
  load_and_authorize_resource

  def update
    @workflow = @block.workflow

    update_block_params = params.require(:block).permit(:title, :parallel, :decision, :content_item_id)

    if update_block_params[:parallel] != @block.parallel && !@block.created?
      @block.errors.add(:parallel, "kann nur direkt nach Erstellen des Blocks geändert werden")
      api_response_with(view: "api/workflows/show", errors: @block.errors)
      return
    end

    @block.update(update_block_params)
    api_response_with(view: "api/workflows/show", errors: @block.errors)
  end

  def destroy
    if @block.items.any?
      @block.errors.add(:base, "Blöcke mit Tasks können nicht gelöscht werden. Bitte erst Tasks verschieben")
      api_response_with(view: "api/workflows/show", errors: @block.errors)
      return
    end

    @block.delete_for_ever!

    @workflow = @block.workflow
    api_response_with(view: "api/workflows/show", errors: @block.errors)
  end

  def move
    target = @block.workflow
    index = params.require(:index).to_i

    position = (target.items[index]&.position || (target.items.max_by(&:position)&.position || 0))
    position -= 1 if index < target.items.index(@block)

    target.direct_tasks.where("position > ?", position).update_all("position = position + 1")
    target.blocks.where("position > ?", position).update_all("position = position + 1") if target.respond_to?(:blocks)

    @block.update(position: position + 1)

    @workflow = @block.workflow.reload
    api_response_with(view: "api/workflows/show", errors: @block.errors)
  end
end
