class Api::WorkflowsController < ApiController
  load_and_authorize_resource except: [:new, :create]
  before_action :check_for_deleted

  def index
    page = params[:page]&.to_i || 1
    query = params[:query].blank? ? "*" : params[:query]
    assignee_ids = params[:assignee_ids] ||= []
    contributor_ids = params[:contributor_ids] ||= []
    workflow_definition_ids = params[:workflow_definition_ids] ||= []
    ambition_ids = params[:ambition_ids] ||= []
    tab_category = params[:tab_category] ||= "ACTIVE"

    case (params[:order] ||= "created_at_desc")
    when "created_at_desc"
      order_attribute = :created_at
      order_sequence = :desc
    when "created_at_asc"
      order_attribute = :created_at
      order_sequence = :asc
    when "title_desc"
      order_attribute = :title
      order_sequence = :desc
    when "title_asc"
      order_attribute = :title
      order_sequence = :asc
    end

    additional_query_params = {
      where: {
        _and: [
          {_or: convert_null_to_nil(assignee_ids).map { |id| {assignee_id: id} }},
          {_or: convert_null_to_nil(contributor_ids).map { |id| {contributor_ids: id} }},
          {_or: workflow_definition_ids.map { |id| {workflow_definition_ids: id} }},
          {_or: ambition_ids.map { |id| {ambition_ids: id} }}
        ],
        tab_categories: tab_category
      },
      order: {order_attribute => order_sequence}
    }
    additional_query_params = additional_query_params.merge({page: page, per_page: 10}) unless params[:format].eql?("xlsx")

    @users = User.reorder_by_name_with_user_first(current_user).all.to_a

    @workflow_definitions = WorkflowDefinition.search_for_index(current_user, "*", false, {execute: false})
    @ambitions = Ambition.search_for_index(current_user, "*", false, {execute: false})

    @result = Workflow.search_for_index(current_user, query, true, additional_query_params.merge({execute: false}))

    Searchkick.multi_search([@result, @workflow_definitions, @ambitions])

    if params[:format].eql?("xlsx")
      send_data Services::WorkflowsToTable.new(@result).to_excel
    end
  end

  def list
    excluded_workflow_ids = params[:except].to_a
    query = params[:query].blank? ? "*" : params[:query]

    additional_query_params = {
      where: {
        id: {not: excluded_workflow_ids}
      },
      order: {id_sort: :desc}
    }

    @workflows = Workflow.search_for_list(current_user, query, additional_query_params)
  end

  def new
    workflow_definition = WorkflowDefinition.find(params.require(:workflow_definition_id))
    @workflow = Interactors::WorkflowInteractor.new(workflow_definition, current_user)
    authorize!(:new, @workflow)
    api_response_with(view: :new, errors: @workflow.errors)
  end

  def create
    workflow_definition = WorkflowDefinition.find(params.require(:workflow_definition_id))
    authorize!(:create, workflow_definition.workflows.new)

    ambition = Ambition.find_by(id: params[:ambition_id])
    predecessor_workflow = Workflow.find_by(id: params[:predecessor_workflow_id])

    @workflow = Interactors::WorkflowInteractor.create(workflow_definition, workflow_create_params, ambition, predecessor_workflow, current_user)
    api_response_with(view: :create, errors: @workflow.errors)
  end

  def show
  end

  def update
    Interactors::WorkflowInteractor.update(@workflow, workflow_update_params, current_user)
    api_response_with(view: :show, errors: @workflow.errors)
  end

  def update_assignee
    Interactors::WorkflowInteractor.update_assignee(@workflow, User.find_by(id: params.fetch(:assignee)), current_user)
    api_response_with(view: :show, errors: @workflow.errors)
  end

  def update_contributors
    Interactors::WorkflowInteractor.update_contributors(@workflow, User.where(id: params.fetch(:contributors)), current_user)
    api_response_with(view: :show, errors: @workflow.errors)
  end

  def update_ambitions
    Interactors::WorkflowInteractor.update_ambitions(@workflow, Ambition.where(id: params.fetch(:ambitions)), current_user)
    api_response_with(status: :ok, errors: @workflow.errors)
  end

  def complete
    Interactors::WorkflowInteractor.complete(@workflow, current_user)
    api_response_with(view: :show, errors: @workflow.errors)
  end

  def reopen
    Interactors::WorkflowInteractor.reopen(@workflow, current_user)

    api_response_with(view: :show, errors: @workflow.errors)
  end

  def destroy
    Interactors::WorkflowInteractor.trash(@workflow, current_user)

    api_response_with(status: :ok, errors: @workflow.errors)
  end

  def add_task
    unless @workflow.active?
      api_response_with(status: :method_not_allowed)
      return
    end

    last_position = @workflow.items.max_by(&:position)&.position || 0
    task = @workflow.direct_tasks.create(add_task_params.merge(workflow: @workflow, position: last_position + 10))

    if task.valid?
      Events::CreatedEvent.create!(subject: current_user, object: task)
      task.reload
    end

    api_response_with(view: "api/workflows/show", errors: task.errors)
  end

  def add_block
    unless @workflow.active?
      api_response_with(status: :method_not_allowed)
      return
    end

    last_position = @workflow.items.max_by(&:position)&.position || 0
    add_block_params = params.require(:block).permit(:title, :parallel, :decision, :content_item_id)
    @block = @workflow.blocks.create(add_block_params.merge(position: last_position + 10))
    api_response_with(view: "api/workflows/show", errors: @block.errors)
  end

  def content_items
    @content_items = @workflow.content_items
    @content_items = @content_items.where(content_type: params[:type]) if params[:type] && ContentType.types_hash.key?(params[:type].to_sym)
  end

  def update_definition
    Interactors::WorkflowDefinitionInteractor.update_structure(@workflow, current_user)

    api_response_with(status: :ok)
  end

  private

  def check_for_deleted
    return unless @workflow
    raise RecordDeleted unless @workflow.not_deleted?
  end

  def workflow_create_params
    params.require(:workflow).permit(:title, :assignee_id)
  end

  def workflow_update_params
    params.require(:workflow).permit(:title, :description)
  end

  def add_task_params
    params.require(:task).permit(:name, :description)
  end
end
