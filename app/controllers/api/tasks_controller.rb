class Api::TasksController < ApiController
  load_and_authorize_resource

  def index
    page = params[:page]&.to_i || 1
    assignee_ids = params[:assignee_ids] ||= []
    contributor_ids = params[:contributor_ids] ||= []
    workflow_assignee_ids = params[:workflow_assignee_ids] ||= []
    workflow_definition_ids = params[:workflow_definition_ids] ||= []
    ambition_ids = params[:ambition_ids] ||= []
    tab_category = params[:tab_category] ||= "TODO"
    task_definition_ids = params[:task_definition_ids] ||= []

    case (params[:order] ||= "created_at_desc")
    when "created_at_desc"
      order_attribute = :created_at
      order_sequence = :desc
    when "created_at_asc"
      order_attribute = :created_at
      order_sequence = :asc
    when "title_desc"
      order_attribute = :title
      order_sequence = :desc
    when "title_asc"
      order_attribute = :title
      order_sequence = :asc
    when "by_due_date"
      order_attribute = :due_at_for_sorting
      order_sequence = :asc
    end

    additional_query_params = {
      where: {
        _and: [
          {_or: convert_null_to_nil(assignee_ids).map { |id| {assignee_id: id} }},
          {_or: convert_null_to_nil(contributor_ids).map { |id| {contributor_ids: id} }},
          {_or: convert_null_to_nil(workflow_assignee_ids).map { |id| {workflow_assignee_id: id} }},
          {_or: workflow_definition_ids.map { |id| {workflow_definition_id: id} }},
          {_or: ambition_ids.map { |id| {ambition_ids: id} }},
          {_or: task_definition_ids.map { |id| {task_definition_id: id} }}
        ],
        tab_categories: tab_category
      },
      order: {order_attribute => order_sequence},
      page: page, per_page: 10
    }

    additional_query_params[:where][:marked_by_user_ids] = current_user.id if tab_category == "MARKED"

    @users = User.reorder_by_name_with_user_first(current_user).all.to_a

    @workflow_definitions = WorkflowDefinition.search_for_index(current_user, "*", false, {execute: false})

    @ambitions = Ambition.search_for_index(current_user, "*", false, {execute: false})

    @marked_count = Task.marked_count_query(current_user, additional_query_params)
    @result = Task.search_for_index(current_user, "*", true, additional_query_params.merge({execute: false}))

    Searchkick.multi_search([@result, @marked_count, @workflow_definitions, @ambitions])

    @task_definitions = TaskDefinition.not_deleted.where(workflow_definition_id: @workflow_definitions.map(&:id))
  end

  def list
    excluded_task_ids = params[:except].to_a
    query = params[:query].blank? ? "*" : params[:query]

    additional_query_params = {
      where: {
        id: {not: excluded_task_ids}
      },
      order: {id_sort: :desc}
    }

    @tasks = Task.search_for_list(current_user, query, additional_query_params)
  end

  def show
    @workflow = @task.workflow
    api_response_with(view: "api/workflows/show")
  end

  def update
    Interactors::TaskInteractor.update(@task, task_params, current_user)
    @workflow = @task.workflow
    api_response_with(view: "api/workflows/show", errors: @task.errors)
  end

  def update_assignee
    Interactors::TaskInteractor.update_assignee(@task, User.find_by(id: params.fetch(:assignee)), current_user)
    @workflow = @task.workflow
    api_response_with(view: "api/workflows/show", errors: @task.errors)
  end

  def update_contributors
    Interactors::TaskInteractor.update_contributors(@task, User.where(id: params.fetch(:contributors)), current_user)
    @workflow = @task.workflow
    api_response_with(view: "api/workflows/show", errors: @task.errors)
  end

  def update_marked
    Interactors::TaskInteractor.update_marked(@task, params.fetch(:marked), current_user)
    api_response_with(status: :ok, errors: @task.errors)
  end

  def update_due_date
    Interactors::TaskInteractor.update_due_at(@task, params.fetch(:due_at), current_user)
    @workflow = @task.workflow
    api_response_with(view: "api/workflows/show", errors: @task.errors)
  end

  def start
    Interactors::TaskInteractor.start(@task, current_user)
    @workflow = @task.workflow
    api_response_with(view: "api/workflows/show", errors: @task.errors)
  end

  def complete
    Interactors::TaskInteractor.complete(@task, current_user)
    @workflow = @task.workflow
    api_response_with(view: "api/workflows/show", errors: @task.errors)
  end

  def skip
    Interactors::TaskInteractor.skip(@task, current_user)
    @workflow = @task.workflow
    api_response_with(view: "api/workflows/show", errors: @task.errors)
  end

  def reopen
    Interactors::TaskInteractor.reopen(@task, current_user)
    @workflow = @task.workflow
    api_response_with(view: "api/workflows/show", errors: @task.errors)
  end

  def move
    target_type = params[:target_type]
    target_id = params[:target_id]
    case target_type
    when "workflow"
      target = Workflow.find_by(id: target_id)
    when "block"
      target = Block.find_by(id: target_id)
    end
    target ||= @task.workflow_or_block
    unless target == @task.workflow || target.workflow == @task.workflow
      api_response_with(status: :unprocessable_entity) # TODO: add error message: "Cannot move task to different workflow"
      return
    end
    index = params.require(:index).to_i

    position = (target.items[index]&.position || (target.items.max_by(&:position)&.position || 0))

    if target.items.index(@task).nil? # Move to new target
      if index < target.items.length # If not move to end of target items
        position -= 1
      end
    elsif index < target.items.index(@task)
      position -= 1 # Move to lower index
    end

    target.direct_tasks.where("position > ?", position).update_all("position = position + 1")
    target.blocks.where("position > ?", position).update_all("position = position + 1") if target.respond_to?(:blocks)

    @task.update(position: position + 1, workflow_or_block: target)

    @workflow = @task.workflow
    api_response_with(view: "api/workflows/show", errors: @task.errors)
  end

  def add_data_field
    unless @task.workflow.active?
      api_response_with(status: :method_not_allowed)
      return
    end

    content_item_params = params.require(:content_item).permit(:id, :label, :content_type, options: {})

    @content_item = if content_item_params[:id]
      @task.workflow.content_items.find(content_item_params[:id])
    else
      @task.workflow.content_items.create(content_item_params)
    end

    if @content_item.valid?
      last_position = @task.task_items.maximum(:position) || 0
      task_item_params = params.require(:task_item).permit(:required, :info_box)
      @task_item = @task.task_items.create(task_item_params.merge(content_item: @content_item, position: last_position + 10))
    end

    @task_items = @task.task_items.order(:position)
    api_response_with(view: "api/tasks/show_data", errors: @content_item.errors)
  end

  def clone
    cloned_task = Services::ClonedTask.new(@task).create
    @workflow = @task.workflow.reload
    api_response_with(view: "api/workflows/show", errors: cloned_task.errors)
  end

  def destroy
    Interactors::TaskInteractor.delete(@task, current_user)
    @workflow = @task.workflow.reload
    api_response_with(view: "api/workflows/show", errors: @task.errors)
  end

  ##################################
  # data stuff
  ##################################
  def show_data
    @task_items = @task.task_items.order(:position)
  end

  def update_data
    content_item = ContentItem.where(workflow: @task.workflow).find(params.require(:item_id))
    new_value = params.fetch(:value)
    Interactors::TaskInteractor.update_content_item(@task, content_item, new_value, current_user)
    @workflow = @task.workflow
    @task_items = @task.task_items.order(:position)
    api_response_with(view: :update_data, errors: content_item.errors)
  end

  private

  def task_params
    params.require(:task).permit(:name, :description)
  end
end
