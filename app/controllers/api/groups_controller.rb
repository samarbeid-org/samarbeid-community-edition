class Api::GroupsController < ApiController
  load_and_authorize_resource

  def index
    page = params[:page]&.to_i || 1
    query = params[:query].blank? ? "*" : params[:query]

    case (params[:order] ||= "name_asc")
    when "created_at_desc"
      order_attribute = :created_at
      order_sequence = :desc
    when "created_at_asc"
      order_attribute = :created_at
      order_sequence = :asc
    when "name_desc"
      order_attribute = :name
      order_sequence = :desc
    when "name_asc"
      order_attribute = :name
      order_sequence = :asc
    end

    additional_query_params = {
      order: {order_attribute => order_sequence},
      page: page, per_page: 10
    }

    @result = Group.search_for_index(current_user, query, true, additional_query_params)
  end

  def list
    excluded_ids = params[:except].to_a
    limit = params[:max_result_count]&.to_i || 10
    query = params[:query].blank? ? "*" : params[:query]

    additional_query_params = {
      where: {
        id: {not: excluded_ids}
      },
      order: {name: :asc},
      limit: limit
    }

    @groups = Group.search_for_list(current_user, query, additional_query_params)
  end

  def show
  end

  def create
    @group = Group.create(group_params)

    api_response_with(view: :show, errors: @group.errors)
  end

  def update
    @group.update(group_params)

    api_response_with(view: :show, errors: @group.errors)
  end

  def destroy
    @group.destroy

    api_response_with(status: :ok, errors: @group.errors)
  end

  private

  wrap_parameters Group, include: Group.attribute_names + [:user_ids, :workflow_definition_ids]
  def group_params
    params[:group][:workflow_definition_ids] = params[:group].delete(:workflow_definition_ids)
    params.require(:group).permit(:name, :description, user_ids: [], workflow_definition_ids: [])
  end
end
