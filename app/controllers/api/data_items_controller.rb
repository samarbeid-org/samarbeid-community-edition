class Api::DataItemsController < ApiController
  load_and_authorize_resource :task_item, id_param: :id

  def update
    if params[:task_item].present?
      update_task_item_params = params.require(:task_item).permit(:required, :info_box)
      @task_item.update(update_task_item_params)
    end

    if params[:content_item].present?
      update_content_item_params = params.require(:content_item).permit(:label)
      @task_item.content_item.update(update_content_item_params)
    end

    # task_item doesn't validate anything and thus doesn't produce currently, but this seems clean
    @task_item.errors.merge!(@task_item.content_item.errors) if @task_item.content_item.errors.any?

    @task_items = @task_item.task.task_items.order(:position)
    @workflow = @task_item.task.workflow
    api_response_with(view: "api/tasks/update_data", errors: @task_item.errors)
  end

  def destroy
    # If this is the last task_item for the content_item, so should delete it
    if @task_item.content_item.task_items.count == 1
      # If the content_item is a precondition for a block we should not delete anything
      if @task_item.content_item.blocks.select { |b| b.not_deleted? }.any?
        @task_item.errors.add(:base, "Dieses Feld wird in einem Block als Bedingung verwendet.")
        api_response_with(errors: @task_item.errors)
        return
      end
      @task_item.content_item.destroy
    end
    @task_item.destroy

    @task_items = @task_item.task.task_items.order(:position)
    @workflow = @task_item.task.workflow
    api_response_with(view: "api/tasks/update_data", errors: @task_item.errors)
  end

  def move
    target = @task_item.task
    index = params.require(:index).to_i

    position = (target.task_items[index]&.position || (target.task_items.max_by(&:position)&.position || 0))
    position -= 1 if index < target.task_items.index(@task_item)

    target.task_items.where("position > ?", position).update_all("position = position + 1")
    @task_item.update(position: position + 1)

    @task_items = @task_item.task.task_items.order(:position)
    @workflow = @task_item.task.workflow
    api_response_with(view: "api/tasks/update_data", errors: @task_item.errors)
  end
end
