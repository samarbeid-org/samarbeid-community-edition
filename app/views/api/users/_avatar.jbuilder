if user.nil?
  json.null!
else
  json.label(user.deactivated_at && !local_assigns[:show_deactivated] ? nil : user.avatar_label)
  json.url(user.deactivated_at && !local_assigns[:show_deactivated] ? nil : user.avatar_url(local_assigns[:size]))
end
