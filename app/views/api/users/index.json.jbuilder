json.tab_categories User.tab_categories_counts(@result).to_h
json.total_pages total_page_count_for(@result)
json.result do
  json.partial! "/api/users/user", collection: @result, as: :user
end
