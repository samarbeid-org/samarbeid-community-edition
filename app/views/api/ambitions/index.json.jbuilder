json.tab_categories Ambition.tab_categories_counts(@result).to_h
json.users do
  json.partial! "/api/users/user", collection: @users, as: :user
end
json.workflow_definitions @workflow_definitions.map { |pd| {value: pd.id, text: pd.name} }
json.total_pages total_page_count_for(@result)
json.result do
  json.partial! "/api/ambitions/ambition", collection: @result, as: :ambition
end
