if ambition.nil?
  json.null!
else
  json.id ambition.id
  json.gid ambition.to_global_id.to_s
  json.assignee do
    json.partial! "api/users/user", user: ambition.assignee
  end

  if current_user.can?(:read, ambition)
    json.title ambition.title
    json.description add_mention_labels(ambition.description, current_user)
    json.closed ambition.closed
    json.stateUpdatedAt ambition.state_updated_at

    json.runningWorkflowCount ambition.active_visible_workflows(current_user).count
    json.workflows ambition.workflows.not_deleted, partial: "api/workflows/workflow_minimal", as: :workflow

    json.contributors ambition.contributors, partial: "api/users/user", as: :user

    json.commentsCount ambition.comments.count

    json.referenceLabel ambition.reference_label
    json.mentionLabel ambition.mention_label
  else
    json.referenceLabel ambition.reference_label(true)
    json.noAccess true
  end
end
