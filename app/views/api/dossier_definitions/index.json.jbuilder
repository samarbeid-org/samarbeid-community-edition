json.total_pages total_page_count_for(@result)
json.result do
  json.partial! "/api/dossier_definitions/dossier_definition", collection: @result, as: :dossier_definition
end
