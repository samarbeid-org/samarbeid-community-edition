json.tab_categories current_user.notifications.tab_categories_counts
json.total_pages total_page_count_for(@result)
json.result do
  json.partial! "/api/notifications/notification", collection: @result.notifications, as: :notification
end
