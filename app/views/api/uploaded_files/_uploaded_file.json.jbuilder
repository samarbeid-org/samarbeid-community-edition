json.id uploaded_file.id
json.title uploaded_file.title
json.createdAt uploaded_file.created_at
json.url rails_blob_url(uploaded_file.file)
json.name uploaded_file.file.filename
json.size uploaded_file.file.byte_size
