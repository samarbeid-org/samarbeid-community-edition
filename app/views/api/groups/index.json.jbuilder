json.total_pages total_page_count_for(@result)
json.result do
  json.partial! "/api/groups/group", collection: @result, as: :group
end
