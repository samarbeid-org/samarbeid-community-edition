if task.nil?
  json.null!
else
  json.id task.id
  json.gid task.to_global_id.to_s
  json.assignee do
    json.partial! "api/users/user", user: task.assignee
  end

  json.workflow do
    json.id task.workflow.id

    if current_user.can?(:read, task.workflow)
      json.title task.workflow.title
      json.definition do
        json.name task.workflow.name
      end
      json.state task.workflow.aasm_state
    else
      json.noAccess true
    end
  end

  if current_user.can?(:read, task)
    json.description add_mention_labels(task.description, current_user)
    json.state task.aasm_state
    json.stateUpdatedAt task.state_updated_at

    json.availableActions do
      json.array!({
        start: task.may_start?,
        complete: task.may_complete?,
        reopen: task.may_reopen?,
        skip: task.may_skip?
      }.filter_map { |k, v| k if v })
    end

    json.dataReadonly !task.open? || !task.workflow.active?
    json.dueAt task.due_at
    json.marked task.marked?(current_user)

    json.name task.name

    json.contributors task.contributors, partial: "api/users/user", as: :user

    json.commentsCount task.comments.count

    json.referenceLabel task.reference_label
    json.mentionLabel task.mention_label
  else
    json.noAccess true
    json.referenceLabel task.reference_label(true)
  end
end
