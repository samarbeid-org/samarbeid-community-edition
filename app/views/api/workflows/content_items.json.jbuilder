json.array! @content_items do |content_item|
  json.id content_item.id
  json.label content_item.label
  json.type do
    json.label content_item.content_type.label
  end
end
