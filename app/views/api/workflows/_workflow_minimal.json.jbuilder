if workflow.nil?
  json.null!
else
  json.id workflow.id
  json.assignee do
    json.partial! "api/users/user", user: workflow.assignee
  end

  if current_user.can?(:read, workflow)
    json.title workflow.title
    json.description add_mention_labels(workflow.description, current_user)
    json.state workflow.aasm_state
    json.stateUpdatedAt workflow.state_updated_at

    json.definition do
      json.name workflow.name
    end

    json.tasksDueCount workflow.due_tasks_count
    json.commentsCount workflow.comments.count

    json.referenceLabel workflow.reference_label
    json.mentionLabel workflow.mention_label

    json.finished workflow.completable?
    json.availableActions do
      json.array!({
        complete: workflow.may_complete?,
        reopen: workflow.may_reopen?,
        design: workflow.active?
      }.filter_map { |k, v| k if v })
    end
  else
    json.referenceLabel workflow.reference_label(true)
    json.noAccess true
  end
end
