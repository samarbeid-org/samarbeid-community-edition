json.id workflow.id
json.gid workflow.to_global_id.to_s
json.title workflow.title
json.description add_mention_labels(workflow.description, current_user)
json.state workflow.aasm_state
json.stateUpdatedAt workflow.state_updated_at

json.finished workflow.completable?
json.availableActions do
  json.array!({
    complete: workflow.may_complete?,
    reopen: workflow.may_reopen?,
    design: workflow.active?
  }.filter_map { |k, v| k if v })
end

json.commentsCount workflow.comments.count

json.definition do
  json.id workflow.workflow_definition.id
  json.name workflow.name
end

json.assignee do
  json.partial! "api/users/user", user: workflow.assignee
end

json.contributors workflow.contributors, partial: "api/users/user", as: :user

json.groups workflow.workflow_definition.groups, partial: "api/groups/group_minimal", as: :group

json.structure workflow.items do |item|
  case item
  when Task
    if current_user.can?(:read, item)
      json.type "task"
      json.partial!("api/tasks/task", task: item)
    end
  when Block
    json.type "block"
    json.id item.id

    json.active item.active? || item.completed?
    json.important item.items.any? { |item| item.ready? || item.active? || item.completed? }

    if item.decision_set?
      json.condition do
        json.valid item.condition_valid_and_not_confirmed?
        json.confirmed item.content_item.confirmed?
      end
    end

    json.title item.title
    json.tasks item.items.select { |task| current_user.can?(:read, task) } do |block_task|
      json.type "task"
      json.partial!("api/tasks/task", task: block_task)
    end

    json.parallel item.parallel
    json.decision item.content_item&.content_type&.deserialize(item.decision)
    json.contentItemId item.content_item_id
  end
end

json.nextStep do
  next_step = Services::NextInterestingThing.new(workflow, current_user).next_station
  json.type next_step.class.name.downcase
  json.id next_step.id
end

json.ambitions workflow.ambitions.not_deleted.select { |ambition| current_user.can? :read, ambition },
  partial: "api/ambitions/ambition", as: :ambition

json.successorWorkflows workflow.successor_workflows.not_deleted.order(created_at: :desc), partial: "api/workflows/workflow_minimal", as: :workflow
json.predecessorWorkflow do
  json.partial! "api/workflows/workflow_minimal", workflow: [workflow.predecessor_workflow].compact.find(&:not_deleted?)
end

json.referenceLabel workflow.reference_label
