json.title @workflow.title
json.assignee do
  json.partial! "api/users/user", user: @workflow.assignee
end
json.groups @workflow.workflow_definition.groups, partial: "api/groups/group_minimal", as: :group
