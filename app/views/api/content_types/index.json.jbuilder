json.array! @content_types do |content_type|
  json.type content_type.type
  json.label content_type.label
  json.options_definition content_type.options_definition
end
