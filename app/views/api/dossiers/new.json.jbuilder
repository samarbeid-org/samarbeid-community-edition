json.dataFields do
  json.partial! "/api/dossiers/dossier_data_fields", data_fields: @dossier.data_fields(true)
end
