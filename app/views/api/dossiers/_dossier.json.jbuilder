if dossier.nil?
  json.null!
else
  json.id dossier.id
  json.gid dossier.to_global_id.to_s
  json.title dossier.title
  json.subtitle dossier.subtitle

  json.createdAt dossier.created_at

  json.groups dossier.definition.groups, partial: "api/groups/group_minimal", as: :group

  json.dataFields do
    json.partial! "/api/dossiers/dossier_data_fields", data_fields: dossier.data_fields
  end

  json.definition do
    json.id dossier.definition.id
    json.name dossier.definition.name
  end

  json.referenceCount @reference_count

  json.referenceLabel dossier.reference_label
end
