json.array! data_fields do |data_field|
  json.partial! "/api/data_fields/data_field", data_field: data_field

  json.definition do
    json.recommended data_field[:definition].recommended?
    json.unique data_field[:definition].unique?
  end
end
