json.definition do
  json.partial! "/api/data_fields/data_field_definition", data_field_definition: data_field[:definition]
end
json.partial! "/api/data_fields/data_field_value", type: data_field[:definition].content_type, value: data_field[:value]
