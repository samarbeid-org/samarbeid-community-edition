json.id @workflow_definition.id
json.gid @workflow_definition.to_global_id.to_s
json.name @workflow_definition.name
json.description @workflow_definition.description
json.updatedAt @workflow_definition.updated_at

json.groups do
  json.partial! "/api/groups/group", collection: @workflow_definition.groups.order(:name), as: :group
end
