json.current_user do
  json.partial! "/api/users/current_user", user: user
end

json.menus do
  MenuEntry.locations.keys.each do |location|
    json.set! location do
      json.array! MenuEntry.where(location: location).order(position: :asc) do |entry|
        json.title entry.title
        json.slug entry.page.slug
      end
    end
  end
end

if Rails.env.in?(%w[production staging canary])
  json.sentry_dsn SENTRY_DSN
end
