class RunAclReindexOnlyJob < ApplicationJob
  queue_as :default

  def perform
    CustomElasticSearchConfig.indexables.each do |klass|
      klass.reindex(:visibility_for_groups)
    end
  end
end
