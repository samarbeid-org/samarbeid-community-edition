import User from 'mixins/models/user'
import { formatDateTime, distanceDate } from 'helpers/date'
import isNil from 'lodash/isNil'
import join from 'lodash/join'

export default {
  mixins: [User],
  props: {
    label: {
      type: String,
      default: ''
    },
    loading: Boolean,
    readonly: Boolean,
    disabled: Boolean,
    errorMessages: {
      type: [String, Array],
      default: () => []
    },
    successMessage: {
      type: [String, Array],
      default: () => 'Erfolgreich gespeichert'
    },
    hint: {
      type: String,
      default: undefined
    },
    persistentHint: Boolean,
    confirmedAt: {
      type: String,
      required: false
    },
    lastUpdated: {
      type: Object,
      default: () => { return { at: null, by: null } }
    },
    options: {
      type: Object,
      default: () => {}
    },
    required: Boolean
  },
  data () {
    return {
      controlSuccessMessage: undefined,
      successMessageTimeoutId: undefined,
      controlIsFocused: false
    }
  },
  computed: {
    controlLoading () {
      return this.hasRequestParameter() ? this.requestableLoading : this.loading
    },
    controlReadonly () {
      return this.readonly
    },
    controlDisabled () {
      return this.disabled || this.controlReadonly || this.controlLoading
    },
    controlClass () {
      return this.controlReadonly ? 'control--is-readonly' : ''
    },
    controlErrorMessages () {
      return this.hasRequestParameter() ? this.errorMessage : this.errorMessages
    },
    controlHasError () {
      return this.hasRequestParameter() ? this.hasError : (!isNil(this.errorMessages) && (this.errorMessages.length > 0))
    },
    controlHint () {
      const hintParts = []

      if (this.required && !this.controlReadonly) {
        hintParts.push(this.$t('general.field.required'))
      }

      if (this.lastUpdated?.at) {
        if (this.lastUpdated?.by) {
          hintParts.push(`${this.userFullnameFor(this.lastUpdated.by)} ${distanceDate(new Date(this.lastUpdated.at))}`)
        }

        if (this.confirmedAt) {
          hintParts.push(this.$t('control.data.confirmed', { timestamp: formatDateTime(new Date(this.confirmedAt)) }))
        }
      }

      if (this.hint) {
        if (this.persistentHint || this.controlIsFocused) {
          hintParts.push(this.hint)
        }
      }

      return hintParts.length > 0 ? join(hintParts, ' • ') : undefined
    },
    controlPersistentHint () {
      return !!this.lastUpdated?.at || this.persistentHint || (this.required && !this.controlReadonly)
    }
  },
  methods: {
    controlOnFocus () {
      this.controlIsFocused = true
    },
    controlOnBlur () {
      this.controlIsFocused = false
    },
    controlOnChange (value = this.cacheValue) {
      if (this.cachedValueChanged) {
        if (this.hasRequestParameter()) {
          this.resetRequestable()
          this.request(this.requestParameter, null, value)
        } else {
          // this.$emit('input', value)
        }
      }
    },
    controlOnInput () {
      if (this.hasRequestParameter()) {
        if (this.controlHasError) this.resetRequestable()
      } else {
        this.$emit('input', this.cacheValue)
      }
    },
    controlReset () {
      this.createCacheValue()
      this.controlOnInput()
    },
    controlShowSuccessMessage () {
      this.controlSuccessMessage = this.successMessage
      clearTimeout(this.successMessageTimeoutId)
      this.successMessageTimeoutId = setTimeout(() => { this.controlSuccessMessage = undefined }, 2000)
    }
  }
}
