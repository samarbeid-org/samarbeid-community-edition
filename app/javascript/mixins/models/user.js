import { stateColors } from '../../helpers/definitions'

export default {
  methods: {
    userIsActiveFor (user) {
      return !user.deactivatedAt
    },
    userFullnameFor (user) {
      return user.fullname
    },
    userBusinessObjectFor (user) {
      return {
        id: user.id,
        title: this.userFullnameFor(user),
        subtitleElements: [user.email],
        avatar: {
          text: user.avatar.label,
          image: user.avatar.url
        },
        type: 'User',
        href: ''
      }
    }
  },
  computed: {
    userBreadcrumbs () {
      return [
        { text: 'Nutzer', to: { name: 'users' } },
        { text: `${this.user.id}` }
      ]
    },
    userIsActive () {
      return this.userIsActiveFor(this.user)
    },
    userStateText () {
      return this.userIsActive ? 'Aktualisiert' : 'Deaktiviert'
    },
    userStateColor () {
      return this.userIsActive ? stateColors.created : stateColors.trashed
    },
    userStateUpdatedAtDate () {
      return new Date(this.user.deactivatedAt || this.user.updatedAt)
    },
    userFullname () {
      return this.userFullnameFor(this.user)
    }
  }
}
