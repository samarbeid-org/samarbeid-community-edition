import { stateColors } from 'helpers/definitions'
import find from 'lodash/find'
import flatMap from 'lodash/flatMap'
import includes from 'lodash/includes'

const WORKFLOW_STATE_ACTIVE = 'active'
const WORKFLOW_STATE_COMPLETED = 'completed'
const WORKFLOW_STATE_TRASHED = 'trashed'

export default {
  methods: {
    workflowIdentifierFor (workflow) {
      return `%${workflow.id}`
    },
    workflowGetTaskWithIdFor (workflow, taskId) {
      if (workflow) {
        const tasks = flatMap(workflow.structure, (item) => {
          switch (item.type) {
            case 'task': return item
            case 'block': return item.tasks
            default:
              console.error('Unsupported workflow structure item type:', item.type)
              return []
          }
        })
        return find(tasks, { id: taskId })
      } else {
        return null
      }
    },
    workflowIsActiveFor (workflow) {
      return workflow.state === WORKFLOW_STATE_ACTIVE
    },
    workflowNoAccessFor (workflow) {
      return workflow.noAccess === true
    },
    workflowHasAvailableActionFor (workflow, action) {
      return includes(workflow.availableActions, action)
    }
  },
  computed: {
    workflowBreadcrumbs () {
      return [
        { text: 'Maßnahmen', to: { name: 'workflows' } },
        { text: this.workflow.definition.name, to: { name: 'workflows', query: { workflow_definition_ids: [this.workflow.definition.id] } } },
        { text: `%${this.workflow.id}` }
      ]
    },
    workflowStateText () {
      return this.$t('workflow.state.' + this.workflow.state)
    },
    workflowStateColor () {
      switch (this.workflow.state) {
        case WORKFLOW_STATE_ACTIVE:
          return stateColors.active
        case WORKFLOW_STATE_COMPLETED:
          return stateColors.completed
        case WORKFLOW_STATE_TRASHED:
          return stateColors.trashed
        default:
          console.error('Unsupported workflow state: ' + this.workflow.state)
      }
    },
    workflowStateUpdatedAtDate () {
      return this.workflow.stateUpdatedAt ? new Date(this.workflow.stateUpdatedAt) : null
    },
    workflowIdentifier () {
      return this.workflowIdentifierFor(this.workflow)
    },
    workflowIsActive () {
      return this.workflowIsActiveFor(this.workflow)
    },
    workflowNoAccess () {
      return this.workflowNoAccessFor(this.workflow)
    }
  }
}
