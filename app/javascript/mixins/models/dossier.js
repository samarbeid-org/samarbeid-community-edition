export default {
  computed: {
    dossierBreadcrumbs () {
      return [
        { text: 'Dossiers', to: { name: 'dossiers' } },
        { text: this.dossier.definition.name, to: { name: 'dossiers', query: { definition_ids: [this.dossier.definition.id] } } },
        { text: `*${this.dossier.id}` }
      ]
    }
  }
}
