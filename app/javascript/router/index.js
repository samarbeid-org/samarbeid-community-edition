import Vue from 'vue'
import VueRouter from 'vue-router'

import WorkflowEditPage from 'pages/workflows/edit'
import WorkflowListPage from 'pages/workflows/list'
import TaskListPage from 'pages/tasks/list'

import WorkflowDefinitionListPage from 'pages/workflow-definitions/list'
import WorkflowDefinitionEditPage from 'pages/workflow-definitions/edit'

import DossierEditPage from 'pages/dossiers/edit'
import DossierListPage from 'pages/dossiers/list'

import DossierDefinitionListPage from 'pages/dossier-definitions/list'
import DossierDefinitionEditPage from 'pages/dossier-definitions/edit'

import AmbitionEditPage from 'pages/ambitions/edit'
import AmbitionListPage from 'pages/ambitions/list'

import UserProfilePage from 'pages/users/profile'
import UserListPage from 'pages/users/list'
import UserSettingsPage from 'pages/users/settings.js'

import GroupEditPage from 'pages/groups/edit'
import GroupListPage from 'pages/groups/list'

import NotificationListPage from 'pages/notifications/list'
import SearchListPage from 'pages/search/list'

import SignInPage from 'pages/sign-in'
import StaticPagesShowPage from 'pages/static-pages/show'
import Error404Page from 'pages/errors/404'

Vue.use(VueRouter)

export default new VueRouter({
  mode: 'history',
  routes: [
    // Please double check the URLs generated in app/helpers/email_helper.rb when changing URLs here. Especially /notifications, settings, and details pages for users, ambitions, worflows, or tasks.
    {
      path: '/users/sign_in',
      name: 'sign-in-page',
      component: SignInPage
    },
    {
      path: '/',
      redirect: '/notifications',
      name: 'home'
    },
    {
      path: '/ambitions',
      name: 'ambitions',
      component: AmbitionListPage,
      props: (route) => ({
        query: route.query
      })
    },
    {
      path: '/ambitions/:id',
      name: 'ambition',
      component: AmbitionEditPage,
      props: (route) => ({
        id: parseInt(route.params.id)
      })
    },
    {
      path: '/workflows/:id', // hint: you need to change app/helpers/email_helper.rb as well
      name: 'workflow',
      component: WorkflowEditPage,
      props: (route) => ({
        id: parseInt(route.params.id),
        type: 'workflow'
      })
    },
    {
      path: '/workflows',
      name: 'workflows',
      component: WorkflowListPage,
      props: (route) => ({
        query: route.query
      })
    },
    {
      path: '/tasks/:id',
      name: 'task',
      component: WorkflowEditPage,
      props: (route) => ({
        id: parseInt(route.params.id),
        type: 'task'
      })
    },
    {
      path: '/tasks',
      name: 'tasks',
      component: TaskListPage,
      props: (route) => ({
        query: route.query
      })
    },
    {
      path: '/workflow-definitions/:id',
      name: 'workflow-definition',
      component: WorkflowDefinitionEditPage,
      props: (route) => ({
        id: parseInt(route.params.id)
      })
    },
    {
      path: '/workflow-definitions',
      name: 'workflow-definitions',
      component: WorkflowDefinitionListPage,
      props: (route) => ({
        query: route.query
      })
    },
    {
      path: '/dossiers/:id/references',
      name: 'dossier-references',
      component: DossierEditPage,
      props: (route) => ({
        id: parseInt(route.params.id),
        type: 'references'
      })
    },
    {
      path: '/dossiers/:id',
      name: 'dossier',
      component: DossierEditPage,
      props: (route) => ({
        id: parseInt(route.params.id),
        type: 'main'
      })
    },
    {
      path: '/dossiers',
      name: 'dossiers',
      component: DossierListPage,
      props: (route) => ({
        query: route.query
      })
    },
    {
      path: '/dossier-definitions/:id/groups',
      name: 'dossier-definition-groups',
      component: DossierDefinitionEditPage,
      props: (route) => ({
        id: parseInt(route.params.id),
        type: 'groups'
      })
    },
    {
      path: '/dossier-definitions/:id',
      name: 'dossier-definition',
      component: DossierDefinitionEditPage,
      props: (route) => ({
        id: parseInt(route.params.id),
        type: 'main'
      })
    },
    {
      path: '/dossier-definitions',
      name: 'dossier-definitions',
      component: DossierDefinitionListPage,
      props: (route) => ({
        query: route.query
      })
    },
    {
      path: '/users/:id/settings/groups',
      name: 'user-settings-groups',
      component: UserSettingsPage,
      props: (route) => ({
        id: parseInt(route.params.id),
        type: 'groups'
      })
    },
    {
      path: '/users/:id/settings/password',
      name: 'user-settings-password',
      component: UserSettingsPage,
      props: (route) => ({
        id: parseInt(route.params.id),
        type: 'password'
      })
    },
    {
      path: '/users/:id/settings/notifications',
      name: 'user-settings-notifications',
      component: UserSettingsPage,
      props: (route) => ({
        id: parseInt(route.params.id),
        type: 'notifications'
      })
    },
    {
      path: '/users/:id/settings',
      name: 'user-settings',
      component: UserSettingsPage,
      props: (route) => ({
        id: parseInt(route.params.id),
        type: 'account'
      })
    },
    {
      path: '/users/:id',
      name: 'user',
      component: UserProfilePage,
      props: (route) => ({
        id: parseInt(route.params.id)
      })
    },
    {
      path: '/users',
      name: 'users',
      component: UserListPage,
      props: (route) => ({
        query: route.query
      })
    },
    {
      path: '/groups/:id',
      name: 'group',
      component: GroupEditPage,
      props: (route) => ({
        id: parseInt(route.params.id)
      })
    },
    {
      path: '/groups',
      name: 'groups',
      component: GroupListPage,
      props: (route) => ({
        query: route.query
      })
    },
    {
      path: '/notifications',
      name: 'notifications',
      component: NotificationListPage,
      props: (route) => ({
        query: route.query
      })
    },
    {
      path: '/search/:query',
      name: 'search',
      component: SearchListPage,
      props: (route) => ({
        query: route.params.query
      })
    },
    {
      path: '/p/:slug',
      name: 'static-page',
      component: StaticPagesShowPage,
      props: (route) => ({
        slug: route.params.slug
      })
    },
    {
      path: '*',
      component: Error404Page
    }
  ],
  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else if (to.hash) {
      return new Promise((resolve, reject) =>
        // FIXME: for comments in tasks I needed a quite long timeout. It would be better to try if an element could be found earlier or even better if we subscribed to some loading event.
        // TODO: this does not yet take switching tabs into account which might be needed for certain targets
        setTimeout(() => {
          console.log('Scrolling to hash-element after timeout', document.querySelector(to.hash))
          resolve({
            selector: to.hash,
            offset: { x: 0, y: 120 },
            behavior: 'smooth'
          })
        }, 3000))
    } else {
      return { x: 0, y: 0 }
    }
  }
})
