import TextControl from 'controls/text-control'
import TextareaControl from 'controls/textarea-control'
import RichtextareaControl from 'controls/richtextarea-control'
import NumericControl from 'controls/numeric-control'
import BoolControl from 'controls/bool-control'
import DateControl from 'controls/date-control'
import TimeControl from 'controls/time-control'
import DatetimeControl from 'controls/datetime-control'
import SelectionControl from 'controls/selection-control'
import DossierControl from 'controls/dossier-control'
import FileControl from 'controls/file-control'
import isArray from 'lodash/isArray'
import isEmpty from 'lodash/isEmpty'
import map from 'lodash/map'

const CONTENT_TYPE_STRING = 'string'
const CONTENT_TYPE_URL = 'url'
const CONTENT_TYPE_EMAIL = 'email'
const CONTENT_TYPE_PASSWORD = 'password'
const CONTENT_TYPE_TEXT = 'text'
const CONTENT_TYPE_RICHTEXT = 'richtext'
const CONTENT_TYPE_INTEGER = 'integer'
const CONTENT_TYPE_DECIMAL = 'decimal'
const CONTENT_TYPE_BOOLEAN = 'boolean'
const CONTENT_TYPE_DATE = 'date'
const CONTENT_TYPE_TIME = 'time'
const CONTENT_TYPE_DATETIME = 'datetime'
const CONTENT_TYPE_SELECTION = 'selection'
const CONTENT_TYPE_DOSSIER = 'dossier'
export const CONTENT_TYPE_FILE = 'file'

export function componentForContentType (type) {
  switch (type) {
    case CONTENT_TYPE_STRING:
    case CONTENT_TYPE_URL:
    case CONTENT_TYPE_EMAIL:
    case CONTENT_TYPE_PASSWORD:
      return TextControl
    case CONTENT_TYPE_TEXT:
      return TextareaControl
    case CONTENT_TYPE_RICHTEXT:
      return RichtextareaControl
    case CONTENT_TYPE_INTEGER:
    case CONTENT_TYPE_DECIMAL:
      return NumericControl
    case CONTENT_TYPE_BOOLEAN:
      return BoolControl
    case CONTENT_TYPE_DATE:
      return DateControl
    case CONTENT_TYPE_TIME:
      return TimeControl
    case CONTENT_TYPE_DATETIME:
      return DatetimeControl
    case CONTENT_TYPE_SELECTION:
      return SelectionControl
    case CONTENT_TYPE_DOSSIER:
      return DossierControl
    case CONTENT_TYPE_FILE:
      return FileControl
    default:
      console.error('Unsupported content type:', type)
      return null
  }
}

export function componentOptionsForContentType (type) {
  switch (type) {
    case CONTENT_TYPE_PASSWORD:
      return { type: 'password' }
    case CONTENT_TYPE_URL:
      return { type: 'url' }
    case CONTENT_TYPE_EMAIL:
      return { type: 'email' }
    case CONTENT_TYPE_INTEGER:
      return { type: 'integer' }
    case CONTENT_TYPE_DECIMAL:
      return { type: 'decimal' }
    default: return {}
  }
}

export function isContentTypeValueEmpty (type, value) {
  if (value === null || value === undefined) return true

  switch (type) {
    case CONTENT_TYPE_STRING:
    case CONTENT_TYPE_URL:
    case CONTENT_TYPE_EMAIL:
    case CONTENT_TYPE_PASSWORD:
    case CONTENT_TYPE_TEXT:
      return value === ''
    case CONTENT_TYPE_SELECTION:
    case CONTENT_TYPE_DOSSIER:
    case CONTENT_TYPE_FILE:
      return isArray(value) && isEmpty(value)
  }
}

export function updateValueForContentType (type, value) {
  switch (type) {
    case CONTENT_TYPE_FILE:
    case CONTENT_TYPE_DOSSIER:
      return map(value, 'id')
    default:
      return value
  }
}
