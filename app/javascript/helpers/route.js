import isNil from 'lodash/isNil'
import defaults from 'lodash/defaults'
import axios from 'axios'
import omit from 'lodash/omit'

export function routeFor (type, id, { query = undefined, hash = undefined } = {}) {
  const routes = {
    ambition: 'ambition',
    dossier: 'dossier',
    dossier_references: 'dossier-references',
    dossier_definition: 'dossier-definition',
    dossier_definition_groups: 'dossier-definition-groups',
    group: 'group',
    task: 'task',
    user: 'user',
    user_settings: 'user-settings',
    user_settings_groups: 'user-settings-groups',
    user_settings_password: 'user-settings-password',
    user_settings_notifications: 'user-settings-notifications',
    workflow: 'workflow',
    workflow_definition: 'workflow-definition'
  }

  const result = { name: routes[type], params: { id: id } }
  if (result.name === undefined) {
    console.error('Unknown route type:', type)
    return null
  }

  if (!isNil(query)) {
    result.query = defaults({}, result.query, query)
  }

  if (!isNil(hash)) {
    result.hash = hash
  }

  return result
}

export function exportUrlFor (path, params) {
  return axios.getUri({ url: path + '.xlsx', params: omit(params, ['page']) })
}
