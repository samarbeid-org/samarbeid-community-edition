import Page from '../page'
import EditHeader from './edit-header'
import EditMainContent from './edit-main-content'
import DossierEditReferencesContent from './edit-references-content'
import EditSidebarLeft from './edit-sidebar-left'
import EditSidebarRight from './edit-sidebar-right'

export default {
  name: 'DossierEditPage',
  mixins: [Page],
  props: {
    id: {
      type: Number,
      required: true
    },
    type: {
      type: String,
      required: true
    }
  },
  computed: {
    pageTitleParts () {
      return [...(this.value ? [this.value.referenceLabel] : []), 'Dossiers']
    },
    pageContentComponents () {
      const components = [EditHeader]

      switch (this.type) {
        case 'main':
          components.push(EditMainContent)
          break
        case 'references':
          components.push(DossierEditReferencesContent)
          break
      }

      return components
    },
    pageSidebarLeftComponents () {
      return EditSidebarLeft
    },
    pageSidebarRightComponents () {
      return EditSidebarRight
    },
    pagePropsToWatch () {
      return this.id
    }
  },
  methods: {
    initPageRequestUrl () {
      return this.$apiEndpoints.dossiers.show(this.id)
    }
  }
}
