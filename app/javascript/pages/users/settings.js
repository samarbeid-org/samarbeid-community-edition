import Page from '../page'
import SettingsHeader from './settings-header'
import SettingsAccountContent from './settings-account-content'
import SettingsGroupsContent from './settings-groups-content'
import SettingsPasswordContent from './settings-password-content'
import SettingsNotificationsContent from './settings-notifications-content'
import SettingsSidebarLeft from './settings-sidebar-left'
import User from 'mixins/models/user'

export default {
  name: 'UserSettingsPage',
  mixins: [Page, User],
  props: {
    id: {
      type: Number,
      required: true
    },
    type: {
      type: String,
      required: true
    }
  },
  computed: {
    pageTitleParts () {
      return [...(this.value ? [this.userFullnameFor(this.value)] : []), 'Nutzer']
    },
    pageContentComponents () {
      const components = [SettingsHeader]

      switch (this.type) {
        case 'account':
          components.push(SettingsAccountContent)
          break
        case 'groups':
          components.push(SettingsGroupsContent)
          break
        case 'password':
          components.push(SettingsPasswordContent)
          break
        case 'notifications':
          components.push(SettingsNotificationsContent)
          break
      }

      return components
    },
    pageSidebarLeftComponents () {
      return SettingsSidebarLeft
    },
    pagePropsToWatch () {
      return this.id
    }
  },
  methods: {
    initPageRequestUrl () {
      return this.$apiEndpoints.userSettings.show(this.id)
    }
  }
}
