import Page from '../page'
import EditHeader from './edit-header'
import EditMainContent from './edit-main-content'
import EditGroupsContent from './edit-groups-content'
import EditSidebarLeft from './edit-sidebar-left'

export default {
  name: 'DossierDefinitionEditPage',
  mixins: [Page],
  props: {
    id: {
      type: Number,
      required: true
    },
    type: {
      type: String,
      required: true
    }
  },
  computed: {
    pageTitleParts () {
      return [...(this.value ? [this.value.name] : []), 'Dossiervorlagen']
    },
    pageContentComponents () {
      const components = [EditHeader]

      switch (this.type) {
        case 'main':
          components.push(EditMainContent)
          break
        case 'groups':
          components.push(EditGroupsContent)
          break
      }

      return components
    },
    pageSidebarLeftComponents () {
      return EditSidebarLeft
    }
  },
  methods: {
    initPageRequestUrl () {
      return this.$apiEndpoints.dossierDefinitions.show(this.id)
    }
  }
}
