<script>
import { PageContentable } from '../page'
import Breadcrumbs from 'components/breadcrumbs'
import PageDetailHeader from 'components/page-detail-header'
import KebabMenu from 'components/kebab-menu'
import TitleDescriptionControl from 'controls/title-description-control'
import WorkflowsControl from 'controls/workflows-control'
import ActivityHub from 'components/activity-hub'
import CustomDialog from 'components/custom-dialog'
import AddTaskDialog from 'dialogs/add-task-dialog'
import AddEditBlockDialog from 'dialogs/add-edit-block-dialog'
import Workflow from 'mixins/models/workflow'
import Request from 'api/request'
import last from 'lodash/last'
import { routeFor } from 'helpers/route'

export default {
  name: 'EditWorkflowContent',
  components: {
    Breadcrumbs,
    PageDetailHeader,
    KebabMenu,
    TitleDescriptionControl,
    WorkflowsControl,
    ActivityHub,
    CustomDialog,
    AddTaskDialog,
    AddEditBlockDialog
  },
  mixins: [PageContentable, Workflow],
  inject: {
    helper: {
      default: {
        Request
      }
    }
  },
  props: {
    designMode: {
      type: Boolean,
      required: true
    }
  },
  computed: {
    workflow () {
      return this.value ? this.value.workflow : null
    }
  },
  methods: {
    onUpdated (workflow) {
      this.$emit('input', {
        workflow: workflow,
        task: null
      })
    },
    onDeleted () {
      this.$router.replace({ name: 'workflows' })
    },
    onCommentCreated (data) {
      this.onUpdated(data.workflow)
    },
    onNewTask (workflow) {
      this.onUpdated(workflow)
      this.$router.push(routeFor('task', last(workflow.structure).id))
    }
  }
}
</script>
<template>
  <div v-if="value">
    <breadcrumbs :items="workflowBreadcrumbs" />

    <page-detail-header
      :state-text="workflowStateText"
      :state-updated-at="workflowStateUpdatedAtDate"
      :state-color="workflowStateColor.background"
      :has-left-sidebar="hasLeftSidebar"
      :has-right-sidebar="hasRightSidebar"
      @open-sidebar="$emit('open-sidebar', $event)"
    >
      <template #actions="{actionRequest, loading}">
        <custom-dialog
          v-if="workflowHasAvailableActionFor(workflow, 'complete')"
          :title="$t('workflow.completeDialog.title')"
          :text="$t('workflow.completeDialog.text')"
          :ok-btn-text="$t('workflow.completeDialog.buttonOk')"
          content-class="complete-workflow-dialog"
          @click-ok="actionRequest($apiEndpoints.workflows.complete(workflow.id), onUpdated)"
        >
          <template #activator="{ on }">
            <v-btn
              color="primary"
              text
              :disabled="loading"
              v-on="workflow.finished ? { click: () => actionRequest($apiEndpoints.workflows.complete(workflow.id), onUpdated) } : on"
            >
              {{ $t('workflow.actions.complete') }}
            </v-btn>
          </template>
        </custom-dialog>

        <v-btn
          v-if="workflowHasAvailableActionFor(workflow, 'reopen')"
          color="primary"
          text
          :disabled="loading"
          @click="actionRequest($apiEndpoints.workflows.reopen(workflow.id), onUpdated)"
        >
          {{ $t('workflow.actions.reopen') }}
        </v-btn>

        <kebab-menu
          :disabled="loading"
          color="primary"
        >
          <template #items="{ closeMenu }">
            <add-task-dialog
              :request-parameter="{method: 'post', url: $apiEndpoints.workflows.addTask(workflow.id), mapping: 'task'}"
              @request-success-data="onNewTask"
              @open="closeMenu"
            >
              <template #activator="{ on }">
                <v-list-item
                  :disabled="!workflowHasAvailableActionFor(workflow, 'design')"
                  v-on="on"
                >
                  {{ $t('workflow.actions.addTask') }}
                </v-list-item>
              </template>
            </add-task-dialog>

            <add-edit-block-dialog
              :request-parameter="{method: 'post', url: $apiEndpoints.workflows.addBlock(workflow.id), mapping: 'block'}"
              :workflow="workflow"
              add-mode
              @request-success-data="onUpdated"
              @open="closeMenu"
            >
              <template #activator="{ on }">
                <v-list-item
                  :disabled="!workflowHasAvailableActionFor(workflow, 'design')"
                  v-on="on"
                >
                  {{ $t('workflow.actions.addBlock') }}
                </v-list-item>
              </template>
            </add-edit-block-dialog>

            <template v-if="$config.current_user.isAdmin">
              <v-divider />

              <custom-dialog
                :title="$t('workflow.updateDefinitionDialog.title')"
                :text="$t('workflow.updateDefinitionDialog.text')"
                :ok-btn-text="$t('workflow.updateDefinitionDialog.buttonOk')"
                ok-btn-color="error"
                @click-ok="actionRequest($apiEndpoints.workflows.updateDefinition(workflow.id), onDeleted)"
                @open="closeMenu"
              >
                <template #activator="{ on }">
                  <v-list-item
                    v-on="on"
                  >
                    <v-list-item-content>{{ $t('workflow.actions.updateDefinition') }}</v-list-item-content>
                  </v-list-item>
                </template>
              </custom-dialog>
            </template>

            <v-divider />

            <custom-dialog
              :title="$t('workflow.deleteDialog.title')"
              :text="$t('workflow.deleteDialog.text')"
              :ok-btn-text="$t('workflow.deleteDialog.buttonOk')"
              ok-btn-color="error"
              @click-ok="actionRequest($apiEndpoints.workflows.destroy(workflow.id), onDeleted, helper.Request.DELETE)"
              @open="closeMenu"
            >
              <template #activator="{ on }">
                <v-list-item
                  v-on="on"
                >
                  <v-list-item-content>{{ $t('workflow.actions.delete') }}</v-list-item-content>
                </v-list-item>
              </template>
            </custom-dialog>
          </template>
        </kebab-menu>
      </template>
    </page-detail-header>

    <v-alert
      v-if="workflow.finished && workflowHasAvailableActionFor(workflow, 'complete')"
      type="info"
    >
      {{ $t('workflow.finishedAlert') }}
    </v-alert>

    <title-description-control
      :title.sync="workflow.title"
      :description.sync="workflow.description"
      :request-parameter="{method: 'patch', url: $apiEndpoints.workflows.update(workflow.id)}"
      class="mb-8"
      @request-success-data="onUpdated"
    />

    <workflows-control
      v-if="workflow.predecessorWorkflow"
      :value="workflow.predecessorWorkflow"
      class="mb-8"
      icon="mdi-location-enter"
      label="Vorangegangene Maßnahme"
    />

    <workflows-control
      :value="workflow.successorWorkflows"
      :predecessor-workflow-id="workflow.id"
      addable
      icon="mdi-location-exit"
      label="Folgemaßnahmen"
      class="mb-8"
    />

    <activity-hub
      ref="activityhub"
      :gid="workflow.gid"
      :groups="workflow.groups"
      class="mt-12"
      @comment-created="onCommentCreated"
    />
  </div>
</template>
