<script>
import Vue from 'vue'
import { PageContentable } from '../page'
import Breadcrumbs from 'components/breadcrumbs'
import PageDetailHeader from 'components/page-detail-header'
import KebabMenu from 'components/kebab-menu'
import TitleDescriptionControl from 'controls/title-description-control'
import ActivityHub from 'components/activity-hub'
import CustomDialog from 'components/custom-dialog'
import DataControl from 'controls/data-control'
import EditDataFieldDialog from 'dialogs/edit-data-field-dialog'
import DeleteDataFieldDialog from 'dialogs/delete-data-field-dialog'
import AddDataFieldDialog from 'dialogs/add-data-field-dialog'
import CloneTaskDialog from 'dialogs/clone-task-dialog'
import DeleteTaskDialog from 'dialogs/delete-task-dialog'
import Workflow from 'mixins/models/workflow'
import Task from 'mixins/models/task'
import Request from 'api/request'
import Requestable from 'mixins/requestable'
import draggable from 'vuedraggable'
import reduce from 'lodash/reduce'
import has from 'lodash/has'
import { routeFor } from 'helpers/route'

export default {
  name: 'EditTaskContent',
  components: {
    Breadcrumbs,
    PageDetailHeader,
    KebabMenu,
    TitleDescriptionControl,
    ActivityHub,
    CustomDialog,
    DataControl,
    draggable,
    EditDataFieldDialog,
    DeleteDataFieldDialog,
    CloneTaskDialog,
    DeleteTaskDialog,
    AddDataFieldDialog
  },
  mixins: [PageContentable, Task, Workflow],
  props: {
    designMode: {
      type: Boolean,
      required: true
    }
  },
  data () {
    return {
      dataFields: null,
      taskDataRequestable: new (Vue.extend(Requestable))({
        methods: {
          onRequestSuccess: (data) => {
            this.dataFields = data.dataFields
          }
        }
      }),
      designRequestable: new (Vue.extend(Requestable))({
        methods: {
          onRequestSuccess: this.onDataUpdated
        }
      }),
      dataErrorDialogOpen: false
    }
  },
  computed: {
    workflow () {
      return this.value ? this.value.workflow : null
    },
    task () {
      return this.value ? this.value.task : null
    }
  },
  watch: {
    task: {
      handler (value, oldValue) {
        if (this.task && (!oldValue || this.task.id !== oldValue.id || this.designMode)) {
          this.$refs['title-description-control']?.onCancel()

          this.taskDataRequestable.request(
            { method: Request.GET, url: this.$apiEndpoints.tasks.data(this.task.id) },
            null, null, true, true)
        }
      },
      deep: true,
      immediate: true
    }
  },
  methods: {
    onUpdated (workflow) {
      this.$emit('input', {
        workflow: workflow,
        task: this.workflowGetTaskWithIdFor(workflow, this.task.id)
      })
    },
    onUpdatedWithNextStep (workflow) {
      this.onUpdated(workflow)

      if (workflow.nextStep) {
        this.$router.push(routeFor(workflow.nextStep.type, workflow.nextStep.id))
      }
    },
    onDeleted (workflow) {
      this.$emit('input', {
        workflow: workflow,
        task: null
      })
      this.$router.replace(routeFor('workflow', workflow.id))
    },
    onDataUpdated (data) {
      this.onUpdated(data.workflow)
      this.dataFields = data.dataFields
    },
    onDataFieldAdded (dataFields) {
      this.taskDataRequestable.cancelRequestable()
      this.dataFields = dataFields.dataFields
    },
    onCommentCreated (data) {
      this.onUpdated(data.workflow)
    },
    checkDataErrors () {
      let hasErrors = false

      if (this.dataFields) {
        hasErrors = reduce(this.dataFields, (res, dataField) => {
          const ref = 'data_control_for_field_' + dataField.definition.task_item_id
          return res || (!!this.$refs[ref] && this.$refs[ref].controlHasError)
        }, false)
      }

      if (hasErrors) {
        this.dataErrorDialogOpen = true
        return false
      } else {
        return true
      }
    },
    onPositionChanged (event) {
      if (has(event, 'moved')) {
        this.designRequestable.request(
          { method: 'patch', url: this.$apiEndpoints.dataItems.move(event.moved.element.definition.task_item_id) },
          { index: event.moved.newIndex })
      }
    }
  }
}
</script>
<template>
  <div v-if="value">
    <breadcrumbs :items="taskBreadcrumbs" />

    <custom-dialog
      v-model="dataErrorDialogOpen"
      :title="$t('task.taskDataWithErrorsDialog.title')"
      :text="$t('task.taskDataWithErrorsDialog.text')"
      :cancel-btn-text="null"
    />

    <page-detail-header
      :state-text="taskStateText"
      :state-updated-at="taskStateUpdatedAtDate"
      :state-color="taskStateColorFor(task, workflowIsActive).background"
      :comment-count="task.commentsCount"
      :has-left-sidebar="hasLeftSidebar"
      :has-right-sidebar="hasRightSidebar"
      @comment-count-click="$refs.activityhub.scrollToAndShowComments()"
      @open-sidebar="$emit('open-sidebar', $event)"
    >
      <template #actions="{actionRequest, loading}">
        <v-btn
          v-if="taskHasAvailableActionFor(task, 'start')"
          color="primary"
          text
          :disabled="loading"
          @click="checkDataErrors() && actionRequest($apiEndpoints.tasks.start(task.id), onUpdated)"
        >
          {{ $t('task.actions.start') }}
        </v-btn>

        <v-btn
          v-else-if="taskHasAvailableActionFor(task, 'complete')"
          color="primary"
          text
          :disabled="loading"
          @click="checkDataErrors() && actionRequest($apiEndpoints.tasks.complete(task.id), onUpdatedWithNextStep)"
        >
          {{ $t('task.actions.complete') }}
        </v-btn>

        <v-btn
          v-else-if="taskHasAvailableActionFor(task, 'reopen')"
          color="primary"
          text
          :disabled="loading"
          @click="actionRequest($apiEndpoints.tasks.reopen(task.id), onUpdated)"
        >
          {{ $t('task.actions.reopen') }}
        </v-btn>

        <kebab-menu
          :disabled="loading"
          color="primary"
        >
          <template #items="{ closeMenu }">
            <v-list-item
              v-if="taskHasAvailableActionFor(task, 'skip')"
              @click="checkDataErrors() && actionRequest($apiEndpoints.tasks.skip(task.id), onUpdatedWithNextStep)"
            >
              {{ $t('task.actions.skip') }}
            </v-list-item>
            <v-divider />

            <add-data-field-dialog
              :request-parameter="{method: 'post', url: task ? $apiEndpoints.tasks.addDataField(task.id) : ''}"
              :workflow="workflow"
              @request-success-data="onDataFieldAdded"
              @open="closeMenu"
            >
              <template #activator="{ on }">
                <v-list-item
                  :disabled="!workflowHasAvailableActionFor(workflow, 'design')"
                  v-on="on"
                >
                  {{ $t('task.actions.addDataField') }}
                </v-list-item>
              </template>
            </add-data-field-dialog>
            <v-divider />

            <clone-task-dialog
              :request-parameter="{method: 'patch', url: $apiEndpoints.tasks.clone(task.id)}"
              :task-name="task.referenceLabel"
              @request-success-data="onUpdated"
              @open="closeMenu"
            >
              <template #activator="{ on }">
                <v-list-item v-on="on">
                  {{ $t('task.actions.clone') }}
                </v-list-item>
              </template>
            </clone-task-dialog>

            <delete-task-dialog
              :request-parameter="{ method: 'delete', url: $apiEndpoints.tasks.destroy(task.id) }"
              @request-success-data="onDeleted"
              @open="closeMenu"
            >
              <template #activator="{ on }">
                <v-list-item v-on="on">
                  {{ $t('task.actions.delete') }}
                </v-list-item>
              </template>
            </delete-task-dialog>
          </template>
        </kebab-menu>
      </template>
    </page-detail-header>

    <title-description-control
      ref="title-description-control"
      :title="task.name"
      :description.sync="task.description"
      :request-parameter="{method: 'patch', url: $apiEndpoints.tasks.update(task.id), mapping: (data) => { return {'name': data.title, 'description': data.description }}}"
      class="mb-8"
      @request-success-data="onUpdated"
    />

    <template v-if="taskDataRequestable.requestableLoading">
      <v-alert
        dense
        text
        type="info"
      >
        <template #prepend>
          <v-progress-circular
            color="primary"
            indeterminate
            size="20"
            width="2"
            class="mr-2"
          />
        </template>
        Daten werden geladen
      </v-alert>
    </template>
    <template v-else-if="dataFields">
      <draggable
        :list="dataFields"
        handle=".handle"
        :force-fallback="true"
        :animation="200"
        @change="onPositionChanged($event)"
      >
        <div
          v-for="(dataField, index) in dataFields"
          :key="dataField.definition.task_item_id"
          :class="{'d-flex align-center': designMode}"
        >
          <v-icon
            v-if="designMode"
            :disabled="designRequestable.requestableLoading"
            color="purple lighten-2"
            class="handle mr-2"
          >
            mdi-drag-horizontal
          </v-icon>

          <edit-data-field-dialog
            v-if="designMode"
            :value="dataField.definition"
            :workflow="workflow"
            :request-parameter="{method: 'patch', url: $apiEndpoints.dataItems.update(dataField.definition.task_item_id)}"
            @request-success-data="onDataUpdated"
          >
            <template #activator="{ on }">
              <v-btn
                icon
                :disabled="designRequestable.requestableLoading"
                class="mr-2"
                v-on="on"
              >
                <v-icon color="purple lighten-2">
                  mdi-pencil
                </v-icon>
              </v-btn>
            </template>
          </edit-data-field-dialog>

          <delete-data-field-dialog
            v-if="designMode"
            :task-usage-count="dataField.definition.task_item_count"
            :block-usage-count="dataField.definition.block_count"
            :request-parameter="{ method: 'delete', url: $apiEndpoints.dataItems.destroy(dataField.definition.task_item_id) }"
            @request-success-data="onDataUpdated"
          >
            <template #activator="{ on }">
              <v-btn
                icon
                :disabled="designRequestable.requestableLoading"
                class="mr-2"
                v-on="on"
              >
                <v-icon color="purple lighten-2">
                  mdi-delete
                </v-icon>
              </v-btn>
            </template>
          </delete-data-field-dialog>

          <data-control
            :ref="'data_control_for_field_' + dataField.definition.task_item_id"
            v-model="dataField.value"
            :acl-object-global-id="task.gid"
            :type="dataField.definition.type"
            :label="dataField.definition.name"
            :options="dataField.definition.options"
            :required="dataField.definition.required"
            :infobox="dataField.definition.infobox"
            :confirmed-at="dataField.confirmedAt"
            :last-updated="dataField.lastUpdated"
            :readonly="task.dataReadonly"
            :request-parameter="{method: 'patch', url: $apiEndpoints.tasks.data(task.id), mapping: (data) => { return {'item_id': dataField.definition.id, 'value': data}}}"
            :class="{'mb-5': index < dataFields.length - 1, 'flex-grow-1': designMode}"
            @request-success-data="onDataUpdated"
          />
        </div>
      </draggable>
    </template>

    <div
      v-if="designMode && workflowHasAvailableActionFor(workflow, 'design')"
      class="d-flex justify-center mt-2"
    >
      <add-data-field-dialog
        :request-parameter="{method: 'post', url: task ? $apiEndpoints.tasks.addDataField(task.id) : ''}"
        :workflow="workflow"
        @request-success-data="onDataFieldAdded"
      >
        <template #activator="{ on }">
          <v-btn
            fab
            dark
            color="purple lighten-2"
            v-on="on"
          >
            <v-icon>
              mdi-plus
            </v-icon>
          </v-btn>
        </template>
      </add-data-field-dialog>
    </div>

    <activity-hub
      ref="activityhub"
      :gid="task.gid"
      :groups="workflow.groups"
      class="mt-12"
      @comment-created="onCommentCreated"
    />
  </div>
</template>
<style>
.sortable-drag {
  background-color: #F3E5F5;
}
</style>
