import Page from '../page'
import EditContent from './edit-content'
import EditSidebarRight from './edit-sidebar-right'

export default {
  name: 'AmbitionEditPage',
  mixins: [Page],
  props: {
    id: {
      type: Number,
      required: true
    }
  },
  computed: {
    pageTitleParts () {
      return [...(this.value ? [this.value.referenceLabel] : []), 'Ziele']
    },
    pageContentComponents () {
      return EditContent
    },
    pageSidebarRightComponents () {
      return EditSidebarRight
    }
  },
  methods: {
    initPageRequestUrl () {
      return this.$apiEndpoints.ambitions.show(this.id)
    }
  }
}
