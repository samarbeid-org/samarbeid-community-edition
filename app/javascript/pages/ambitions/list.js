import ListPage from '../list-page'
import ListContent from './list-content'
import map from 'lodash/map'
import parseInt from 'lodash/parseInt'

export default {
  name: 'AmbitionListPage',
  mixins: [ListPage],
  computed: {
    pageTitleParts () {
      return ['Ziele']
    },
    pageContentComponents () {
      return ListContent
    }
  },
  methods: {
    initPageRequestUrl () {
      return this.$apiEndpoints.ambitions.index()
    },
    initPageRequestParams () {
      return {
        page: this.pages.current,
        query: this.filters.values.query,
        order: this.filters.values.order,
        tab_category: this.filters.values.tab_category,
        assignee_ids: this.filters.values.assignee_ids,
        contributor_ids: this.filters.values.contributor_ids,
        workflow_definition_ids: this.filters.values.workflow_definition_ids
      }
    },
    onPropUpdated (prop, value, info) {
      ListPage.methods.onPropUpdated.call(this, prop, value, info)
      this.$router.replace({ name: 'ambitions', query: this.createQuery() })
    },
    filterTabs () {
      return {
        name: 'tab_category',
        items: [
          { text: 'Offen', value: 'OPEN' },
          { text: 'Mit offenen Maßnahmen', value: 'SOME_WORKFLOWS_ACTIVE' },
          { text: 'Abgeschlossen', value: 'CLOSED' },
          { text: 'Alle', value: 'ALL' }
        ],
        counts: this.valueAttributeOrDefault('tab_categories', {}),
        default: 'OPEN'
      }
    },
    filterFields () {
      return [
        [
          {
            name: 'query',
            type: 'text',
            label: 'Zieltitel',
            default: ''
          },
          {
            name: 'order',
            type: 'single-select',
            label: 'Sortierung',
            items: [
              { text: 'Erstellungsdatum – neueste zuerst', value: 'created_at_desc' },
              { text: 'Erstellungsdatum – älteste zuerst', value: 'created_at_asc' },
              { text: 'Titel – aufsteigend', value: 'title_asc' },
              { text: 'Titel – absteigend', value: 'title_desc' }
            ],
            default: 'created_at_desc',
            cols: 4
          }],
        [
          {
            name: 'assignee_ids',
            type: 'user-multi-select',
            label: 'Verantwortlicher',
            items: this.addNoneUser(this.valueAttributeOrDefault('users')),
            default: [],
            cast: (value) => map([value].flat(), (value) => { return value === null ? null : parseInt(value) })
          },
          {
            name: 'workflow_definition_ids',
            type: 'multi-select',
            label: 'Art der Maßnahme',
            items: this.valueAttributeOrDefault('workflow_definitions'),
            default: [],
            cast: (value) => map([value].flat(), parseInt)
          }],
        [
          {
            name: 'contributor_ids',
            type: 'user-multi-select',
            label: 'Teilnehmer',
            items: this.addNoneUser(this.valueAttributeOrDefault('users')),
            default: [],
            cast: (value) => map([value].flat(), (value) => { return value === null ? null : parseInt(value) })
          }]
      ]
    }
  }
}
