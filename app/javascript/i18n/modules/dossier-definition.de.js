export default {
  actions: {
    create: 'Dossiervorlage erstellen',
    delete: 'Dossiervorlage löschen'
  },
  createDialog: {
    title: 'Dossiervorlage erstellen',
    buttonOk: 'Dossiervorlage erstellen'
  },
  deleteDialog: {
    title: 'Dossiervorlage löschen?',
    text: 'Löschen Sie die Dossiervorlage, wenn Sie sicher sind, dass diese Dossiervorlage nicht mehr benöigt wird. Gelöschte Dossiervorlagen sind unwiederbringlich verloren.',
    buttonOk: 'Dossiervorlage löschen'
  }
}
