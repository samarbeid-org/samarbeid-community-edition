export default {
  appName: 'samarbeid',
  title: 'Titel',
  description: 'Beschreibung',
  commentsCount: '{count} Kommentar | {count} Kommentare',
  buttons: {
    cancel: 'Abbrechen',
    save: 'Speichern',
    change: 'Ändern',
    retry: 'Wiederholen'
  },
  field: {
    required: 'erforderlich'
  },
  editorHelpText: 'Verweise im Text: @Nutzer, !Ziel, %Maßnahme, #Aufgabe, *Dossier',
  time: '{time} Uhr',
  sortableHint: 'Reihenfolge per Drag&Drop änderbar.'
}
