export default {
  actions: {
    add: 'Hinzufügen',
    edit: 'Bearbeiten',
    delete: 'Löschen'
  },
  deleteDialog: {
    title: 'Datenfeld aus Dossiervorlage löschen?',
    text: 'Löschen Sie das Datenfeld aus der Dossiervorlage nur, wenn Sie sicher sind, dass dieses Datenfeld nicht mehr benöigt wird. Gelöschte Datenfelder und ihre Werte gehen unwiederbringlich verloren.',
    buttonOk: 'Datenfeld löschen'
  }
}
