export default {
  state: {
    active: 'Aktiv',
    completed: 'Abgeschlossen',
    trashed: 'Gelöscht'
  },
  tasksDueCount: '{count} fällige Aufgabe | {count} fällige Aufgaben',
  finishedAlert: 'Alle Aufgaben in dieser Maßnahme sind abgeschlossen oder übersprungen. Diese Maßnahme könnte abgeschlossen werden.',
  actions: {
    create: 'Maßnahme starten',
    complete: 'Maßnahme abschließen',
    reopen: 'Maßnahme wiedereröffnen',
    delete: 'Maßnahme löschen',
    updateDefinition: 'Vorlage aktualisieren',
    addTask: 'Aufgabe hinzufügen',
    addBlock: 'Block hinzufügen',
    export: 'Aktuelle Liste exportieren'
  },
  createDialog: {
    title: 'Maßnahme starten',
    buttonOk: 'Maßnahme starten',
    buttonBack: 'Zurück'
  },
  completeDialog: {
    title: 'Maßnahme abschließen?',
    text: 'In dieser Maßnahme sind Aufgaben noch nicht abgeschlossen oder übersprungen. Soll diese Maßnahme trotzdem abgeschlossen werden?',
    buttonOk: 'Maßnahme abschließen'
  },
  deleteDialog: {
    title: 'Maßnahme löschen?',
    text: 'Löschen Sie die Maßnahme, wenn Sie sicher sind, dass diese Maßnahme nicht mehr benöigt wird. Gelöschte Maßnahmen sind unwiederbringlich verloren.',
    buttonOk: 'Maßnahme löschen'
  },
  updateDefinitionDialog: {
    title: 'Vorlage aktualisieren',
    text: 'Die Vorlage dieser Maßnahme wird so aktualisiert, dass zukünftig gestartete Maßnahmen wie diese hier aussehen.',
    buttonOk: 'Vorlage aktualisieren'
  }
}
