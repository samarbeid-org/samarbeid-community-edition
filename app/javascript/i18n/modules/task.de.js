export default {
  state: {
    created: 'Erstellt',
    ready: 'Wartet',
    active: 'Aktiv',
    completed: 'Abgeschlossen',
    skipped: 'Übersprungen',
    deleted: 'Abgebrochen'
  },
  dueDistance: 'Fällig {distance}',
  actions: {
    start: 'Aufgabe starten',
    complete: 'Aufgabe abschließen',
    reopen: 'Aufgabe wiedereröffnen',
    skip: 'Aufgabe überspringen',
    clone: 'Aufgabe duplizieren',
    delete: 'Aufgabe löschen',
    addDataField: 'Datenfeld hinzufügen'
  },
  noAssigneeDialog: {
    title: 'Verantwortlicher werden?',
    text: 'Um diese Aktion durchzuführen, müssen Sie Verantwortlicher sein.'
  },
  taskDataWithErrorsDialog: {
    title: 'Datenfelder mit Fehlerzustand',
    text: 'Mindestens ein Datenfeld befindet sich in einem Fehlerzustand. Bitte korrigieren Sie die fehlerhaften Eingaben.'
  },
  deleteDialog: {
    title: 'Aufgabe löschen?',
    text: 'Löschen Sie die Aufgabe, wenn Sie sicher sind, dass diese nicht mehr benöigt wird. Gelöschte Aufgaben sind unwiederbringlich verloren.',
    buttonOk: 'Aufgabe löschen'
  },
  cloneDialog: {
    title: 'Aufgabe duplizieren?',
    text: 'Es wird eine Kopie der Aufgabe {name} erzeugt. Eingaben und Verantwortlichkeiten werden nicht dupliziert, sondern zurückgesetzt.',
    buttonOk: 'Aufgabe duplizieren'
  }
}
