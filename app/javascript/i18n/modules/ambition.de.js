export default {
  state: {
    open: 'Eröffnet',
    closed: 'Abgeschlossen'
  },
  openWorkflowsCount: '{count} offene Maßnahme | {count} offene Maßnahmen',
  actions: {
    create: 'Ziel erstellen'
  },
  detailActions: {
    close: 'Ziel abschließen',
    reopen: 'Ziel wiedereröffnen',
    delete: 'Ziel löschen'
  },
  closeDialog: {
    title: {
      standard: 'Ziel abschließen?',
      openWorkflows: 'Ziel mit offenen Maßnahmen abschließen?',
      noAccessWorkflows: 'Ziel mit unbekannten Maßnahmen abschließen?'
    },
    text: {
      standard: 'Schließen Sie das Ziel ab, wenn das Vorhaben erreicht ist. Abgeschlossene Ziele können später wiedereröffnet werden.',
      openWorkflows: 'Das Ziel beinhaltet Maßnahmen, die noch nicht abgeschlossen wurden. Schließen Sie das Ziel ab, wenn das Vorhaben trotzdem erreich ist. Abgeschlossene Ziele können später wiedereröffnet werden.',
      noAccessWorkflows: 'Das Ziel beinhaltet Maßnahmen, auf die Sie keinen Zugriff haben. Schließen Sie das Ziel nur ab, wenn Sie sicher sind, dass das Vorhaben trotzdem erreicht ist. Abgeschlossene Ziele können später wiedereröffnet werden.'
    },
    buttonOk: 'Ziel abschließen'
  },
  createDialog: {
    title: 'Neues Ziel erstellen',
    buttonOk: 'Ziel erstellen',
    fields: {
      title: 'Titel des neuen Ziels'
    }
  },
  deleteDialog: {
    title: 'Ziel löschen?',
    text: 'Löschen Sie das Ziel, wenn Sie sicher sind, dass dieses Ziel nicht mehr benöigt wird. Gelöschte Ziele sind unwiederbringlich verloren.',
    buttonOk: 'Ziel löschen'
  }
}
