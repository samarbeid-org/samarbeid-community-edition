import Vue from 'vue'
import VueI18n from 'vue-i18n'
import generalDE from 'i18n/modules/general.de'
import errorDE from 'i18n/modules/error.de'
import ambitionDE from 'i18n/modules/ambition.de'
import workflowDE from 'i18n/modules/workflow.de'
import taskDE from 'i18n/modules/task.de'
import blockDE from 'i18n/modules/block.de'
import dataFieldDE from 'i18n/modules/data-field.de'
import dossierDE from 'i18n/modules/dossier.de'
import groupDE from 'i18n/modules/group.de'
import userDE from 'i18n/modules/user.de'
import controlDE from 'i18n/modules/control.de'
import workflowDefinitionDE from 'i18n/modules/workflow-definition.de'
import dossierDefinitionDE from 'i18n/modules/dossier-definition.de'
import dossierFieldDefinitionDE from 'i18n/modules/dossier-field-definition.de'

Vue.use(VueI18n)

const messages = {
  de: {
    general: generalDE,
    error: errorDE,
    ambition: ambitionDE,
    workflow: workflowDE,
    task: taskDE,
    block: blockDE,
    dataField: dataFieldDE,
    dossier: dossierDE,
    group: groupDE,
    user: userDE,
    control: controlDE,
    workflowDefinition: workflowDefinitionDE,
    dossierDefinition: dossierDefinitionDE,
    dossierFieldDefinition: dossierFieldDefinitionDE
  }
}

const defaultI18n = new VueI18n({
  locale: 'de',
  messages
})

export default defaultI18n
