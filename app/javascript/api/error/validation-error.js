import ApiError from './api-error'
import join from 'lodash/join'
import map from 'lodash/map'

export default class ValidationError extends ApiError {
  constructor (error) {
    super()

    this.code = error.response.status
    this.errors = error.response.data
  }

  forAttribute (attribute) {
    return join(this.errors[attribute], ', ')
  }

  forOther () {
    return null
  }

  forAll () {
    return join(map(this.errors, (value, key) => { return this.forAttribute(key) }), ', ')
  }

  get titleLocalizationKey () {
    return undefined
  }

  get messageLocalizationKey () {
    return undefined
  }
}
