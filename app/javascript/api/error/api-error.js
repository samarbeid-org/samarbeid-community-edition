export default class ApiError {
  constructor (error) {
    this.code = null
    this.message = error ? error.message : null
  }

  forAttribute (attribute) {
    return null
  }

  forOther () {
    return this.message
  }

  forAll () {
    return this.forOther()
  }

  get titleLocalizationKey () {
    return 'error.request.general.title'
  }

  get messageLocalizationKey () {
    return 'error.request.general.message'
  }
}
