import Vue from 'vue'
import camelCase from 'lodash/camelCase'
import includes from 'lodash/includes'
import map from 'lodash/map'

Vue.mixin({
  beforeCreate () {
    const options = this.$options
    if (options.apiEndpoints) {
      this.$apiEndpoints = options.apiEndpoints
    } else if (options.parent && options.parent.$apiEndpoints) {
      this.$apiEndpoints = options.parent.$apiEndpoints
    }
  }
})

const API_BASE_URL = '/api/'

function collections (...paths) {
  return map(paths, function (path) {
    return {
      key: camelCase(path),
      member: false,
      path: function () { return includes(['index', 'create'], path) ? '' : `/${path}` }
    }
  })
}

function members (...paths) {
  return map(paths, function (path) {
    return {
      key: camelCase(path),
      member: true,
      path: function (id) { return includes(['show', 'update', 'destroy'], path) ? `/${id}` : `/${id}/${path}` }
    }
  })
}

function addAction (actions, actionDef, parentPath) {
  if (actionDef.member) {
    actions[actionDef.key] = function (id) { return parentPath + actionDef.path(id) }
  } else {
    actions[actionDef.key] = function () { return parentPath + actionDef.path() }
  }
}

function resources (path, collectionActions = ['index', 'create'], memberActions = ['show', 'update', 'destroy']) {
  const actions = {}

  collections(...collectionActions).forEach(function (action) {
    addAction(actions, action, API_BASE_URL + path)
  })

  members(...memberActions).forEach(function (action) {
    addAction(actions, action, API_BASE_URL + path)
  })

  return {
    [camelCase(path)]: actions
  }
}

export default {
  ...resources('ambitions',
    ['index', 'create', 'list'],
    ['show', 'update', 'destroy', 'update_assignee', 'update_contributors', 'update_workflows',
      'close', 'reopen']
  ),
  ...resources('workflow_definitions',
    ['index', 'list', 'create'],
    ['show', 'update', 'destroy']
  ),
  ...resources('workflows',
    ['index', 'new', 'create', 'list'],
    ['show', 'update', 'update_assignee', 'update_contributors', 'update_ambitions',
      'complete', 'reopen', 'destroy', 'add_task', 'add_block', 'content_items', 'update_definition']
  ),
  ...resources('tasks',
    ['index', 'list'],
    ['show', 'update', 'update_assignee', 'update_contributors', 'update_marked', 'update_due_date',
      'start', 'complete', 'skip', 'reopen', 'data', 'add_data_field', 'move', 'clone', 'destroy']
  ),
  ...resources('blocks',
    [],
    ['update', 'move', 'destroy']
  ),
  ...resources('data_items',
    [],
    ['update', 'move', 'destroy']
  ),
  ...resources('content_types',
    ['index'],
    []
  ),
  ...resources('events',
    ['index', 'create_comment'],
    ['obsolete_info']
  ),
  ...resources('notifications',
    ['index'],
    ['delivered', 'read', 'bookmark', 'unbookmark', 'done']
  ),
  ...resources('search',
    ['fulltext'],
    []
  ),
  ...resources('dossiers',
    ['index', 'create', 'list', 'new'],
    ['show', 'update', 'destroy', 'show_references']
  ),
  ...resources('dossier_definitions',
    ['index', 'create', 'list'],
    ['show', 'update', 'destroy']
  ),
  ...resources('dossier_field_definitions',
    ['create'],
    ['update', 'destroy', 'move']
  ),
  ...resources('users',
    ['index', 'create', 'list', 'info'],
    ['show']
  ),
  ...resources('user_settings',
    [],
    ['show', 'update', 'update_admin_status', 'update_active_status', 'update_password',
      'update_notification_settings', 'groups']
  ),
  ...resources('groups',
    ['index', 'create', 'list'],
    ['show', 'update', 'destroy']
  ),
  ...resources('uploaded_files',
    ['create'],
    ['update']
  ),
  ...resources('pages', [], ['show'])
}
