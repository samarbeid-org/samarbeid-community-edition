// Styles
import './richtextarea.sass'

// Extensions
import VInput from 'vuetify/lib/components/VInput/VInput'
import VTextField from 'vuetify/lib/components/VTextField/VTextField'
import Mentionable from './mentionable'

// Editor
import { TiptapVuetify, Bold, Italic, Underline, Strike, Link, Blockquote, OrderedList, BulletList, ListItem, TodoItem, TodoList } from 'tiptap-vuetify'

// Utilities
import mixins from 'vuetify/lib/util/mixins'
const baseMixins = mixins(VTextField, Mentionable)

/* @vue/component */
export default baseMixins.extend({
  name: 'RichtextArea',
  props: {
    hint: {
      type: String,
      default: 'Markiere um zu formatieren. Für Verweise im Text: @Nutzer, !Ziel, %Maßnahme, #Aufgabe, *Dossier'
    }
  },
  data () {
    return {
      editor: null,
      extensions: [
        [Bold, { renderIn: 'bubbleMenu' }],
        [Italic, { renderIn: 'bubbleMenu' }],
        [Underline, { renderIn: 'bubbleMenu' }],
        [Strike, { renderIn: 'bubbleMenu' }],
        [Link, { renderIn: 'bubbleMenu', options: { target: '_blank' } }],
        [Blockquote, { renderIn: 'bubbleMenu' }],
        [BulletList, { renderIn: 'bubbleMenu' }],
        [OrderedList, { renderIn: 'bubbleMenu' }],
        [ListItem, { renderIn: 'bubbleMenu' }],
        [TodoList, { renderIn: 'bubbleMenu' }],
        [TodoItem, { renderIn: 'bubbleMenu' }]
      ]
    }
  },
  computed: {
    classes () {
      return {
        richtextarea: true,
        ...VTextField.options.computed.classes.call(this)
      }
    }
  },

  methods: {
    blur (e) {
      // https://github.com/vuetifyjs/vuetify/issues/5913
      // Safari tab order gets broken if called synchronous
      window.requestAnimationFrame(() => {
        this.editor && this.editor.blur()
      })
    },

    clearableCallback () {
      this.editor && this.editor.focus()
      this.$nextTick(() => { this.internalValue = null })
    },

    genTextFieldSlot () {
      return this.$createElement('div', {
        staticClass: 'v-text-field__slot'
      }, [this.genLabel(), this.prefix ? this.genAffix('prefix') : null, this.genInput(), this.genMentionMenu(), this.suffix ? this.genAffix('suffix') : null])
    },

    genInput () {
      const listeners = Object.assign({}, this.listeners$)
      delete listeners.change // Change should not be bound externally

      return this.$createElement(TiptapVuetify, {
        props: {
          value: this.internalValue,
          extensions: this.extensions,
          cardProps: { flat: true },
          disabled: this.isDisabled || this.isReadonly,
          nativeExtensions: [this.mentionableExtension]
        },
        on: {
          init: this.onEditorInit,
          input: this.onEditorInput,
          focus: this.onEditorFocus,
          blur: this.onEditorBlur
        },
        attrs: {
          ...this.attrs$,
          id: this.computedId
        },
        ref: 'tiptapVuetify'
      })
    },

    onClick (event) {
      if (this.isFocused || this.isDisabled || !this.editor) return

      this.editor.focus()
    },

    onFocus (e) {
      if (!this.editor || this.isDisabled) return

      if (!this.editor.focused) {
        this.editor.focus()
      }

      if (!this.isFocused) {
        this.isFocused = true
        e && this.$emit('focus', e)
      }
    },

    onMouseDown (e) {
      VInput.options.methods.onMouseDown.call(this, e)
    },

    tryAutofocus () {
      if (!this.autofocus || typeof document === 'undefined' || !this.editor || !this.editor.focused) return false
      this.editor.focus()
      return true
    },

    onEditorInit ({ editor }) {
      this.editor = editor

      this.$nextTick(function () {
        const contentElement = this.$el.querySelector('div.tiptap-vuetify-editor__content')
        contentElement.addEventListener('focusin', this.onEditorContentFocusin)
        contentElement.addEventListener('focusout', this.onEditorContentFocusout)
      })
    },

    onEditorInput (output, info) {
      const json = info.getJSON().content

      if (Array.isArray(json) && json.length === 1 && !Object.prototype.hasOwnProperty.call(json[0], 'content')) {
        this.internalValue = null
      } else {
        this.internalValue = info.getHTML()
      }
    },

    onEditorFocus (event, view) {
      this.onFocus(event)
    },

    onEditorBlur (event, view) {
      if (this.isFocused && !this.$refs.tiptapVuetify.$el.contains(event.relatedTarget)) {
        this.onBlur(event)
      }
    },

    onEditorContentFocusin (e) {
      if (!this.editor || this.isDisabled) return

      if (!this.isFocused) {
        this.isFocused = true
        e && this.$emit('focus', e)
      }
    },

    onEditorContentFocusout (event) {
      if (this.isFocused && !this.$refs.tiptapVuetify.$el.contains(event.relatedTarget)) {
        this.onBlur(event)
      }
    },

    mentionableOnInsert () {
      if (this.editor) this.editor.focus()
    }
  }
})
