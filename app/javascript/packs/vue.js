/* eslint no-console: 0 */

import Vue from 'vue'
import router from 'router'
import i18n from 'i18n'
import vuetify from 'plugins/vuetify'
import 'plugins/v-currency-field'
import 'plugins/action-cable'
import config from 'helpers/config'
import apiEndpoints from 'api/endpoints'
import SiteHeader from 'components/site-header'
import SiteFooter from 'components/site-footer'
import browserDetect from 'vue-browser-detect-plugin'
import * as Sentry from '@sentry/vue'

if (config.sentry_dsn?.length) {
  Sentry.init({
    Vue,
    dsn: config.sentry_dsn
  })
}

Vue.use(browserDetect)

document.addEventListener('DOMContentLoaded', () => {
  (() => new Vue({
    el: '#vue-app',
    i18n,
    vuetify,
    apiEndpoints,
    config,
    router,
    components: { SiteHeader, SiteFooter }
  }))()
})
