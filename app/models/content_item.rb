class ContentItem < ApplicationRecord
  include ContentTypeSupport
  include ReferenceSupport

  belongs_to :workflow
  has_many :task_items, dependent: :destroy
  belongs_to :content_item_definition, optional: true
  has_many :blocks, dependent: :nullify

  validates :label, uniqueness: {scope: :workflow_id}
  validates :label, presence: true
  validates :label, format: {without: /\./, message: "darf keine Punkte enthalten"}
  validate :validate_value

  after_update :reindex_workflow
  after_destroy :reindex_workflow

  include AASM

  aasm whiny_persistence: true do
    state :undefined, initial: true
    state :confirmed

    event :confirm do
      transitions from: :undefined, to: :confirmed, after: -> { update(confirmed_at: Time.now) }
    end

    event :change do
      transitions from: :confirmed, to: :undefined, after: -> { update(confirmed_at: nil) }
    end
  end

  def value
    content_type.deserialize(read_attribute(:value))
  end

  def localized_value
    localized_string(value)
  end

  def value=(new_value)
    casted_new_value = content_type.serialize(content_type.cast(new_value))
    return if casted_new_value == value

    write_attribute(:value, casted_new_value)
    change if may_change?
  end

  def search_data
    case content_type.name
    when ContentTypes::File.name
      value.map { |doc| doc.search_data("#{label} - ") }.reduce({}, :merge)
    else
      {label => " #{localized_value} "}
    end
  end

  def reference_label(_noaccess)
    ""
  end

  private

  def validate_value
    return if content_type.data_empty?(value)
    validation_result = content_type.validate_data(value)
    errors.add(:value, validation_result) if validation_result != true
  end

  def reindex_workflow
    workflow.reindex
  end
end
