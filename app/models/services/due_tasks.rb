class Services::DueTasks < ApplicationRecord
  def self.find_and_process
    find_unprocessed.find_each do |due_task|
      due_task.reindex
      Events::DueEvent.create!(object: due_task)
      UserChannel.broadcast_info(due_task.assignee) if due_task.assignee
      due_task.update!(due_processed: true)
    end
  end

  def self.find_unprocessed
    Task.open.workflow_open.with_due_in_past.where(due_processed: false)
  end
end
