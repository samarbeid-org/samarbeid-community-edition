class Services::ClonedTask
  attr_accessor :original_task
  def initialize(task)
    @original_task = task
  end

  def create
    cloned_task = build_clone
    move_all_existing_tasks_one_down
    cloned_task.save
    cloned_task
  end

  def build_clone
    # Maybe some day it is more elegant to do this by building a definition from given task and to build a task from
    # that definition (needed for modified stuff + tasks without definition)

    cloned_task = Task.new(name: "#{@original_task.name} (Kopie #{next_free_label_counter})", position: @original_task.position + 1,
      description: @original_task.description, workflow_or_block: @original_task.workflow_or_block)

    cloned_task.workflow = @original_task.workflow_or_block if @original_task.workflow_or_block.is_a?(Workflow)
    cloned_task.workflow = @original_task.workflow_or_block.workflow if @original_task.workflow_or_block.is_a?(Block)

    cloned_task.update(due_at: nil, assignee: nil, contributors: [])

    cloned_task.task_items = build_task_item_clones

    cloned_task
  end

  def build_task_item_clones
    @original_task.task_items.map do |o_ti|
      if o_ti.info_box
        TaskItem.new(position: o_ti.position, info_box: o_ti.info_box, required: o_ti.required,
          content_item: o_ti.content_item)
      else
        TaskItem.new(position: o_ti.position, info_box: o_ti.info_box, required: o_ti.required,
          content_item: build_content_item_clone(o_ti.content_item))
      end
    end.to_a
  end

  def build_content_item_clone(content_item)
    ContentItem.new(label: "#{content_item.label} (Kopie #{next_free_label_counter})",
      content_type: content_item.content_type.to_s, options: content_item.options, value: nil,
      workflow: @original_task.workflow)
  end

  def next_free_label_counter
    content_items_to_clone = @original_task.task_items.select { |ti| !ti.info_box }.map(&:content_item)
    max_counter_for_content_items = content_items_to_clone.map { |ci| @original_task.workflow.content_items.map(&:label).count { |t| t.include?(ci.label) } }.max

    max_counter_for_task_name = @original_task.workflow.all_tasks.select(&:not_deleted?).map(&:title).count { |t| t.include?(@original_task.title) }

    [max_counter_for_content_items.to_i, max_counter_for_task_name].max
  end

  def move_all_existing_tasks_one_down
    items_behind_original = @original_task.workflow_or_block.items.select { |item| item.position > @original_task.position }
    items_behind_original.each { |item| item.increment(:position) }
  end
end
