class Services::WorkflowToDefinition
  attr_accessor :workflow
  def initialize(workflow)
    raise "Must be initialized with a workflow instead of: #{workflow.inspect}" unless workflow.is_a?(Workflow)
    @workflow = workflow
  end

  def update!
    ActiveRecord::Base.transaction do
      update_attributes(@workflow, [:description])

      update_from_content_items
      update_from_blocks
      update_from_tasks
      update_from_task_items

      @workflow.workflow_definition.increment!(:version)
    end
  end

  def update_attributes(instance, attributes)
    definition = definition_for(instance)
    attributes.each do |attribute|
      definition.update!(attribute => instance.send(attribute)) unless definition.send(attribute).eql?(instance.send(attribute))
    end
  end

  def update_from_content_items
    ci_definitions_not_to_delete = []
    @workflow.content_items.each do |ci|
      if ci.content_item_definition
        ci.content_item_definition.update!(label: ci.label, options: ci.options, workflow_definition: @workflow.workflow_definition)
        ci_definitions_not_to_delete << ci.content_item_definition
      else
        new_definition = ContentItemDefinition.create!(label: ci.label, content_type: ci.content_type.to_s, options: ci.options,
          workflow_definition: @workflow.workflow_definition)
        ci.update!(content_item_definition: new_definition)
        ci_definitions_not_to_delete << new_definition
      end
    end
    content_item_defs_to_delete = @workflow.workflow_definition.content_item_definitions.reject { |definition| definition.in?(ci_definitions_not_to_delete) }
    content_item_defs_to_delete.each { |definition| definition.update!(deleted: true) }
  end

  def update_from_blocks
    block_defs_not_to_delete = []
    @workflow.blocks.select(&:not_deleted?).each do |block|
      new_content_item_definition = @workflow.workflow_definition.content_item_definitions.find { |cid| cid.label == block.content_item&.label }

      if block.block_definition
        block.block_definition.update!(title: block.title, position: block.position, parallel: block.parallel,
          decision: block.decision, content_item_definition: new_content_item_definition)
        block_defs_not_to_delete << block.block_definition
      else
        new_block_definition = BlockDefinition.create!(title: block.title, position: block.position, parallel: block.parallel,
          decision: block.decision, content_item_definition: new_content_item_definition,
          workflow_definition: @workflow.workflow_definition)
        block.update!(block_definition: new_block_definition)
        block_defs_not_to_delete << new_block_definition
      end
    end
    block_defs_to_delete = @workflow.workflow_definition.block_definitions.reject { |definition| definition.in?(block_defs_not_to_delete) }
    block_defs_to_delete.each { |definition| definition.direct_task_definitions.each { |task_def| task_def.update!(definition_workflow_or_block: @workflow.workflow_definition) } }
    block_defs_to_delete.each { |definition| definition.update!(deleted: true) }
  end

  def update_from_tasks
    task_defs_not_to_delete = []
    @workflow.all_tasks.select(&:not_deleted?).each do |task|
      if task.task_definition
        task.task_definition.update!(name: task.name, description: task.description, position: task.position,
          definition_workflow_or_block: definition_for(task.workflow_or_block))
        task_defs_not_to_delete << task.task_definition
      else
        new_task_definition = TaskDefinition.create!(name: task.name, position: task.position, description: task.description,
          definition_workflow_or_block: definition_for(task.workflow_or_block), workflow_definition: @workflow.workflow_definition)
        task.update!(task_definition: new_task_definition)
        task_defs_not_to_delete << new_task_definition
      end
    end
    task_defs_to_delete = @workflow.workflow_definition.all_task_definitions.reject { |definition| definition.in?(task_defs_not_to_delete) }
    task_defs_to_delete.each { |definition| definition.update!(definition_workflow_or_block: @workflow.workflow_definition, deleted: true) } # Update workflow_definition_or_block because we have invalid data as we did not change tasks in block action before (may be removed later).
  end

  def update_from_task_items
    task_item_defs_not_to_delete = @workflow.all_tasks.select(&:not_deleted?).flat_map do |task|
      update_task_items_for(task)
    end
    task_item_defs_to_delete = @workflow.workflow_definition.all_task_definitions.flat_map(&:task_item_definitions).reject { |definition| definition.in?(task_item_defs_not_to_delete) }
    task_item_defs_to_delete.each { |definition| definition.update!(deleted: true) }
  end

  def update_task_items_for(task)
    task_item_defs_not_to_delete = []
    task.task_items.each do |task_item|
      if task_item.task_item_definition
        task_item.task_item_definition.update!(position: task_item.position, required: task_item.required, info_box: task_item.info_box)
        task_item_defs_not_to_delete << task_item.task_item_definition
      else
        content_item_definition = @workflow.workflow_definition.content_item_definitions.find { |cid| cid.label == task_item.content_item.label }
        new_task_item_definition = TaskItemDefinition.create!(position: task_item.position, required: task_item.required, info_box: task_item.info_box,
          task_definition: task.task_definition, workflow_definition: @workflow.workflow_definition,
          content_item_definition: content_item_definition)
        task_item.update!(task_item_definition: new_task_item_definition)
        task_item_defs_not_to_delete << new_task_item_definition
      end
    end
    task_item_defs_not_to_delete
  end

  def definition_for(instance)
    return instance.workflow_definition if instance.is_a?(Workflow)
    return instance.block_definition if instance.is_a?(Block)
    return instance.task_definition if instance.is_a?(Task)
  end
end
