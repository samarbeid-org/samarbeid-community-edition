class Services::WorkflowsToTable
  include ExcelExport

  def initialize(workflows = [])
    @definitions_and_items = workflows.group_by(&:workflow_definition).sort_by { |wd, _| wd.name }
  end

  def base_columns
    ["Maßnahme-ID", "Maßnahmetitel", "Status", "Verantwortlicher", "Teilnehmer", "Erstellungsdatum", "Ziel"]
  end

  def definition_columns(workflow_definition)
    content_item_definitions_sorted(workflow_definition).map(&:label)
  end

  def values_for_base_columns(workflow)
    [workflow, workflow.title, workflow.aasm_state, workflow.assignee&.email, workflow.contributors.map(&:email),
      workflow.created_at.strftime("%Y-%m-%d %H:%M"), workflow.ambitions.map(&:title)]
  end

  def values_for_definition_columns(workflow)
    values_for_content_item_definitions_in(workflow)
  end

  def values_for_content_item_definitions_in(workflow)
    content_item_definitions_sorted(workflow.workflow_definition).map do |definition|
      workflow.content_items.find { |ci| ci.content_item_definition.eql?(definition) }
    end
  end

  def content_item_definitions_sorted(workflow_definition)
    workflow_definition.content_item_definitions.sort_by(&:label)
  end
end
