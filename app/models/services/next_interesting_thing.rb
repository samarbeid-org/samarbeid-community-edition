class Services::NextInterestingThing
  attr_accessor :workflow, :user
  def initialize(workflow, user)
    @workflow = workflow
    @user = user
  end

  def next_station
    return workflow if workflow.completable? # wenn Maßnahme abschließbar wird, dann Maßnahmeübersicht

    # die Liste aller Aufgaben wird von oben nach unten durchgegangen und die Aufgabe wird "nächste interessante Station" wenn:
    # aktiv und Nutzer ist Verantwortlicher
    # bereit und Nutzer ist Verantwortlicher
    # aktiv und hat keinen Verantwortlichen
    # bereit und hat keinen Verantwortlichen
    workflow.tasks_ordered_in_structure.each do |task|
      return task if task.aasm_state.in?(["active", "ready"]) && task.assignee.in?([user, nil])
    end

    # wenn es keine dieser Aufgaben gibt, wird die oberste aktive oder bereite Aufgabe die einen anderen Verantwortlichen hat
    workflow.tasks_ordered_in_structure.each do |task|
      return task if task.aasm_state.in?(["active", "ready"])
    end

    # wenn es auch eine solche Aufgabe nicht gibt, default oberste erstellte Aufgabe
    tasks_with_state("created").first
  end

  def tasks_with_state(state)
    workflow.tasks_ordered_in_structure.select do |task|
      task.send("#{state}?")
    end
  end
end
