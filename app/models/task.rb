class Task < ApplicationRecord
  include SearchForIndexWithCategories
  include ReferenceSupport

  after_commit :broadcast_user_info_changes, on: :update
  after_save :reindex_self_and_workflow, if: :saved_change_to_aasm_state?

  belongs_to :workflow
  belongs_to :task_definition, optional: true

  belongs_to :workflow_or_block, polymorphic: true, inverse_of: :direct_tasks

  has_many :task_items, -> { order(:position) }, inverse_of: :task, dependent: :destroy

  alias_attribute :title, :name

  belongs_to :assignee, class_name: "User", optional: true

  has_many :contributions, as: :contributable, dependent: :destroy
  has_many :contributors, -> { order "contributions.created_at" }, through: :contributions, inverse_of: false, source: :user, after_add: :reindex_self_and_workflow, after_remove: :reindex_self_and_workflow

  has_and_belongs_to_many :marked_by_users, class_name: "User", foreign_key: :workflow_task_id, join_table: :users_workflow_tasks_marked, after_add: :reindex_self, after_remove: :reindex_self

  has_many :comments, as: :object, dependent: :destroy

  searchkick language: "german", callbacks: :async, word_middle: [:identifier, :title, :subtitle], suggest: [:title, :subtitle]
  scope :not_deleted, -> {
                        where(aasm_state: [:created, :ready, :active, :completed, :skipped])
                          .joins(:workflow).where(workflows: {aasm_state: [:ready, :active, :completed]})
                      }
  scope :search_import, -> { not_deleted }

  def should_index?
    not_deleted?
  end

  validates :name, :workflow, presence: true
  validate :assignee_has_access

  include AASM

  aasm whiny_persistence: true do
    state :created, initial: true, after_exit: :set_due_date
    state :ready, after_enter: -> { Events::ReadiedEvent.create!(object: self) }
    state :active, :completed, :skipped, :deleted

    after_all_events do
      update!(state_updated_at: Time.now)
      Services::NewEngine.new(workflow).run
    end

    event :enable do
      transitions from: :created, to: :ready, if: -> { workflow.active? && workflow_or_block.active? }
    end

    event :start do
      transitions from: [:created, :ready], to: :active, if: -> { workflow.active? }
    end

    event :snooze do
      transitions from: :active, to: :ready, if: -> { workflow.active? }
    end

    event :complete do
      transitions from: :active, to: :completed, if: -> { workflow.active? && completable? }
      after do
        update!(completed_at: Time.now)
        confirm_content_items
      end
    end

    event :reopen do
      transitions from: [:completed, :skipped], to: :active, if: -> { workflow.active? },
        after: -> { maybe_reopen_block }
    end

    event :skip do
      transitions from: [:created, :ready, :active], to: :skipped, if: -> { workflow.active? }
    end

    event :delete_for_ever do
      transitions to: :deleted, if: -> { workflow.active? }
    end
  end

  def open?
    created? || ready? || active?
  end

  def closed?
    !open?
  end

  def not_deleted?
    !deleted? && workflow.not_deleted?
  end

  # helper for mentioning
  def trashed?
    workflow.deleted? || workflow.trashed?
  end

  def set_due_date
    update!(due_at: Time.now + task_definition.due_in_hours.hours) if task_definition&.due_in_hours
  end

  def maybe_reopen_block
    if workflow_or_block.is_a?(Block) && workflow_or_block.completed?
      workflow_or_block.reopen!
    end
  end

  def confirm_content_items
    task_items.map(&:content_item).each do |content_item|
      content_item.confirm! if content_item.may_confirm?
    end
  end

  scope :open, -> { ready.or(active).or(created) }
  scope :closed, -> { completed.or(deleted).or(skipped) }
  scope :workflow_open, -> { joins(:workflow).where(workflows: {aasm_state: :active}) }
  scope :current_and_assigned_to, ->(user) { accessible_by(user.ability).workflow_open.ready.or(active).where(assignee: user) }
  scope :with_due_in_past, -> { where("due_at <= ?", Date.today) }
  scope :with_start_at_in_past, -> { where("start_at <= ?", Time.now) }

  def completable?
    if task_items.select(&:required).any? { |task_item| task_item.content_item.content_type.data_empty?(task_item.content_item.value) }
      errors.add(:base, "Aufgabe kann erst abgeschlossen werden, wenn Daten vollständig eingegeben sind.")
      return false
    end
    true
  end

  def previous_items
    own_index = workflow_or_block.items.index(self)
    return [] if own_index == 0
    workflow_or_block.items[0..(own_index - 1)]
  end

  def events
    Event.where(object: self).order(:created_at)
  end

  def marked?(user)
    marked_by_users.exists?(user.id)
  end

  def due?
    open? && due_at.present? && due_at <= Date.today
  end

  def due_at=(new_due_date)
    super(new_due_date)
    if due_processed && !due?
      update(due_processed: false)
    end
  end

  def contributors=(new_contributors)
    super(new_contributors)
    reindex_self_and_workflow
  end

  def marked_by_users=(new_users)
    super(new_users)
    reindex_self
  end

  def assignee=(new_assignee)
    super(new_assignee)
    reindex_self_and_workflow
  end

  def self.define_tab_categories
    [
      ["TODO", ->(t) { (t.ready? || t.active?) && t.workflow.active? }],
      ["DUE", ->(t) { t.open? && t.due? && t.workflow.active? }],
      ["MARKED", ->(t) { true && t.workflow.not_deleted? }],
      ["ALL", ->(t) { true && t.workflow.not_deleted? }]
    ]
  end

  def self.marked_count_query(user, query_params)
    query_params = query_params.deep_dup
    query_params[:where][:marked_by_user_ids] = user.id
    query_params[:where][:tab_categories] = "MARKED"
    query_params[:execute] = false
    Task.search_for_index(user, "*", false, query_params)
  end

  def identifier
    "##{id}"
  end

  # ReferenceSupport
  def reference_label(noaccess = false)
    identifier + (noaccess || trashed? || deleted? ? "" : " • #{title}")
  end

  def mention_label(noaccess = false)
    reference_label(noaccess)
  end

  private

  def search_data
    {
      id_sort: id.to_s.rjust(8, "0"),
      created_at: created_at,
      due_at_for_sorting: due_at_for_sorting,
      identifier: [identifier, id.to_s],
      title: title&.downcase,
      subtitle: "#{workflow.name} > #{workflow.title}",
      description: read_attribute(:description), # only index custom descriptions
      comments: comments.map(&:message),
      tab_categories: tab_categories,
      assignee_id: assignee_id,
      contributor_ids: contributors.map(&:id),
      workflow_assignee_id: workflow.assignee_id,
      workflow_definition_id: workflow.workflow_definition.id,
      ambition_ids: workflow.ambitions.not_deleted.map(&:id),
      task_definition_id: task_definition_id,
      marked_by_user_ids: marked_by_user_ids
    }.merge(visibility_for_groups)
  end

  def due_at_for_sorting
    return due_at if due_at.present?
    Time.at(Time.new(3000) - created_at).to_datetime
  end

  def visibility_for_groups
    {visible_for_groups: workflow.workflow_definition.groups.map(&:id)}
  end

  def reindex_self(_params = nil)
    reindex
  end

  def reindex_self_and_workflow(_params = nil)
    reindex
    workflow.reindex
  end

  def assignee_has_access
    return unless assignee
    errors.add(:assignee, "must have access") unless assignee.can?(:read, self)
  end

  def broadcast_user_info_changes
    if previous_changes.key?(:aasm_state) || previous_changes.key?(:due_at) || previous_changes.key?(:assignee_id)
      UserChannel.broadcast_info(assignee) unless assignee.nil?

      if previous_changes.key?(:assignee_id)
        old_assignee = User.find_by(id: previous_changes[:assignee_id].first)
        UserChannel.broadcast_info(old_assignee) unless old_assignee.nil?
      end
    end
  end
end
