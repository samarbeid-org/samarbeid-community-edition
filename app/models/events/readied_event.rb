class Events::ReadiedEvent < Event
  validate :validate_event_type_values

  def notification_receivers
    # TODO: as soon as we have subject this should be implemented as follows:
    # return [object.assignee].compact - [subject] if object.assignee.present?
    # return [object.workflow.assignee] - [subject] if object.workflow.assignee.present?
    []
  end

  private

  def validate_event_type_values
    validate_object_is_one_of(Task)
  end
end
