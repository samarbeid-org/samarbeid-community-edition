class Events::ReopenedEvent < Event
  validate :validate_event_type_values

  validates :subject, presence: true

  def notification_receivers
    return [object.assignee] - [subject] if object.assignee.present?
    return [object.workflow.assignee] - [subject] if object.is_a?(Task) && object.workflow.assignee.present?
    []
  end

  private

  def validate_event_type_values
    validate_object_is_one_of(Ambition, Workflow, Task)
  end
end
