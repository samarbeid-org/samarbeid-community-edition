class TaskItem < ApplicationRecord
  belongs_to :task
  belongs_to :content_item
  belongs_to :task_item_definition, optional: true

  validates :position, presence: true

  def as_datafield
    {
      definition: OpenStruct.new(
        id: content_item.id,
        name: content_item.label,
        content_type: content_item.content_type,
        required: required,
        options: content_item.options,
        infobox: info_box,
        task_item_id: id,
        task_item_count: content_item.task_items.count,
        block_count: content_item.blocks.count
      ),
      value: content_item.value,
      state: content_item.aasm_state,
      confirmed_at: content_item.confirmed_at,
      last_updated: last_data_change&.created_at,
      last_updated_by: last_data_change&.subject
    }
  end

  def last_data_change
    Event.where(object: content_item).order(:created_at).last
  end
end
