class Group < ApplicationRecord
  include SearchForIndexWithCategories

  has_many :user_groups, dependent: :destroy
  has_many :users, through: :user_groups, inverse_of: :groups, dependent: :destroy

  has_and_belongs_to_many :workflow_definitions, after_add: :acl_reindex, after_remove: :acl_reindex
  has_and_belongs_to_many :dossier_definitions, after_add: :acl_reindex, after_remove: :acl_reindex

  validates :name, presence: true, uniqueness: true

  searchkick language: "german", callbacks: :async, word_middle: [:name, :email], suggest: [:name, :email]

  def self.defaults_for_search_for_index
    {fields: [:name]}
  end

  def search_data
    {
      name: name&.downcase,
      created_at: created_at
    }.merge(visibility_for_groups)
  end

  def visibility_for_groups
    {visible_for_groups: Group.all.map(&:id)}
  end

  private

  def acl_reindex(_params)
    RunAclReindexOnlyJob.perform_later
  end
end
