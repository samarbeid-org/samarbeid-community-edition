require "active_support/concern"

module NotificationSupport
  extend ActiveSupport::Concern

  included do
    def schedule_next_delivery(interval = noti_interval)
      # This method should do both, update the object and return the corresponding datetime.
      case interval
      when 0
        # never
        self.noti_next_send_at = nil
      when 1
        # immediately
        self.noti_next_send_at = 1.minute.from_now.beginning_of_minute
      when 1.hour
        # if the first event is e.g. at hh:58 and another one within the next minute, the user would get two mails within ~ 2 minutes: at :59 and at :00... so here's a cut-off: if next scheduling would only be 5 minutes in the future, we schedule one hour later.
        cutoff = 5.minutes
        self.noti_next_send_at = (1.hour.from_now + cutoff).beginning_of_hour
      when 1.day
        # If the first event since last sending happens between midnight an 6:00 we won't send another mail for the day.
        self.noti_next_send_at = DateTime.now.tomorrow.beginning_of_day + 6.hours
      when 1.week
        # If the first event of the week happens monday between midnight and 6:00, it will then send again only in the next week.
        self.noti_next_send_at = DateTime.now.next_week + 6.hours
      else
        # If mail is delivered within 10 minutes after scheduled, it will delay `interval` seconds after last-scheduled, otherwise after now.
        base_date = noti_next_send_at && (noti_next_send_at < 10.minutes.ago || noti_next_send_at > DateTime.now) ? noti_next_send_at : DateTime.now
        self.noti_next_send_at = interval.to_i.seconds.from_now(base_date).beginning_of_minute
      end
    end

    def persist_sending_notification_and_schedule_next
      update_columns(noti_last_sent_at: DateTime.now, noti_next_send_at: schedule_next_delivery)
    end

    scope :due_for_notification, lambda {
                                   where("users.noti_next_send_at is not null and users.noti_next_send_at <= NOW()")
                                 }

    scope :unsent_notifications_count, lambda {
      with_unsent_notifications.count
    }
    scope :with_unsent_notifications, lambda {
      joins(:notifications).where("notifications.created_at > users.noti_last_sent_at").where("notifications.delivered_at": nil).group(:id)
    }

    scope :for_sending_notifications, -> { active.due_for_notification.with_unsent_notifications }

    def unseen_and_unsent_notifications
      notifications.undelivered.since(noti_last_sent_at).order(created_at: :desc)
    end

    def noti_next_send_at_for_display
      # This makes sure we're not showing dates in the past
      noti_next_send_at && [noti_next_send_at, 1.minute.from_now.beginning_of_minute].max
    end
  end

  class_methods do
    def allowed_notification_intervals
      [
        1, # asap
        1.hour, # every hour
        1.day,
        1.week,
        0 # never
      ]
    end

    def notification_interval_label(interval)
      # TODO: move to I18n
      case interval
      when 0
        "nie"
      when 1
        "sofort"
      when 1.hour
        "stündlich"
      when 1.day
        "täglich"
      when 1.week
        "wöchentlich"
      else
        "ca. alle #{distance_of_time_in_words(interval)}"
      end
    end

    def notification_select_options
      allowed_notification_intervals.map { |i| [i.to_i, notification_interval_label(i)] }
    end
  end
end
