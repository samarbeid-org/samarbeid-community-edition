class Workflow < ApplicationRecord
  include SearchForIndexWithCategories
  include ReferenceSupport

  has_many :all_tasks, -> { order(:position) }, class_name: "Task", inverse_of: :workflow, dependent: :destroy
  has_many :direct_tasks, -> { order(:position) }, class_name: "Task", as: :workflow_or_block, inverse_of: :workflow_or_block
  has_many :blocks, -> { order(:position) }, dependent: :destroy
  has_many :content_items, dependent: :destroy
  def items
    (direct_tasks + blocks).select(&:not_deleted?).sort_by(&:position)
  end

  def tasks_ordered_in_structure
    items.flat_map { |item| item.is_a?(Block) ? item.items : item }
  end

  belongs_to :workflow_definition

  has_and_belongs_to_many :ambitions, after_add: :reindex_ambition_and_self, after_remove: :reindex_ambition_and_self

  belongs_to :assignee, class_name: "User", optional: true

  has_many :contributions, as: :contributable, dependent: :destroy
  has_many :contributors, -> { order "contributions.created_at" }, through: :contributions, inverse_of: false, source: :user, after_add: :reindex_tasks_and_self, after_remove: :reindex_tasks_and_self

  belongs_to :predecessor_workflow, class_name: "Workflow", optional: true
  has_many :successor_workflows, class_name: "Workflow", foreign_key: "predecessor_workflow_id", inverse_of: :predecessor_workflow, dependent: :nullify

  has_many :comments, as: :object, dependent: :destroy
  has_many :events, as: :object, dependent: :destroy

  validates_presence_of :title

  validate :assignee_has_access

  after_save :reindex_tasks_and_self

  searchkick language: "german", callbacks: :async, word_middle: [:identifier, :title, :subtitle], suggest: [:title, :subtitle]

  scope :not_deleted, -> { where(aasm_state: [:ready, :active, :completed]) }
  scope :search_import, -> { not_deleted }

  def should_index?
    not_deleted?
  end

  delegate :name, to: :workflow_definition

  include AASM
  after_create do
    items.first.enable! if items.first&.may_enable?
  end
  aasm whiny_persistence: true do
    state :active, initial: true
    state :completed, :trashed, :deleted

    after_all_events do
      update!(state_updated_at: Time.now)
      Services::NewEngine.new(self).run
    end

    event :complete do
      transitions from: :active, to: :completed,
        after: -> { update!(completed_at: Time.now) }
    end
    event :reopen do
      transitions from: :completed, to: :active,
        after: -> { update!(completed_at: nil) }
    end
    event :trash do
      transitions from: [:active, :completed], to: :trashed
    end
    event :untrash do
      transitions from: :trashed, to: :completed, if: -> { completed_at.present? }
      transitions from: :trashed, to: :active, unless: -> { completed_at.present? }
    end
    event :delete_for_ever do
      transitions from: :trashed, to: :deleted,
        after: -> { update!(deleted_at: Time.now) }
    end
  end

  def completable?
    direct_tasks.select(&:not_deleted?).all?(&:closed?) &&
      blocks.select(&:not_deleted?).select(&:condition_valid?).all? { |block| block.items.all?(&:closed?) }
  end

  def not_deleted?
    !(trashed? || deleted?)
  end

  def due_tasks_count
    all_tasks.count(&:due?)
  end

  def self.define_tab_categories
    [
      ["ACTIVE", ->(w) { w.active? }],
      ["WITH_DUE_TASKS", ->(w) { w.active? && w.all_tasks.any?(&:due?) }],
      ["ALL", ->(w) { w.not_deleted? }]
    ]
  end

  def ambitions=(ambitions)
    super(ambitions)
    ambitions.each { |a| a.reindex }
  end

  def contributors=(new_contributors)
    super(new_contributors)
    reindex_tasks_and_self
  end

  def identifier
    "%#{id}"
  end

  # ReferenceSupport
  def reference_label(noaccess = false)
    identifier + (noaccess || trashed? || deleted? ? "" : " • #{title}")
  end

  def mention_label(noaccess = false)
    reference_label(noaccess)
  end

  private

  def search_data
    {
      id_sort: id.to_s.rjust(8, "0"),
      identifier: [identifier, id.to_s],
      created_at: created_at,
      title: title&.downcase,
      subtitle: name,
      description: description,
      comments: comments.map(&:message),
      ambitions: ambitions.not_deleted.map(&:title),
      ambition_ids: ambitions.not_deleted.map(&:id),
      tab_categories: tab_categories,
      assignee_id: assignee_id,
      contributor_ids: contributors.map(&:id),
      workflow_definition_ids: workflow_definition&.id
    }.merge(content_items_search_data).merge(visibility_for_groups)
  end

  def visibility_for_groups
    {visible_for_groups: workflow_definition.groups.map(&:id)}
  end

  def content_items_search_data
    content_items.reject { |ci| ci.value.nil? }.map(&:search_data).reduce({}, :merge)
  end

  def reindex_tasks_and_self(_params = nil)
    reindex
    all_tasks.all.each(&:reindex)
  end

  def reindex_ambition_and_self(ambition)
    ambition.reindex

    reindex
  end

  def assignee_has_access
    return unless assignee
    return unless assignee_id_changed?
    errors.add(:assignee, "must have access") unless assignee.can?(:read, self)
  end
end
