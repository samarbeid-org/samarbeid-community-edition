class MenuEntry < ApplicationRecord
  belongs_to :page

  enum location: {footer_menu: "footer_menu"}

  validates :location, :position, :title, :page, presence: true
  validates :position, uniqueness: {scope: :location}
end
