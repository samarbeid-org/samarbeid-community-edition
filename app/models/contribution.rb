class Contribution < ApplicationRecord
  belongs_to :contributable, polymorphic: true
  belongs_to :user
end
