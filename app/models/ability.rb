class Ability
  include CanCan::Ability

  def initialize(user)
    #################################
    # Normal User
    return unless user

    can [:show, :list, :info], User
    can [:show_settings, :update, :update_password, :update_notification_settings, :groups], User, id: user.id

    can :manage, Notification, user: user

    can :read, Services::Search

    can :show, Group, id: user.group_ids

    can :create, UploadedFile

    can [:read, :list], DossierDefinition, groups: {id: user.group_ids}
    can :manage, Dossier, definition: {groups: {id: user.group_ids}}

    can :manage, Ambition

    can :manage, Task, workflow: {workflow_definition: {groups: {id: user.group_ids}}}
    can :manage, TaskItem, task: {workflow: {workflow_definition: {groups: {id: user.group_ids}}}}
    can :manage, Block, workflow: {workflow_definition: {groups: {id: user.group_ids}}}

    can [:manage, :read, :new, :create, :update, :update_assignee, :update_contributors, :update_private_status], Workflow, workflow_definition: {groups: {id: user.group_ids}}

    can [:read, :list], WorkflowDefinition, groups: {id: user.group_ids}

    #################################
    # Admins
    return unless user.is_admin?

    can :manage, WorkflowDefinition
    can :manage, DossierDefinition
    can :manage, DossierFieldDefinition

    can :manage, User
    cannot [:update_admin_status, :update_active_status], User, id: user.id

    can :manage, Group

    #################################
    # Super Admins
    return unless user.super_admin?

    can :manage, :all # grant access to do anything on all models
    can :access, :rails_admin # grant access to rails_admin
  end
end
