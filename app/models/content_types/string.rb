class ContentTypes::String < ContentTypes::Value
  class << self
    def type
      :string
    end

    def cast(value)
      ActiveModel::Type::String.new.cast(value) unless value.nil?
    end

    def data_empty?(value)
      super || value.blank?
    end
  end
end
