class ContentTypes::Richtext < ContentTypes::String
  class << self
    def type
      :richtext
    end

    def serialize(value)
      value, _ = Services::Mentioning.extract_mentions(Services::EditorSanitizer.sanitize(value)) unless value.nil?
      super(value)
    end

    def localized_string(value, options)
      with_labels = Services::Mentioning.add_mention_labels(value)
      # Note: This is a quick-n-easy HTML to TXT conversion which allows to read/understand.
      # If required we could use an external library to cover all possible markup in a standard way
      # (for instance Markdown).
      readable_text = with_labels.gsub("</p>", "</p>\n").gsub("<br />", "<br />\n")
      ApplicationController.helpers.strip_tags(readable_text)
    end

    def data_empty?(value)
      super || Nokogiri::HTML.fragment(value).text.blank?
    end
  end
end
