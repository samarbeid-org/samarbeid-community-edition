class Interactors::DossierFieldDefinitionInteractor
  def self.create(dossier_definition, params)
    last_position = dossier_definition.fields.maximum(:position) || 0
    dossier_definition.fields.create(params.merge(position: last_position + 1))
  end

  def self.update(dossier_field_definition, params)
    dossier_field_definition.update(params)
  end

  def self.destroy(dossier_field_definition)
    if dossier_field_definition.destroy
      dossier_field_definition.definition.title_fields -= [dossier_field_definition.id]
      dossier_field_definition.definition.subtitle_fields -= [dossier_field_definition.id]
      dossier_field_definition.definition.save

      dossier_field_definition.definition.dossiers.each do |dossier|
        dossier.remove_field_value(dossier_field_definition.id)
        dossier.save
      end
    end

    dossier_field_definition
  end

  def self.move(dossier_field_definition, index)
    target = dossier_field_definition.definition

    position = (target.fields[index]&.position || (target.fields.max_by(&:position)&.position || 0))
    position -= 1 if index < target.fields.index(dossier_field_definition)

    target.fields.where("position > ?", position).update_all("position = position + 1")
    dossier_field_definition.update(position: position + 1)
  end
end
