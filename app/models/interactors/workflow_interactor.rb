class Interactors::WorkflowInteractor
  def self.new(workflow_definition, user)
    workflow = workflow_definition.build_workflow
    workflow.assignee = user
    workflow
  end

  def self.create(workflow_definition, params, ambition, predecessor_workflow, user)
    workflow = workflow_definition.build_workflow
    workflow.attributes = params

    if workflow.save
      Events::StartedEvent.create!(subject: user, object: workflow)

      if workflow.saved_change_to_assignee_id?
        Events::AssignedEvent.create!(subject: user, object: workflow, data: {new_assignee: workflow.assignee, old_assignee: nil})
        add_contributor_if_needed(workflow, workflow.assignee)
      end

      add_contributor_if_needed(workflow, user)
      workflow.ambitions << ambition if ambition

      workflow.all_tasks.each { |task| Events::CreatedEvent.create!(object: task) }
      # TODO Add Events for Blocks if needed - see #633

      if predecessor_workflow
        workflow.update(predecessor_workflow: predecessor_workflow)
        Events::AddedSuccessorWorkflowEvent.create!(subject: user, object: predecessor_workflow, data: {successor_workflow: workflow})
      end
    end

    workflow
  end

  def self.complete(workflow, user)
    update_assignee(workflow, user, user) if workflow.assignee.nil?
    workflow.complete!
    Events::CompletedEvent.create!(subject: user, object: workflow)

    workflow.ambitions.not_deleted.each do |ambition|
      Events::CompletedWorkflowEvent.create!(object: ambition, data: {completed_workflow: workflow})
    end

    add_contributor_if_needed(workflow, user)
  end

  def self.reopen(workflow, user)
    update_assignee(workflow, user, user) if workflow.assignee.nil?
    workflow.reopen!
    Events::ReopenedEvent.create!(object: workflow, subject: user)
  end

  def self.trash(workflow, user)
    workflow.trash!
    Events::DeletedEvent.create!(object: workflow, subject: user)
    add_contributor_if_needed(workflow, user)
  end

  def self.update_assignee(workflow, assignee, user)
    old_assignee = workflow.assignee

    if old_assignee != assignee
      if workflow.update(assignee: assignee)
        if assignee.nil?
          Events::UnassignedEvent.create!(subject: user, object: workflow, data: {old_assignee: old_assignee})
        else
          Events::AssignedEvent.create!(subject: user, object: workflow, data: {new_assignee: assignee, old_assignee: old_assignee})
          add_contributor_if_needed(workflow, assignee)
        end

        add_contributor_if_needed(workflow, user)
      end
    end
  end

  def self.update_contributors(workflow, contributors, user)
    to_add = contributors - workflow.contributors
    to_remove = workflow.contributors - contributors

    if to_add.any? || to_remove.any?
      workflow.contributors = contributors

      if workflow.valid?
        to_add.each do |contributor|
          Events::AddedContributorEvent.create!(subject: user, object: workflow, data: {added_contributor: contributor})
        end

        to_remove.each do |contributor|
          Events::RemovedContributorEvent.create!(subject: user, object: workflow, data: {removed_contributor: contributor})
        end

        add_contributor_if_needed(workflow, user) unless to_remove.include?(user)
      end
    end
  end

  def self.update(workflow, params, user)
    old_title = workflow.title
    old_description = workflow.description

    workflow.title = params[:title] if params.key?(:title)
    if params.key?(:description) && params[:description].present?
      workflow.description, mentions = Services::Mentioning.extract_mentions(Services::EditorSanitizer.sanitize(params[:description]))
      user_mention_ids = Services::Mentioning.get_user_mentions(mentions)
    end

    if workflow.changed? && workflow.save
      if workflow.saved_change_to_title?
        Events::ChangedTitleEvent.create!(subject: user, object: workflow, data: {new_title: workflow.title, old_title: old_title})
      end

      if workflow.saved_change_to_description?
        old_user_mention_ids = Services::Mentioning.get_user_mentions(Services::Mentioning.get_mentions(old_description))
        user_mention_ids -= old_user_mention_ids
        mentioned_users = User.where(id: user_mention_ids)

        Events::ChangedSummaryEvent.create!(subject: user, object: workflow, data: {new_summary: workflow.description, old_summary: old_description, mentioned_users: mentioned_users})
        mentioned_users.each { |mu| add_contributor_if_needed(workflow, mu) }
      end

      add_contributor_if_needed(workflow, user)
    end
  end

  def self.update_ambitions(workflow, ambitions, user)
    to_add = ambitions - workflow.ambitions
    to_remove = workflow.ambitions - ambitions

    if to_add.any? || to_remove.any?
      workflow.ambitions = ambitions

      if workflow.valid?
        to_add.each do |ambition|
          Events::AddedWorkflowEvent.create!(subject: user, object: ambition, data: {added_workflow: workflow})
        end

        to_remove.each do |ambition|
          Events::RemovedWorkflowEvent.create!(subject: user, object: ambition, data: {removed_workflow: workflow})
        end

        add_contributor_if_needed(workflow, user)
      end
    end
  end

  def self.add_contributor_if_needed(workflow, contributor)
    workflow.contributors << contributor unless workflow.contributors.include?(contributor)
  end
  private_class_method :add_contributor_if_needed
end
