class Interactors::WorkflowDefinitionInteractor
  def self.create(params, user)
    new_workflow_definition = WorkflowDefinition.new(params)
    if new_workflow_definition.save
      Events::CreatedEvent.create!(subject: user, object: new_workflow_definition)
    end
    new_workflow_definition
  end

  def self.update(workflow_definition, params, user)
    old_name = workflow_definition.name

    workflow_definition.update(params)

    if workflow_definition.name != old_name
      Events::ChangedTitleEvent.create!(subject: user, object: workflow_definition, data: {new_title: workflow_definition.name, old_title: old_name})
    end
    workflow_definition
  end

  def self.update_structure(workflow_instance, user)
    Services::WorkflowToDefinition.new(workflow_instance).update!
    workflow_definition = workflow_instance.workflow_definition
    Events::ChangedStructureEvent.create!(subject: user, object: workflow_definition,
      data: {workflow_used_for_update: workflow_instance, new_workflow_definition_version: workflow_definition.version})
  end

  def self.destroy(workflow_definition, _user)
    if workflow_definition.workflows.any?
      workflow_definition.errors.add(:base, "Es existieren noch Instanzen dieser Maßnahme (evtl. auch soft-deleted)! Für diese Situation gibt es noch kein Spezifiziertes Verhalten, bis dahin ist es daher nicht möglich.")
    else
      workflow_definition.destroy
    end
  end
end
