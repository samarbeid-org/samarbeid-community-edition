class Interactors::DossierDefinitionInteractor
  def self.create(params, user)
    new_dossier_definition = DossierDefinition.new(params)
    if new_dossier_definition.save
      Events::CreatedEvent.create!(subject: user, object: new_dossier_definition)
    end
    new_dossier_definition
  end

  def self.update(dossier_definition, params, user)
    old_name = dossier_definition.name

    dossier_definition.update(params)

    if dossier_definition.name != old_name
      Events::ChangedTitleEvent.create!(subject: user, object: dossier_definition, data: {new_title: dossier_definition.name, old_title: old_name})
    end
    dossier_definition
  end

  def self.destroy(dossier_definition)
    if dossier_definition.dossiers.any?
      dossier_definition.errors.add(:base, "Es existieren noch Instanzen dieses Dossiervorlage (evtl. auch soft-deleted)! Für diese Situation gibt es noch kein Spezifiziertes Verhalten, bis dahin ist es daher nicht möglich.")
    else
      dossier_definition.destroy
    end
  end
end
