class Interactors::TaskInteractor
  def self.update(task, params, user)
    old_name = task.name
    if params.key?(:name)
      task.name = params[:name]
    end

    old_description = task.description

    if params.key?(:description) && params[:description].present?
      task.description, mentions = Services::Mentioning.extract_mentions(Services::EditorSanitizer.sanitize(params[:description]))
      user_mention_ids = Services::Mentioning.get_user_mentions(mentions)
    end

    if task.changed? && task.save
      if task.saved_change_to_name?
        Events::ChangedTitleEvent.create!(subject: user, object: task, data: {new_title: task.name, old_title: old_name})
      end

      if task.saved_change_to_description?
        old_user_mention_ids = Services::Mentioning.get_user_mentions(Services::Mentioning.get_mentions(old_description))
        user_mention_ids -= old_user_mention_ids
        mentioned_users = User.where(id: user_mention_ids)

        Events::ChangedSummaryEvent.create!(subject: user, object: task, data: {new_summary: task.description, old_summary: old_description, mentioned_users: mentioned_users})
        mentioned_users.each { |mu| add_contributor_if_needed(task, mu) }
      end

      add_contributor_if_needed(task, user)
    end
  end

  def self.update_assignee(task, assignee, user)
    old_assignee = task.assignee

    if old_assignee != assignee
      if task.update(assignee: assignee)
        if assignee.nil?
          Events::UnassignedEvent.create!(subject: user, object: task, data: {old_assignee: old_assignee})
        else
          Events::AssignedEvent.create!(subject: user, object: task, data: {new_assignee: assignee, old_assignee: old_assignee})
          add_contributor_if_needed(task, assignee)
        end

        add_contributor_if_needed(task, user)
      end
    end
  end

  def self.update_contributors(task, contributors, user)
    to_add = contributors - task.contributors
    to_remove = task.contributors - contributors

    if to_add.any? || to_remove.any?
      task.contributors = contributors

      if task.valid?
        to_add.each do |contributor|
          Events::AddedContributorEvent.create!(subject: user, object: task, data: {added_contributor: contributor})
        end

        to_remove.each do |contributor|
          Events::RemovedContributorEvent.create!(subject: user, object: task, data: {removed_contributor: contributor})
        end

        add_contributor_if_needed(task, user) unless to_remove.include?(user)
      end
    end
  end

  def self.update_marked(task, marked, user)
    if marked === !task.marked?(user)
      if marked
        task.marked_by_users << user
      else
        task.marked_by_users.delete(user)
      end
    end
  end

  def self.update_due_at(task, due_at, user)
    due_at = nil if due_at&.empty?
    old_due_at = task.due_at

    if old_due_at != due_at
      if task.update(due_at: due_at)
        Events::ChangedDueDateEvent.create!(subject: user, object: task, data: {new_due_date: due_at&.to_date, old_due_date: old_due_at})

        add_contributor_if_needed(task, user)
      end
    end
  end

  def self.start(task, user)
    task.errors.add(:base, "task may not be started") && return unless task.may_start?
    update_assignee(task, user, user) if task.assignee.nil?

    if task.start!
      Events::StartedEvent.create!(subject: user, object: task)
      add_contributor_if_needed(task, user)
    end
  end

  def self.snooze(task, start_at, user)
    start_at = nil if start_at&.blank?
    task.errors.add(:base, "task may not be snoozed") && return unless task.may_snooze?
    if task.snooze!
      task.update!(start_at: start_at)
      add_contributor_if_needed(task, user)
    end
  end

  def self.complete(task, user)
    task.errors.add(:base, "task may not be completed") && return unless task.may_complete?
    if task.complete!
      Events::CompletedEvent.create!(subject: user, object: task)
      add_contributor_if_needed(task, user)
    end
  end

  def self.reopen(task, user)
    task.errors.add(:base, "task may not be reopened") && return unless task.may_reopen?
    update_assignee(task, user, user) if task.assignee.nil?
    task.reopen!
    Events::ReopenedEvent.create!(object: task, subject: user)
  end

  def self.skip(task, user)
    task.errors.add(:base, "task may not be skipped") && return unless task.may_skip?
    task.skip!
    Events::SkippedEvent.create!(object: task, subject: user)
  end

  def self.delete(task, user)
    task.errors.add(:base, "task may not be deleted") && return unless task.may_delete_for_ever?
    if task.task_items.map(&:content_item).any? { |ci| ci.blocks.select { |b| b.not_deleted? }.any? }
      task.errors.add(:base, "Dieses Datenfeld wird noch als Bedingung in mindestens einem Block genutzt. Es kann daher nicht gelöscht werden.") && return
    end
    if task.delete_for_ever!
      task.task_items.each do |task_item|
        task_item.destroy!
        task_item.content_item.destroy! if task_item.content_item.task_items.empty?
      end
      Events::DeletedEvent.create!(object: task, subject: user)
      add_contributor_if_needed(task, user)
    end
  end

  def self.update_content_item(task, content_item, new_value, user)
    if content_item.update(value: new_value)
      add_contributor_if_needed(task, user)
      Events::ChangedDataEvent.create!(object: content_item, subject: user)
      start(task, user) if task.may_start?
    end
  end

  def self.add_contributor_if_needed(task, contributor)
    task.contributors << contributor unless task.contributors.include?(contributor)
  end
  private_class_method :add_contributor_if_needed
end
