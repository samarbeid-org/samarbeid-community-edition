class TaskItemDefinition < ApplicationRecord
  belongs_to :task_definition
  belongs_to :content_item_definition
  has_many :task_items, dependent: :destroy
  belongs_to :workflow_definition

  scope :not_deleted, -> { where(deleted: false) }

  validates :position, presence: true
  validate :all_belong_to_same_workflow_definition

  def rails_admin_label
    "#{task_definition&.rails_admin_label} > #{content_item_definition&.label}"
  end

  def build_task_item(task, content_item_definition)
    content_item = task.workflow.content_items.find { |ci| ci.label == content_item_definition.label }
    TaskItem.new(task: task, position: position, info_box: info_box, required: required,
      content_item: content_item, task_item_definition: self)
  end

  private

  def all_belong_to_same_workflow_definition
    return unless task_definition && content_item_definition

    unless [task_definition.workflow_definition_id, content_item_definition.workflow_definition_id].all? { |ref_id| ref_id.eql?(workflow_definition_id) }
      errors.add(:base, "Alles muss zur selben workflow_definition '#{workflow_definition&.rails_admin_label}' gehören!")
    end
  end
end
