class Block < ApplicationRecord
  belongs_to :workflow
  belongs_to :content_item, optional: true
  belongs_to :block_definition, optional: true
  has_many :direct_tasks, -> { order(:position) }, class_name: "Task", as: :workflow_or_block, dependent: :destroy

  validates_presence_of :position
  validates :decision, presence: true, if: -> { content_item.present? }
  validate :validate_decision

  def items
    direct_tasks.to_a.select(&:not_deleted?)
  end

  include AASM

  aasm whiny_persistence: true do
    state :created, initial: true
    state :active, after_enter: -> { enable_one_or_all_tasks }
    state :skipped, :completed, :deleted

    event :enable do
      transitions from: :created, to: :active, if: -> { condition_valid? }
      transitions from: :created, to: :skipped, unless: -> { condition_valid? }
    end

    event :check_condition do
      transitions from: :skipped, to: :active, if: -> { condition_valid? }
      transitions from: :active, to: :skipped, unless: -> { condition_valid? }
    end

    event :complete do
      transitions from: :active, to: :completed, if: -> { items.all?(&:closed?) }
    end
    event :reopen do
      transitions from: :completed, to: :active, if: -> { condition_valid? }
      transitions from: :completed, to: :skipped, unless: -> { condition_valid? }
    end

    event :delete_for_ever do
      transitions to: :deleted
    end
  end

  def open?
    created? || active?
  end

  def closed?
    !open?
  end

  def not_deleted?
    !deleted? && workflow.not_deleted?
  end

  def previous_items
    own_index = workflow.reload.items.index(self)
    return [] if own_index == 0
    workflow.items[0..(own_index - 1)]
  end

  def name
    "B: #{title}"
  end

  def title
    return "Block (#{content_item&.label}:#{decision})" if read_attribute(:title).blank?
    read_attribute(:title)
  end

  def decision_set?
    decision.present? && content_item.present?
  end

  def condition_valid?
    return true unless decision_set?
    return false unless content_item.confirmed?

    content_item.value == content_item.content_type.cast(decision)
  end

  def condition_valid_and_not_confirmed?
    return true unless decision_set?

    content_item.value == content_item.content_type.cast(decision)
  end

  def enable_one_or_all_tasks
    items.each(&:reload)
    if parallel
      items.each { |task| task.enable! if task.may_enable? }
    else
      items.find(&:may_enable?)&.enable!
    end
  end

  private

  def validate_decision
    return if decision.nil? || content_item.nil?

    validation_result = content_item.content_type.validate_data(decision)
    errors.add(:decision, validation_result) if validation_result != true
  end
end
