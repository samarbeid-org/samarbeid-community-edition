class User < ApplicationRecord
  include SearchForIndexWithCategories
  include ReferenceSupport
  include NotificationSupport

  has_one_attached :avatar
  attr_accessor :remove_avatar

  after_save { avatar.purge if remove_avatar == "1" }

  has_many :user_groups, dependent: :destroy
  has_many :groups, through: :user_groups, inverse_of: :users, dependent: :destroy

  has_many :assigned_workflows, class_name: "Workflow", foreign_key: "assignee_id", inverse_of: :assignee, dependent: :nullify
  has_many :assigned_tasks, class_name: "Task", foreign_key: "assignee_id", inverse_of: :assignee, dependent: :nullify
  has_many :assigned_ambitions, class_name: "Ambition", foreign_key: "assignee_id", inverse_of: :assignee, dependent: :nullify
  has_and_belongs_to_many :marked_tasks, class_name: "Task", association_foreign_key: :workflow_task_id, join_table: :users_workflow_tasks_marked

  has_many :contributions, dependent: :destroy
  has_many :contributed_workflows, -> { order "contributions.created_at" }, through: :contributions, source: :contributable, source_type: "Workflow", inverse_of: :contributors
  has_many :contributed_tasks, -> { order "contributions.created_at" }, through: :contributions, source: :contributable, source_type: "Task", inverse_of: :contributors
  has_many :contributed_ambitions, -> { order "contributions.created_at" }, through: :contributions, source: :contributable, source_type: "Ambition", inverse_of: :contributors

  has_many :events, foreign_key: "subject_id", inverse_of: :subject, dependent: :nullify
  has_many :notifications, dependent: :destroy

  has_many :comments, foreign_key: "author_id", inverse_of: :author, dependent: :nullify

  has_many :dossiers, foreign_key: "created_by_id", inverse_of: :created_by, dependent: :nullify

  validates :firstname, :lastname, presence: true
  validates :firstname, :lastname, format: {with: /\A(?!.*deaktiviert).*\z/i, message: "darf nicht 'deaktiviert' enthalten"}

  scope :reorder_by_name_with_user_first, ->(user) { reorder(Arel.sql("(users.id = #{user.id}) desc")).order(:firstname, :lastname, :id) }
  scope :filtered_by_query, ->(query) { where("concat_ws(' ', users.id, firstname, lastname, email) ILIKE ?", "%#{query}%") }
  scope :filtered_by_groups, ->(group_ids) { joins(:groups).where(groups: group_ids) }
  scope :except_users, ->(ids_to_exclude) { where.not("users.id": ids_to_exclude) }
  scope :active, -> { where(deactivated_at: nil) }
  scope :admin, -> { where(role: [:team_admin, :super_admin]) }

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, # basic authentication support for hashed password in users table
    :timeoutable, # timouts session after configure period. Without this user is NEVER signed out by default (even if "remember me" is NOT checked)
    :rememberable, # makes session cookie expire after 14 days if "remember me" is checked on login.
    :validatable #  creates all needed validations for a user email and password

  delegate :can?, :cannot?, to: :ability

  enum role: {user: 0, team_admin: 1, super_admin: 2}

  searchkick language: "german", callbacks: :async, word_middle: [:name, :email], suggest: [:name, :email]

  def ability
    @ability ||= Ability.new(self)
  end

  def is_admin?
    super_admin? || team_admin?
  end

  def groups_for_search
    return Group.all if super_admin?
    groups
  end

  def name
    "#{firstname} #{lastname}"
  end

  def fullname
    "#{deactivated_at ? "[deaktiviert] " : ""}#{name}"
  end

  def identifier
    "@#{id}"
  end

  # ReferenceSupport
  def reference_label(noaccess = false)
    fullname
  end

  def mention_label(noaccess = false)
    "#{deactivated_at ? "[deaktiviert] " : ""}@#{name}"
  end

  def avatar_label
    "#{firstname[0]}#{lastname[0]}"
  end

  def avatar_url(size = nil)
    size ||= 36
    return nil unless avatar.attached?
    Rails.application.routes.url_helpers.rails_representation_url(avatar.variant(resize_to_fit: [size, size]), only_path: true)
  end

  def self.filter_for_list(current_user, apply_filter_to: all, query: "", excluded_user_ids: [], group_ids: [])
    result = apply_filter_to.active
    result = result.except_users(excluded_user_ids) unless excluded_user_ids.empty?
    result = result.filtered_by_query(query) if query.present?
    result = result.filtered_by_groups(group_ids) unless group_ids.empty?

    result.reorder_by_name_with_user_first(current_user).first(10).uniq
    result.uniq
  end

  def self.define_tab_categories
    [["ADMIN", ->(a) { a.is_admin? }],
      ["NO_ADMIN", ->(a) { !a.is_admin? }],
      ["ALL", ->(_a) { true }]]
  end

  def self.defaults_for_search_for_index
    {fields: [:name, :email]}
  end

  def search_data
    {
      name: name&.downcase,
      email: email,
      created_at: created_at,
      tab_categories: tab_categories
    }.merge(visibility_for_groups)
  end

  def visibility_for_groups
    {visible_for_groups: Group.all.map(&:id)}
  end

  # support for deactivation of an user
  def deactivate
    update_attribute(:deactivated_at, Time.current) unless deactivated_at
  end

  def activate
    update_attribute(:deactivated_at, nil) if deactivated_at
  end

  def active_for_authentication?
    super && !deactivated_at
  end
end
