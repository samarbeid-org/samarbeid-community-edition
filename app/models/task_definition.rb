class TaskDefinition < ApplicationRecord
  belongs_to :workflow_definition
  has_many :tasks, dependent: :destroy
  has_many :task_item_definitions, -> { order(:position) }, dependent: :destroy

  belongs_to :definition_workflow_or_block, polymorphic: true, inverse_of: :direct_task_definitions

  scope :not_deleted, -> { where(deleted: false) }

  validates :name, presence: true
  validates :workflow_definition, presence: true
  validates :position, presence: true
  validate :all_belong_to_same_workflow_definition

  def rails_admin_label
    "#{workflow_definition&.rails_admin_label} > #{id}-#{name&.parameterize}"
  end

  def build_task(workflow_or_block)
    new_task = Task.new(name: name, position: position, description: description, task_definition: self)
    new_task.workflow_or_block = workflow_or_block

    new_task.workflow = workflow_or_block if workflow_or_block.is_a?(Workflow)
    new_task.workflow = workflow_or_block.workflow if workflow_or_block.is_a?(Block)

    task_item_definitions.not_deleted.each { |def_task_item| new_task.task_items << def_task_item.build_task_item(new_task, def_task_item.content_item_definition) }
    new_task
  end

  private

  def all_belong_to_same_workflow_definition
    return unless definition_workflow_or_block # Is mandatory with existing validation but may be nil

    ref_workflow_id = definition_workflow_or_block.is_a?(WorkflowDefinition) ? definition_workflow_or_block.id : definition_workflow_or_block.workflow_definition.id
    errors.add(:definition_workflow_or_block, "Muss identisch zu workflow_definition '#{workflow_definition&.rails_admin_label}' sein!") unless ref_workflow_id.eql?(workflow_definition_id)

    unless task_item_definitions.all? { |tid| tid.workflow_definition_id.eql?(workflow_definition_id) }
      errors.add(:task_item_definitions, "Muss zur selben workflow_definition '#{workflow_definition&.rails_admin_label}' gehören!")
    end
  end
end
