class BlockDefinition < ApplicationRecord
  has_many :blocks
  belongs_to :workflow_definition
  belongs_to :content_item_definition, optional: true
  has_many :direct_task_definitions, -> { order(:position) }, class_name: "TaskDefinition", as: :definition_workflow_or_block, dependent: :destroy

  scope :not_deleted, -> { where(deleted: false) }

  validates_presence_of :position
  validate :validate_decision
  validate :all_belong_to_same_workflow_definition

  def name
    "B: #{title}"
  end

  def rails_admin_label
    "#{workflow_definition&.rails_admin_label} > #{id}-#{name&.parameterize}"
  end

  def build_block(new_workflow)
    new_block = Block.new(title: title, position: position, parallel: parallel, decision: decision, block_definition: self)
    new_block.workflow = new_workflow
    new_block.direct_tasks = direct_task_definitions.not_deleted.map { |def_task_in_block| def_task_in_block.build_task(new_block) }

    if content_item_definition
      new_block.content_item = new_workflow.content_items.find { |ci| ci.label == content_item_definition.label }
    end
    new_block
  end

  private

  def validate_decision
    return if decision.nil? || content_item_definition.nil?
    validation_result = content_item_definition.content_type.validate_data(decision)
    errors.add(:decision, validation_result) if validation_result != true
  end

  def all_belong_to_same_workflow_definition
    if content_item_definition && !content_item_definition.workflow_definition.id.eql?(workflow_definition_id)
      errors.add(:content_item_definition, "Muss zur selben workflow_definition '#{workflow_definition&.rails_admin_label}' gehören!")
    end
    unless direct_task_definitions.all? { |td| td.workflow_definition_id.eql?(workflow_definition_id) }
      errors.add(:direct_task_definitions, "Muss zur selben workflow_definition '#{workflow_definition&.rails_admin_label}' gehören!")
    end
  end
end
