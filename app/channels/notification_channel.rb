class NotificationChannel < ApplicationCable::Channel
  def subscribed
    stream_for current_user
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end

  def self.broadcast_info(user, notification)
    NotificationChannel.broadcast_to(user, notification_json(user, notification)) unless notification.seen?
  end

  def self.notification_json(user, notification)
    Jbuilder.new { |json|
      ApplicationController.render(
        "api/notifications/_notification",
        locals: {json: json, notification: notification, current_user: user, truncate_payload_max_length: 500},
        formats: [:json],
        as: :notification
      )
    }.attributes!
  end
  private_class_method :notification_json
end
