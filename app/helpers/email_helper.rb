module EmailHelper
  def attach_email_image(path)
    attachments.inline[path] = File.read(Rails.root.join("app/assets/images/#{path}")) unless attachments[path]
    attachments[path]
  end

  def object_reference_url(ref)
    raise "cannot generate url for #{ref.inspect}" unless ref[:object_type].present? && ref[:object_id].present?

    # XXX: we could also reject to create a url if ref[:deleted] or ref[:noaccess]. Not sure if it's a good idea or not.
    type_path = ref[:object_type].pluralize
    query = "?event=#{ref[:event]}" if ref[:event].present?
    hash = ref[:hash] if ref[:hash]
    "#{root_url_for_mails}#{type_path}/#{ref[:object_id]}#{query}#{hash}"
  end

  def notifications_url
    "#{root_url_for_mails}notifications"
  end

  def my_notification_settings_url
    "#{root_url_for_mails}my/settings/notifications"
  end

  private

  def root_url_for_mails
    default_mailer_url = Rails.application.config.action_mailer.default_url_options[:host]
    URI(default_mailer_url).normalize.to_s
  end
end
